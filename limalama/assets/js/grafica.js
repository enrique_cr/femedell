$(document).ready(function () {
    var network;
    $('body').on('click', '#boton_ver_grafica', function (e) {
        e.preventDefault();
        grafica.obtener_competidores();
    });
    var grafica = {
        obtener_competidores: function () {
            $.ajax({
                url: base_url + 'campeonato/grafica/obtener_competidores',
                data: {},
                type: 'POST',
                dataType: 'JSON',
                success: function (oponentes) {
                    if (oponentes.length > 0) {
                        var cuadrente_perfecto = ['4', '8', '16', '32'];
                        var cp = 0;
                        var numeroCompetidores = oponentes.length;

                        $.each(cuadrente_perfecto, function (index, value) {
                            if (numeroCompetidores <= value) {
                                cp = value;
                                return false;
                            }
                        });
                        var by = cp - numeroCompetidores;
                        var numeroJugadores = numeroCompetidores + by;
                        var numeroNodos = numeroJugadores + (numeroJugadores - 1);
                        var numeroCiclos = Math.floor(numeroNodos / 2);

                        var arreglo_nodos = [];
                        for (var i = 1; i <= numeroNodos; i++) {
                            arreglo_nodos.push(
                                {
                                    id: i,
                                    label: (i.toString() + '________'),
                                    shape: "image",
                                    image: base_url + 'assets/img/cuadro.png'
                                }
                            );
                        }
                        var nodes = new vis.DataSet(arreglo_nodos);

                        //obtener las posiciones impares aleatorias por el by
                        var impares = [];
                        for (var n = 0; n < by; n++) {
                            var numero = Math.round(Math.random() * (1 - cp) + parseInt(cp));
                            if (numero % 2 == 0) {
                                n = n - 1;
                            } else {
                                if (impares.includes(numero)) {
                                    n = n - 1;
                                } else {
                                    impares[n] = numero;
                                }
                            }
                        }
                        //Actualizar los nodos de los jugadores con su informacion por by
                        var nodosJugadores = arreglo_nodos.slice(arreglo_nodos.length - numeroJugadores);
                        var p = 0;
                        console.log(nodosJugadores);
                        console.log(impares);
                        for (var pos = 0; pos < nodosJugadores.length; pos++) {

                            var id_oponente = nodosJugadores[pos].id;//id_nodo donde voy a actualizar

                            if (!impares.includes(pos) && p <= numeroCompetidores) {//solo si no son impares puedes actualizar
                                var nombre_oponente = oponentes[p].nombre;
                                var foto_oponente = oponentes[p].foto;
                                var puntos_oponente = oponentes[p].puntos;

                                //actualizamos la informacion de los nodos de los oponentes
                                nodes.update({
                                    id: id_oponente,
                                    label: nombre_oponente,
                                    shape: 'image',
                                    image: foto_oponente,
                                    puntos: puntos_oponente
                                });

                                p = p + 1;
                            }
                        }
                        var enlace = [];
                        var A = 1;
                        var B = 2;

                        for (var e = 1; e <= numeroCiclos; e++) {
                            enlace.push(
                                {from: e, to: (e + A)},
                                {from: e, to: (e + B)}
                            );
                            A = A + 1;
                            B = B + 1;
                        }
                        var edges = new vis.DataSet(enlace);

                        var container = document.getElementById('grafica');

                        //var container_2 = document.getElementById('grafica_2');

                        var data = {
                            nodes: nodes,
                            edges: edges
                        };

                        var options = {
                            autoResize: true,
                            clickToUse: true,
                            layout: {
                                hierarchical: {
                                    enabled: true,
                                    height: '100%',
                                    width: '100%',
                                    levelSeparation: 150,
                                    nodeSpacing: 100,
                                    treeSpacing: 200,
                                    blockShifting: false,
                                    edgeMinimization: true,
                                    parentCentralization: true,
                                    direction: 'RL',        // UD, DU, LR, RL
                                    sortMethod: 'directed'   // hubsize, directed

                                }
                            },
                            configure: {
                                enabled: false,
                                filter: 'nodes,edges',
                                container: undefined,
                                showButton: true
                            }
                        };

                        network = new vis.Network(container, data, options);
                        //var network = new vis.Network(container_2, data, options);

                        /*Para convertir una network a imagen png*/
                        network.on('afterDrawing', function (ctx) {
                            var dataURL = ctx.canvas.toDataURL('image/png');
                            //$('#imagen_grafica').attr('src', dataURL); //prewiev
                            grafica.enviar_dataurl(dataURL, oponentes[0].id_grafica);
                        });

                    }
                }
            });
        },
        enviar_dataurl:function(dataurl, id_grafico) {
            $.ajax({
                url: base_url + 'campeonato/grafica/dataURL_imagen',
                data:{dataurl, id_grafico},
                type: 'POST',
                dataType: 'JSON',
                success: function (respuesta) {
                    if(respuesta.estatus){
                        $('#boton_ver_grafica_pdf').show();
                        $('#boton_ver_grafica_pdf').attr('href',base_url+'campeonato/grafica/crear_pdf_grafica/'+respuesta.nombre);
                    }
                }
            });
        }
    };
});