$(document).ready(function () {
    $('.sidenav').sidenav();
    $('.collapsible').collapsible();
});

function cargar_mensajes(mensaje) {
    switch(mensaje.tipo) {
        case 'exito':
            color='green';
            break;
        case 'error':
            color='red';
            break;
        case 'advertencia':
            color='yellow';
            break;
        // default:
        // code block
    }
    M.toast({html: mensaje.mensaje, classes: 'rounded '+color});
}

function mostrar_mensaje_toasts(texto, tipo) {
    switch(tipo) {
        case 'success':
            color='green';
            break;
        case 'error':
            color='red';
            break;
        case 'warning':
            color='yellow';
            break;
        // default:
        // code block
    }
    M.toast({html:texto, classes: 'rounded '+color});
}