$(document).ready(function () {
    $('select').formSelect();
    $('.modal').modal();
    $('body').on('click', '#abrir_modal_cat_convocatoria', function (e) {
        e.preventDefault();

        Convocatoria.abrir_modal_convocatoria($(this));
    }),

        $('body').on('change', '#nombre_archivo', function (e) {
            e.preventDefault();
            console.log('boton1');
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#vista_preview').attr('src', e.target.result)
                }
            }
            reader.readAsDataURL(this.files[0]);
        }),

        $('body').on('click', '#boton_guardar_convocatoria', function (e) {
            e.preventDefault();
            Convocatoria.guardar_convocatorias();
            Convocatoria.buscar_convocatorias();
        });

    $('body').on('click', '#boton_buscar_convocatoria', function (e) {
        e.preventDefault();
        Convocatoria.buscar_convocatorias();
    });

    $('body').on('click', '#boton_editar_convocatoria', function (e) {
        e.preventDefault();
        Convocatoria.abrir_modal_convocatoria($(this));
        /*var id_convocatoria = $(this).data('id_convocatoria');
        Convocatoria.obtener_convocatoria_by_id(id_convocatoria);*/
    });

    $('body').on('click', '#boton_eliminar_convocatoria', function (e) {
        e.preventDefault();
        var id_convocatoria = $(this).data('id_convocatoria');
        $('#boton_confirmar_eliminar_convocatoria').data('id_convocatoria', id_convocatoria);
        $('#modal_eliminar_convocatoria').modal('open');
    });

    $('body').on('click', '#boton_confirmar_eliminar_convocatoria', function (e) {
        e.preventDefault();
        var id_convocatoria = $(this).data('id_convocatoria');
        Convocatoria.eliminar_convocatoria(id_convocatoria);
    });

    Convocatoria.trigger_buscar_alumnos();

});


var Convocatoria = {

    trigger_buscar_alumnos: function () {
        $('#boton_buscar_convocatoria').trigger('click');
    },

    abrir_modal_convocatoria: function (btn) {
        var id_convocatoria = btn.data('id_convocatoria');
        var parametro = '';
        if (id_convocatoria != undefined && id_convocatoria != ''){
            parametro = '/'+ id_convocatoria;
        }
        $.ajax({
                url: base_url + 'convocatoria/Convocatoria/abrir_modal'+parametro,
                data: {},
                type: 'post',
                dataType: 'html',
            success: function (respuesta) {
                    console.log(respuesta);
                $('#contenedor_modal_convocatoria').html(respuesta);
                $('#modal_cat_convocatoria').modal().modal('open');
            }
            }
        )
    },

    buscar_convocatorias: function () {
        var palabra_buscar = $('#buscar_convocatoria').val();
        $.ajax({
            url: base_url + 'convocatoria/Convocatoria/consultar_convocatorias',
            data: {
                palabra_buscar
            },
            type: 'POST',
            dataType: 'HTML',
            success: function (respuesta) {
                $('#contenedor_tabla_convocatoria').html(respuesta);
            }
        });
    },

    obtener_convocatoria_by_id: function (id_convocatoria) {
        $.ajax({
            url: base_url + 'convocatoria/Convocatoria/consultar_convocatoria_por_id',
            data: {id_convocatoria},
            type: 'POST',
            datatype: 'HTML',
            success: function (respuesta) {
                $('#contenedor_modal_convocatoria').html(respuesta);
                $('#modal_cat_convocatoria').modal('open');
            }
        })
    },

    guardar_convocatorias: function () {
        var opciones = $('#seleccionar_formas').val();
        var objData = new FormData($('#form_agregar_modificar_convocatoria')[0]);
        objData.append('opciones', opciones);
        console.log(objData);

        $.ajax({
            url: base_url + 'convocatoria/Convocatoria/agregar_modificar_convocatoria',
            data: objData,
            type: 'POST',
            datatype: 'JSON',
            contentType: false,
            processData: false,
            success: function (result) {
                Convocatoria.buscar_convocatorias();
            }
        });
    },

    eliminar_convocatoria: function (id_convocatoria) {
        $.ajax({
            url: base_url + 'convocatoria/Convocatoria/eliminar_convocatoria',
            data: {id_convocatoria},
            type: 'POST',
            datatype: 'JSON',
            success: function (resultado) {
                $('#modal_eliminar_convocatoria').modal('close');
                Convocatoria.buscar_convocatorias();
            }
        });
    }
}


