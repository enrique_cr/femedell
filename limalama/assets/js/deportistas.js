

$(document).ready(function () {

    $('body').on('click','#agregar_modificar_deportista',function () {
        Deportista.agregar_modificar_deportista($(this));
    });
    $('body').on('change','#rol_persona',function () {
        if($(this).val()==2){
            console.log('cambio');
            $('#contenedor_instructor').hide();
        }else{
            $('#contenedor_instructor').show();
        }
    });
    $('body').on('change', '#seleccionar_estado',function (e) {
        e.preventDefault();
        var id_estado=$(this).val();
        $('#seleccionar_municipio option').remove();
        $('#seleccionar_localidad option').remove();
        Domicilio.obtener_municipios_by_id_estado(id_estado);
    });
    $('body').on('change', '#club_persona',function (e) {
        e.preventDefault();
        var id= $(this).val();
        Deportista.obtener_instructores(id)
    });

    $('body').on('change', '#seleccionar_municipio',function (e) {
        e.preventDefault();
        var id_municipio=$(this).val();
        Domicilio.obtener_localidades_by_id_municipio(id_municipio);
    });
    $('body').on('click','.btn_modificar_persona',function () {
        Deportista.agregar_modificar_deportista($(this));
    });
    $('body').on('change','#foto_persona',function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img_foto_persona').attr('src',e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        }
    });
    $('body').on('click','.btn_cerar_modal_registro_persona',function () {

        $('#modal_agregar_modificar_persona').modal('close');
    });
    $('body').on('click','.btn_guardar_perosna',function () {
        if(Deportista.validar('#formulario_regitro_persona')){
            Deportista.guardar_persona($(this));
        }

    });
    $('body').on('click','#buscar_persona',function () {
        Deportista.buscar_personas($(this));
    });
    $('body').on('click','.btn_modal_eliminar_persona',function () {
        Deportista.modal_eliminar_persona($(this));
    });
    $('body').on('click','.btn_eliminar_persona',function () {
        Deportista.eliminar_persona_id($(this));
    });

    Deportista.trigger_buscar_alumnos();


});

var Deportista = {
    trigger_buscar_alumnos:function(){
        $('#buscar_persona').trigger('click');
    },
    buscar_personas:function(){
        var palabra_buscar = $('#barra_busqueda_personsa').val();
        $('#contenedor_tabla_personas').html(' <div class="preloader-wrapper small active">\n' +
            '    <div class="spinner-layer spinner-green-only">\n' +
            '      <div class="circle-clipper left">\n' +
            '        <div class="circle"></div>\n' +
            '      </div><div class="gap-patch">\n' +
            '        <div class="circle"></div>\n' +
            '      </div><div class="circle-clipper right">\n' +
            '        <div class="circle"></div>\n' +
            '      </div>\n' +
            '    </div>\n' +
            '  </div>');
        $.ajax({
            url: base_url+'deportistas/registro_deportistas/buscar_personas',
            type: 'POST',
            dataType:'html',
            data:{palabra_buscar},
            success: function (respuesta) {
                $('#contenedor_tabla_personas').html(respuesta);
            }
        })

    },
    agregar_modificar_deportista: function (btn) {
        $('#contenedor_modal_deportistas').html(' <div class="preloader-wrapper small active">\n' +
            '    <div class="spinner-layer spinner-green-only">\n' +
            '      <div class="circle-clipper left">\n' +
            '        <div class="circle"></div>\n' +
            '      </div><div class="gap-patch">\n' +
            '        <div class="circle"></div>\n' +
            '      </div><div class="circle-clipper right">\n' +
            '        <div class="circle"></div>\n' +
            '      </div>\n' +
            '    </div>\n' +
            '  </div>');
        var id_persona = btn.data('id_persona');
        var parametro = '';
        if (id_persona != undefined && id_persona != ''){
            parametro = '/'+ id_persona;
        }
        $.ajax({
            url: base_url+'deportistas/registro_deportistas/agregar_modificar_deportista'+parametro,
            type: 'POST',
            dataType:'html',
            data:{},
            success: function (respuesta) {
                console.log(respuesta);
                $('#contenedor_modal_deportistas').html(respuesta);
                $('.modal').modal();
                $('select').formSelect();
                $('.collapsible').collapsible();
                $('#modal_agregar_modificar_persona').modal('open');
                Domicilio.obtener_id_es_mun_lo_by_data();
            }
        })
    },
    guardar_persona:function (btn) {
        var form_post = new FormData ($('#formulario_regitro_persona')[0]);
        var id_persona = btn.data('id_persona');
        var parametro = '';
        if (id_persona != undefined && id_persona != ''){
            parametro = '/'+ id_persona;
        }
        console.log(form_post);
        $.ajax({
            url: base_url +'deportistas/registro_deportistas/guardar_persona'+parametro,
            type: 'POST',
            dataType: 'JSON',
            data: form_post,
            contentType:false,
            processData:false,
            success:function (respuesta) {
                if(respuesta.tipo== 'exito') {
                    $('#modal_agregar_modificar_persona').modal('close');
                    Deportista.trigger_buscar_alumnos();
                }
                if (respuesta.tipo=='error'){
                    cargar_mensajes(respuesta);
                }
            }
        })
    },
    modal_eliminar_persona: function (btn) {
        $('#contenedor_modal_deportistas').html(' <div class="preloader-wrapper small active">\n' +
            '    <div class="spinner-layer spinner-green-only">\n' +
            '      <div class="circle-clipper left">\n' +
            '        <div class="circle"></div>\n' +
            '      </div><div class="gap-patch">\n' +
            '        <div class="circle"></div>\n' +
            '      </div><div class="circle-clipper right">\n' +
            '        <div class="circle"></div>\n' +
            '      </div>\n' +
            '    </div>\n' +
            '  </div>');
        var id_persona = btn.data('id_persona');
        var parametro = '';
        if (id_persona != undefined && id_persona != ''){
            parametro = '/'+ id_persona;
        }
        $.ajax({
            url: base_url+'deportistas/registro_deportistas/confirmar_eliminar_persona'+parametro,
            type: 'POST',
            dataType:'html',
            data:{},
            success: function (respuesta) {
                console.log('por aqui pase');
                $('#contenedor_modal_deportistas').html(respuesta);
                $('.modal').modal();
                $('select').formSelect();
                $('.collapsible').collapsible();
                $('#modal_eliminar_persona').modal('open');
            }
        })
    },
    eliminar_persona_id: function (btn) {
        var id_persona = btn.data('id_persona');
        var parametro = '';
        if (id_persona != undefined && id_persona != ''){
            parametro = '/'+ id_persona;
        }
        $.ajax({
            url: base_url+'deportistas/registro_deportistas/eliminar_persona'+parametro,
            type: 'POST',
            dataType:'JSON',
            data:{},
            success: function (respuesta) {
                console.log(respuesta);
                if(respuesta.tipo== 'exito') {
                    cargar_mensajes(respuesta);
                    $('#modal_eliminar_persona').modal('close');
                    Deportista.trigger_buscar_alumnos();
                }
                if (respuesta.tipo=='error'){
                    cargar_mensajes(respuesta);
                }
            }
        })
    },
    obtener_instructores:function (id_club) {
      $.ajax({
          url: base_url+'deportistas/registro_deportistas/obtener_instructores',
          type:'POST',
          dataType:'JSON',
          data:{id_club},
          success:function () {

          }
      })
    },
    validar: function (formulario) {
        var validator = $(formulario).validate({
            rules:{
                //aqui van las reglas
            },
            messages:{
                nombre_persona:"Nombre es requerido",
                apellido_paterno:"Apellido es requerido",
                apellido_materno: "Apellido es requerido",
                curp: "Curp es requerido",
                email_persona: "Email es requerido",
                telefono_persona:'Telefono es requerido',
                calle_persona:'Calle es requerido'
            }
        });
        validator.form();
        var result = validator.valid();
        return result;
    }

}
var Domicilio = {
    obtener_municipios_by_id_estado:function (id_estado, id_municipio=null) {
        $.ajax({
            url: base_url + 'clubes_escuelas/clubes_escuelas_c/obtener_catalogo_municipios_by_id_estado',
            data: {
                id_estado
            },
            type: 'POST',
            dataType: 'JSON',
            success: function (respuesta) {
                Domicilio.mostrar_municipios_en_select(respuesta.catalogo_municipios, id_municipio)
            }
        });
    },
    mostrar_municipios_en_select:function (municipios, id_municipio=null) {
        $('#seleccionar_municipio option').remove();
        $(municipios).each(function (index, item) {
            if(id_municipio!=null && (item.id == id_municipio)){
                $('#seleccionar_municipio').append($('<option selected></option>').attr("value",item.id).text(item.nombre));
            }
            $('#seleccionar_municipio').append($('<option></option>').attr({"value":this.id}).text(this.nombre));
        });
    },
    obtener_localidades_by_id_municipio: function (id_municipio, id_localidad=null) {
        $.ajax({
            url: base_url + 'clubes_escuelas/clubes_escuelas_c/obtener_catalogo_localidades_by_id_municipio',
            data: {
                id_municipio
            },
            type: 'POST',
            dataType: 'JSON',
            success: function (respuesta) {
                Domicilio.mostrar_localidades_en_select(respuesta.catalogo_localidades, id_localidad)
            }
        });
    },
    mostrar_localidades_en_select:function (localidades, id_localidad=null) {
        $('#seleccionar_localidad option').remove();
        $(localidades).each(function (index, item) {
            if(id_localidad!=null && (item.id == id_localidad)){
                $('#seleccionar_localidad').append($('<option selected></option>').attr("value",item.id).text(item.nombre));
            }
            $('#seleccionar_localidad').append($('<option></option>').attr("value",this.id).text(this.nombre));
        });
    },
    obtener_id_es_mun_lo_by_data:function () {
        var id_estado=$('select#seleccionar_estado').data('id_estado')
        var id_municipio=$('select#seleccionar_municipio').data('id_municipio')
        var id_localidad=$('select#seleccionar_localidad').data('id_localidad')
        Domicilio.obtener_municipios_by_id_estado(id_estado, id_municipio);
        Domicilio.obtener_localidades_by_id_municipio(id_municipio, id_localidad);
    },
    limpiar:function () {
        $('#form_agregar_modificar_clubes_escuelas input').attr('value', ''); //Limpiar los campos de texto
        $('#form_agregar_modificar_clubes_escuelas select#seleccionar_municipio option').remove(); //Limpiar los campos de texto
        $('#form_agregar_modificar_clubes_escuelas select#seleccionar_localidad option').remove(); //Limpiar los campos de texto
        $('#form_agregar_modificar_clubes_escuelas').attr('value', '');
        $('#form_agregar_modificar_clubes_escuelas')[0].reset(); //Limpiamos el formulario
    }
}
