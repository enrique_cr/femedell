$(document).ready(function () {
    $('#modal_pregunta_inicial').modal().modal('open');
    $('select').formSelect();

    $('body').on('change','#form_pregunta_inicial input[type=radio]',function (e) {
        e.preventDefault();
        if(this.value =='si'){
            $('#boton_modal_verificar_curp').show().attr("disabled", false);
            $('#boton_modal_continuar_invitado').hide().attr("disabled", true);
            $('#bloque_buscar_curp').show().attr("disabled", false);
        }
        if(this.value =='no'){
            $('#boton_modal_verificar_curp').hide().attr("disabled", true);
            $('#boton_modal_continuar_invitado').show().attr("disabled", false);
            $('#bloque_buscar_curp').hide().attr("disabled", true);
            $('#boton_modal_continuar').hide().attr('disabled',true);
        }

    });

    $('body').on('click', '#boton_modal_verificar_curp', function (e) {
        e.preventDefault();
        var curp = $('#input_modal_buscar_curp').val();
        campeonato.verificar_curp(curp);
    });

    $('body').on('change', '#estado_busqueda', function (e) {
        $('#buscar_alumnos').attr('disabled',false);
    });

    $('body').on('click', '#buscar_alumnos', function (e) {
        var id_estado = $('#estado_busqueda').val();
        campeonato.buscar_alumnos(id_estado);
    });

    $('body').on('change', '.checkbox_pre_registro', function (e) {
        var ids_alumnos = [];
        $("#tabla_pre_registro_alumnos input:checkbox:checked").each(function(index, value) {
            ids_alumnos.push($(this).data('id_alumno'));
        });
    });
    /**
     * Registro de alumnos
     */
    $('body').on('click', '#boton_guardar_pre_registro_alumnos', function (e) {
        var ids_alumnos = [];
        $("#tabla_pre_registro_alumnos input:checkbox:checked").each(function(index, value) {
            ids_alumnos.push($(this).data('id_alumno'));
        });
        var select_id_tipo_continua = $('#id_tipo_continua').val();
        var select_id_combate = $('#id_combate').val();
        var select_id_forma = $('#id_forma').val();

        var datos = new FormData($('#form_pre_registro_alumnos_competencia')[0]);

        datos.append('ids_alumnos',ids_alumnos);
        datos.append('select_id_tipo',select_id_tipo_continua);
        datos.append('select_id_combate',select_id_combate);
        datos.append('select_id_forma',select_id_forma);
        if(campeonato.validar('#form_pre_registro_alumnos_competencia')){
            campeonato.guardar_datos_pre_registro_alumno(datos);
        }
    });
    /**
     *
     */

    $('body').on('click', '#boton_modal_continuar_invitado', function (e) {
        e.preventDefault();
        var id_tipo = $(this).data('id_tipo');
        campeonato.obtener_formularios_registro(id_tipo, null);
        $('#modal_pregunta_inicial').modal('close');
    });

    $('body').on('click', '#boton_modal_continuar', function (e) {
        e.preventDefault();
        var id_tipo = $(this).data('id_tipo');
        var curp = $('#input_modal_buscar_curp').val();
        console.log(curp);
        campeonato.obtener_formularios_registro(id_tipo, curp);
        $('#modal_pregunta_inicial').modal('close');
    });

    $('body').on('change', '#seleccionar_estado',function (e) {
        e.preventDefault();
        var id_estado=$(this).val();
        $('#seleccionar_municipio option').remove();
        $('#seleccionar_localidad option').remove();
        Domicilio.obtener_municipios_by_id_estado(id_estado);
    });
    $('body').on('change', '#seleccionar_municipio',function (e) {
        e.preventDefault();
        var id_municipio=$(this).val();
        Domicilio.obtener_localidades_by_id_municipio(id_municipio);
    });

    $('body').on('click', '#boton_guardar_pre_registro_combate',function (e) {
        e.preventDefault();
        var select_id_tipo_continua = $('#id_tipo_continua').val();
        var select_id_combate = $('#id_combate').val();
        var select_id_forma = $('#id_forma').val();

        var objData = new FormData($('#pre_registro_combate')[0]);

        objData.append('select_id_tipo',select_id_tipo_continua);
        objData.append('select_id_combate',select_id_combate);
        objData.append('select_id_forma',select_id_forma);

        if(campeonato.validar('#pre_registro_combate')){
            campeonato.guardar_datos_pre_registro(objData);
        }
    });

    $('body').on('click', '#boton_generar_ficha_pre_registro',function (e) {
        e.preventDefault();
        window.open($(this).attr('href'),'_blank');

    });

    $('body').on('change','#foto_persona',function (e) {
        e.preventDefault();
        if (this.files && this.files[0]){
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img_foto_persona').attr('src',e.target.result)
            }
        }
        reader.readAsDataURL(this.files[0]);
    });
    $('body').on('change', '#id_tipo_continua',function (e) {
        e.preventDefault();
        var valor = $(this).val();
        $("#id_forma").val('0');
        $("#id_combate").val('0');
        $('#costo_combate_total').val(0);
        $.each(valor,function (index, value) {
            if(value == '1') {
                $('#select_id_combate').show();
                $('#id_combate').show();
                $('#select_id_forma').hide();
                $("#id_forma").val('0');
            }
            if(value == '2') {
                $('#select_id_forma').show();
                $('#id_forma').show();
                $('#select_id_combate').hide();
                $("#id_combate").val('0');
            }
            if(index > 0){
                $('#select_id_combate').show();
                $('#id_combate').show();
                $('#select_id_forma').show();
                $('#id_forma').show();
            }
        });

    });
    $('body').on('change', '#id_forma_pago',function (e) {
        e.preventDefault();
        var valor = $(this).val();
        if(valor == '3'){
            $('#select_id_banco').show();
        }else{
            $("#id_banco").val('0');
            $('#select_id_banco').hide();
        }
    });

    $('body').on('change', 'select.competencia',function (e) {
        e.preventDefault();
        var opcion_combate = $('#id_combate').val();
        var opcion_forma = $('#id_forma').val();

        var cuota = parseInt($('#costo_combate').val());
        var extra = parseInt($('#costo_combate_adicional').val());

        $('#costo_combate_total').val(cuota);

        var total=0;
        var suma_extra=0;

        var los_dos = parseInt(opcion_combate.length)+parseInt(opcion_forma.length);

        if(opcion_combate.length > 1 ||opcion_forma.length>1 || los_dos > 1){
            for (var i =1; i<los_dos;i++){
                suma_extra = suma_extra+extra;
            }
            total = cuota + suma_extra;

            $('#costo_combate_total').val(total);
        }
    });
    var campeonato = {
        verificar_curp:function (curp) {
            $.ajax({
                url: base_url + 'campeonato/registro/verificar_curp',
                data:{curp},
                type: 'POST',
                dataType: 'JSON',
                success: function (respuesta) {
                    console.log(respuesta);
                    if(respuesta.estatus){
                        $('#input_modal_buscar_curp').attr('disabled',true);
                        $('#boton_modal_verificar_curp').attr('disabled',true);
                        $('#boton_modal_continuar').show().attr('disabled',false);
                        $('#mensaje_validacion_curp').text(respuesta.mensaje).css('color','green');
                    }else{
                        $('#mensaje_validacion_curp').text(respuesta.mensaje).css('color','red');;
                    }
                }
            });

        },
        obtener_formularios_registro:function (id_tipo,curp) {
            $.ajax({
                url: base_url + 'campeonato/registro/obtener_formularios_registro',
                data:{
                    id_tipo,
                    curp
                },
                type: 'POST',
                dataType: 'HTML',
                success: function (respuesta) {
                    $('#contenedor_formulario_datos_personales').html(respuesta);
                    $('#contenedor_formulario_datos_combate').attr('hidden',false);
                    $('#boton_guardar_pre_registro_combate').show().attr('disabled', false);
                    Domicilio.obtener_id_es_mun_lo_by_data();

                }
            });
        },
        guardar_datos_pre_registro: function (datos) {
            $.ajax({
                url: base_url + 'campeonato/registro/guardar_datos_pre_registro',
                data:datos,
                type: 'POST',
                dataType: 'JSON',
                contentType: false,
                processData: false,
                success: function (respuesta) {
                    if(respuesta.estatus){
                        console.log(respuesta.id_nuevo_registro);
                        mostrar_mensaje_toasts(respuesta.mensaje,'success');
                        $('#boton_guardar_pre_registro_combate').attr('disabled', true);
                        $('#boton_generar_ficha_pre_registro').data('id_registro',respuesta.id_nuevo_registro);
                        $('#boton_generar_ficha_pre_registro').attr('href',base_url+'campeonato/registro/generar_ficha_pdf/'+respuesta.id_nuevo_registro);
                        $('#boton_generar_ficha_pre_registro').show().attr('disabled', false);

                    }else {
                        mostrar_mensaje_toasts(respuesta.mensaje,'error');
                    }
                }
            });
        },
        buscar_alumnos:function(id_estado){
            $.ajax({
                url: base_url +'campeonato/registro/get_alumnos',
                type:'POST',
                dataType: 'HTML',
                data:{id_estado},
                success: function(respuesta){
                    $('#boton_guardar_pre_registro_alumnos').show();
                    $('#contenedor_pre_registro_alumnos').show();
                    $('#contenedor_tabla_alumnos').html(respuesta);
                }
            });
        },
        guardar_datos_pre_registro_alumno:function(datos){
            $.ajax({
                url: base_url +'campeonato/registro/guardar_datos_pre_registro_alumnos',
                data:datos,
                type:'POST',
                dataType: 'JSON',
                contentType: false,
                processData: false,
                success: function(respuesta){
                    if(respuesta.estatus){
                        mostrar_mensaje_toasts(respuesta.mensaje,'success');
                        $('#boton_guardar_pre_registro_alumnos').attr('disabled', true);
                        $('#boton_generar_ficha_pre_registro_alumnos').show().attr('disabled', false);
                        $('#boton_generar_ficha_pre_registro_alumnos').attr('href','generar_vista_ficha_pre_registro/'+respuesta.datos);

                    }else{
                        if(respuesta.error!=1){
                            var html="<table>";
                            $(respuesta.datos).each(function (index, value) {
                                if(value!=null){
                                    html+=" <tr> <td>" + value[0].nombre +
                                        "</td> <td>" + value[0].apellido_paterno +
                                        "</td><td>"+ value[0].apellido_materno +
                                        "</td><td>"+ value[0].curp+"</td></tr>";
                                }
                            });
                            html+="</table>";
                            $('#contenido_modal_aviso_alumnos_ya_registrados').html(html);
                            $('#mensaje_modal_aviso_alumnos_ya_registrados').text(respuesta.mensaje);
                            $('#modal_aviso_alumnos_ya_registrados').modal().modal('open');
                        }else{
                            $('#contenido_modal_aviso_alumnos_ya_registrados').html('');
                            $('#mensaje_modal_aviso_alumnos_ya_registrados').text(respuesta.mensaje);
                            $('#modal_aviso_alumnos_ya_registrados').modal().modal('open');
                        }
                    }
                }
            })
        },
        generar_ficha_pdf: function (id_nuevo_registro) {
            $.ajax({
                url: base_url + 'campeonato/registro/generar_ficha_pdf/'+id_nuevo_registro,
                data:{},
                type: 'POST',
                dataType: '',
                success: function (respuesta) {

                },

            });
        },
        validar: function (formulario) {
            var validator = $(formulario).validate({
                rules:{
                    //aqui van las reglas de cada input(html)
                },
                messages:{
                    //aqui van los mensajes de cada input[name](html)
                    id_tipo_continua:"Seleccione una o varias opciones",
                    id_combate: "Seleccione una o varias",
                    id_forma:"Seleccione una varias  formas",
                    id_forma_pago: "Seleccione una forma de pago",
                    id_banco:"Seleccione un número de cuenta"
                }
            });
            validator.form();
            var result = validator.valid();
            return result;
        }
    };

    var Domicilio = {
        obtener_municipios_by_id_estado:function (id_estado, id_municipio=null) {
            $.ajax({
                url: base_url + 'clubes_escuelas/clubes_escuelas_c/obtener_catalogo_municipios_by_id_estado',
                data: {
                    id_estado
                },
                type: 'POST',
                dataType: 'JSON',
                success: function (respuesta) {
                    Domicilio.mostrar_municipios_en_select(respuesta.catalogo_municipios, id_municipio)
                }
            });
        },
        mostrar_municipios_en_select:function (municipios, id_municipio=null) {
            $('#seleccionar_municipio option').remove();
            $(municipios).each(function (index, item) {
                if(id_municipio!=null && (item.id == id_municipio)){
                    $('#seleccionar_municipio').append($('<option selected></option>').attr("value",item.id).text(item.nombre));
                }
                $('#seleccionar_municipio').append($('<option></option>').attr({"value":this.id}).text(this.nombre));
            });
        },
        obtener_localidades_by_id_municipio: function (id_municipio, id_localidad=null) {
            $.ajax({
                url: base_url + 'clubes_escuelas/clubes_escuelas_c/obtener_catalogo_localidades_by_id_municipio',
                data: {
                    id_municipio
                },
                type: 'POST',
                dataType: 'JSON',
                success: function (respuesta) {
                    Domicilio.mostrar_localidades_en_select(respuesta.catalogo_localidades, id_localidad)
                }
            });
        },
        mostrar_localidades_en_select:function (localidades, id_localidad=null) {
            $('#seleccionar_localidad option').remove();
            $(localidades).each(function (index, item) {
                if(id_localidad!=null && (item.id == id_localidad)){
                    $('#seleccionar_localidad').append($('<option selected></option>').attr("value",item.id).text(item.nombre));
                }
                $('#seleccionar_localidad').append($('<option></option>').attr("value",this.id).text(this.nombre));
            });
        },
        obtener_id_es_mun_lo_by_data:function () {
            var id_estado=$('select#seleccionar_estado').data('id_estado')
            var id_municipio=$('select#seleccionar_municipio').data('id_municipio')
            var id_localidad=$('select#seleccionar_localidad').data('id_localidad')
            Domicilio.obtener_municipios_by_id_estado(id_estado, id_municipio);
            Domicilio.obtener_localidades_by_id_municipio(id_municipio, id_localidad);
        }
    };
});