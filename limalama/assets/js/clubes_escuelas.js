$(document).ready(function () {

    $('.modal').modal();

    $('body').on('click','#boton_buscar_clubes_escuelas',function (e) {
       e.preventDefault();
       clubes_escuelas.buscar_clubes_escuelas();
    });

    $('body').on('click', '#boton_agregar_clubes_escuelas', function (e) {
        e.preventDefault();
        clubes_escuelas.limpiar();
        clubes_escuelas.agregar_clubes_escuelas();
    });

    $('body').on('change', '#seleccionar_estado',function (e) {
        e.preventDefault();
        var id_estado=$(this).val();
        $('#seleccionar_municipio option').remove();
        $('#seleccionar_localidad option').remove();
        clubes_escuelas.obtener_municipios_by_id_estado(id_estado);
    });

    $('body').on('change', '#seleccionar_municipio',function (e) {
        e.preventDefault();;
        var id_municipio=$(this).val();
        clubes_escuelas.obtener_localidades_by_id_municipio(id_municipio);
    });

    $('body').on('click','#boton_guardar_clubes_escuelas',function (e) {
        e.preventDefault();
        if(clubes_escuelas.validar('#form_agregar_modificar_clubes_escuelas')){
            clubes_escuelas.guardar_modificar_clubes_escuelas();
        }
    });

    $('body').on('click','#boton_cerrar_modal_clubes_escuelas',function (e) {
        e.preventDefault();
        clubes_escuelas.cerrar_clubes_escuelas();
    });

    $('body').on('click', '#boton_editar_clubes_escuelas', function (e) {
        e.preventDefault();
        var id_clubes_escuelas=$(this).data('id_clubes_escuelas');
        clubes_escuelas.obtener_datos_clubes_escuelas_by_id(id_clubes_escuelas);
    });

    $('body').on('click', '#boton_eliminar_clubes_escuelas', function (e) {
        e.preventDefault();
        var id_clubes_escuelas=$(this).data('id_clubes_escuelas');
        $('#modal_confirmacion_eliminar a#boton_confirmar_si_eliminar_clubes_escuelas').data('id_clubes_escuelas',id_clubes_escuelas);
        $('#modal_confirmacion_eliminar').modal('open');
    });

    $('body').on('click', '#boton_confirmar_si_eliminar_clubes_escuelas', function (e) {
        e.preventDefault();
        var id_clubes_escuelas=$(this).data('id_clubes_escuelas');
        clubes_escuelas.boton_confirmar_si_eliminar_clubes_escuelas(id_clubes_escuelas);
    });

    var clubes_escuelas = {
        buscar_clubes_escuelas: function () {
          var palabra_buscar = $('#input_palabra_buscar').val();
          $.ajax({
              url: base_url + 'clubes_escuelas/clubes_escuelas_c/consultar_clubes_escuelas',
              data: {
                  palabra_buscar
              },
              type: 'POST',
              dataType: 'HTML',
              success: function (respuesta) {
                  $('#contenedor_tabla_clubes_escuelas').html(respuesta);
              }
          });
          },
        agregar_clubes_escuelas:function () {
            $('#modal_agregar_clubes_escuelas').modal('open');
        },

        cerrar_clubes_escuelas:function () {
            $('#modal_agregar_clubes_escuelas').modal('close');
        },

        boton_confirmar_si_eliminar_clubes_escuelas:function (id_clubes_escuelas) {
            $.ajax({
                url: base_url + 'clubes_escuelas/clubes_escuelas_c/eliminar_clubes_escuelas',
                data: {
                    id_clubes_escuelas
                },
                type: 'POST',
                dataType: 'JSON',
                success: function (respuesta) {
                    if(respuesta.estado){
                        $('#modal_confirmacion_eliminar').modal('close');
                        mostrar_mensaje_toasts(respuesta.mensaje,'error');
                        clubes_escuelas.buscar_clubes_escuelas();
                    }
                }
            });
        },
        obtener_municipios_by_id_estado:function (id_estado, id_municipio=null) {
            $.ajax({
                url: base_url + 'clubes_escuelas/clubes_escuelas_c/obtener_catalogo_municipios_by_id_estado',
                data: {
                    id_estado
                },
                type: 'POST',
                dataType: 'JSON',
                success: function (respuesta) {
                    clubes_escuelas.mostrar_municipios_en_select(respuesta.catalogo_municipios, id_municipio)
                }
            });
        },
        mostrar_municipios_en_select:function (municipios, id_municipio=null) {
            $('#seleccionar_municipio option').remove();
            $(municipios).each(function (index, item) {
                if(id_municipio!=null && (item.id == id_municipio)){
                    $('#seleccionar_municipio').append($('<option selected></option>').attr("value",item.id).text(item.nombre));
                }
                $('#seleccionar_municipio').append($('<option></option>').attr({"value":this.id}).text(this.nombre));
            });
        },
        obtener_localidades_by_id_municipio: function (id_municipio, id_localidad=null) {
            $.ajax({
                url: base_url + 'clubes_escuelas/clubes_escuelas_c/obtener_catalogo_localidades_by_id_municipio',
                data: {
                    id_municipio
                },
                type: 'POST',
                dataType: 'JSON',
                success: function (respuesta) {
                    clubes_escuelas.mostrar_localidades_en_select(respuesta.catalogo_localidades, id_localidad)
                }
            });
        },
        mostrar_localidades_en_select:function (localidades, id_localidad=null) {
            $('#seleccionar_localidad option').remove();
            $(localidades).each(function (index, item) {
                if(id_localidad!=null && (item.id == id_localidad)){
                    $('#seleccionar_localidad').append($('<option selected></option>').attr("value",item.id).text(item.nombre));
                }
                $('#seleccionar_localidad').append($('<option></option>').attr("value",this.id).text(this.nombre));
            });
        },
        guardar_modificar_clubes_escuelas:function () {
            var datos_clubes_escuela = $('#form_agregar_modificar_clubes_escuelas').serializeArray();
            $.ajax({
                url: base_url + 'clubes_escuelas/clubes_escuelas_c/agregar_modificar_clubes_escuelas',
                data: datos_clubes_escuela, //Si usas {} envias un json...
                type: 'POST',
                dataType: 'JSON',
                success: function (respuesta) {
                    if(respuesta.estado){
                        mostrar_mensaje_toasts(respuesta.mensaje,'success');
                        $('#modal_agregar_clubes_escuelas').modal('close');
                        clubes_escuelas.buscar_clubes_escuelas();
                    }
                }
            });
        },
        obtener_datos_clubes_escuelas_by_id:function (id_clubes_escuelas) {
            $.ajax({
                url: base_url + 'clubes_escuelas/clubes_escuelas_c/consultar_clubes_escuelas_by_id',
                data: {id_clubes_escuelas},
                type: 'POST',
                dataType: 'HTML', //si recibimos un json el controlador debe devolver con un echo json_encode, si recibimos un html, el controlador debe de  hacer   $this->load->view('curso_html/tarjeta_personaje',$data);
                success: function (respuesta) {
                    $('#contenedor_modal_agregar_clubes_escuelas').html(respuesta);
                    $('#modal_agregar_clubes_escuelas').modal().modal('open');//Porque asi?
                    clubes_escuelas.obtener_id_es_mun_lo_by_data();
                }
            });
        },
        obtener_id_es_mun_lo_by_data:function () {
            var id_estado=$('select#seleccionar_estado').data('id_estado')
            var id_municipio=$('select#seleccionar_municipio').data('id_municipio')
            var id_localidad=$('select#seleccionar_localidad').data('id_localidad')
            clubes_escuelas.obtener_municipios_by_id_estado(id_estado, id_municipio);
            clubes_escuelas.obtener_localidades_by_id_municipio(id_municipio, id_localidad);
        },
        limpiar:function () {
            $('#form_agregar_modificar_clubes_escuelas input').attr('value', ''); //Limpiar los campos de texto
            $('#form_agregar_modificar_clubes_escuelas select#seleccionar_municipio option').remove(); //Limpiar los campos de texto
            $('#form_agregar_modificar_clubes_escuelas select#seleccionar_localidad option').remove(); //Limpiar los campos de texto
            $('#form_agregar_modificar_clubes_escuelas').attr('value', '');
            $('#form_agregar_modificar_clubes_escuelas')[0].reset(); //Limpiamos el formulario
        },
        validar: function (formulario) {
            var validator = $(formulario).validate({
                rules:{
                    //aqui van las reglas
                },
                messages:{
                    folio:"Introdusca un folio",
                    nombre:"Introdusca un nombre",
                    id_estado: "Elija un Estado",
                    id_municipio: "Elija un municipio",
                    id_localidad: "Elija una localidad",
                    calle:'Ingrese una direccion',
                    numero_exterior:'Ingrese el numero'
                }
            });
            validator.form();
            var result = validator.valid();
            return result;
        }

    };
});


