<?php
?>

<div id="modal_eliminar_convocatoria" class="modal">
    <div class="modal-content">
        <h5>¿Desea eliminar el registro?</h5>
    </div>
    <div class="modal-footer">
        <a class="waves-effect waves-light btn-small"
           id="boton_confirmar_eliminar_convocatoria"
           data-id_convocatoria=""
        >
            Eliminar
        </a>
        <a class="modal-close waves-effect waves-red red darken-1 btn-small">
            Cancelar
        </a>
    </div>
</div>
