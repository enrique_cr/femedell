<div id="modal_cat_convocatoria" class="modal modal-fixed-footer">
    <div class="modal-content">
        <?php
        //        echo "<pre>"; print_r($catalogo_nivel_convocatoria_ids);exit;
        ?>
        <h4>Agregar convocatoria</h4>
        <div class="row">
            <form class="col s12" id="form_agregar_modificar_convocatoria" method="post"
                  action="modal_cat_convocatoria.php">
                <input type="hidden" name="convocatoria[id_convocatoria]"
                       value="<?= isset($convocatoria) ? $convocatoria['id_convocatoria'] : '' ?>">

                <div class="input-field col s12 l6">
                    <label class="active">Nivel del Campeonato</label>
                    <select class="browser-default" name="convocatoria[convocatoria_id_nivel]">
                        <?php foreach ($catalogo_nivel_convocatoria as $cnc): ?>
                            <option value="<?= $cnc->id_nivel_convocatoria ?>" <?= isset($convocatoria) && $convocatoria['convocatoria_id_nivel'] == $cnc->id_nivel_convocatoria ? 'selected="selected"' : '' ?>><?= $cnc->nombre_nivel_convocatoria ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="input-field col s12 l6">
                    <label class="active">Campeonato rumbo a:</label>
                    <select class="browser-default" name="campeonatos[]">
                        <?php foreach ($catalogo_campeonato as $cc): ?>
                            <option value="<?= $cc->id_catalogo_campeonato ?>" <?= isset($convocatoria_has_campeonato_ids) && in_array($cc->id_catalogo_campeonato, $convocatoria_has_campeonato_ids) ? 'selected="selected"' : '' ?> > <?= $cc->nombre_catalogo_campeonato ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="input-field col s12 l6">
                    <label class="active">Categorias</label>
                    <select style="height: 130px" class="browser-default" multiple name="categorias[]">
                        <?php foreach ($catalogo_categorias as $cc): ?>
                            <option value="<?= $cc->id_catalogo_categorias ?>" <?= isset($convocatoria_has_categorias_ids) && in_array($cc->id_catalogo_categorias, $convocatoria_has_categorias_ids) ? 'selected="selected"' : '' ?>><?= $cc->nombre_catalogo_categorias ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="input-field col s12 l6">
                    <label class="active">Formas de la disciplina</label>
                    <select style="height: 130px" class="browser-default" multiple name="formas[]">
                        <?php foreach ($catalogo_formas as $cf): ?>
                            <option value="<?= $cf->id_catalogo_modalidades ?>" <?= isset($convocatoria_has_formas_ids) && in_array($cf->id_catalogo_modalidades, $convocatoria_has_formas_ids) ? 'selected="selected"' : '' ?>><?= $cf->nombre_catalogo_modalidades ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="input-field col s12 l6">
                    <label class="active">Ramas</label>
                    <select style="height: 50px" class="browser-default" multiple name="ramas[]">
                        <?php foreach ($catalogo_ramas as $cr): ?>
                            <option value="<?= $cr->id_catalogo_ramas ?>" <?= isset($convocatoria_has_ramas_ids) && in_array($cr->id_catalogo_ramas, $convocatoria_has_ramas_ids) ? 'selected="selected"' : '' ?>><?= $cr->nombre_catalogo_ramas ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="input-field col s12 l6">
                    <label class="active">Tipo de competicion</label>
                        <select style="height: 50px" class="browser-default" multiple name="tipos[]">
                        <?php foreach ($catalogo_tipos_competicion as $ctc): ?>
                            <option value="<?= $ctc->id_catalogo_tipos_competicion ?>" <?= isset($convocatoria_has_tipos_competicion_ids) && in_array($ctc->id_catalogo_tipos_competicion, $convocatoria_has_tipos_competicion_ids) ? 'selected="selected"' : '' ?>><?= $ctc->nombre_catalogo_tipos_competicion ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="input-field col s12 l6">
                    <label class="active">Combates</label>
                    <select style="height: 70px" class="browser-default" multiple name="combates[]">
                        <?php foreach ($catalogo_combates as $cc): ?>
                            <option value="<?= $cc->id_catalogo_combates ?>" <?= isset($convocatoria_has_combates_ids) && in_array($cc->id_catalogo_combates, $convocatoria_has_combates_ids) ? 'selected="selected"' : '' ?>><?= $cc->nombre_catalogo_combates ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="input-field col s12 l6">
                    <label class="active">Combates Continuos</label>
                    <select style="height: 85px" class="browser-default" multiple name="continua[]">
                        <?php foreach ($catalogo_combate_continua as $ccc): ?>
                            <option value="<?= $ccc->id_catalogo_continua ?>" <?= isset($convocatoria_has_combate_continua_ids) && in_array($ccc->id_catalogo_continua, $convocatoria_has_combate_continua_ids) ? 'selected="selected"' : '' ?>><?= $ccc->nombre_catalogo_continua ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="input-field col s12">
                    <label class="active">Estado</label>
                    <select class="browser-default" name="convocatoria[convocatoria_id_estado]">
                        <?php foreach ($catalogo_estados as $es): ?>
                            <option value="<?= $es['id'] ?>" <?= isset($convocatoria) && $convocatoria['convocatoria_id_estado'] == $es['id'] ? 'selected="selected"' : '' ?>><?= $es['nombre'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="input-field col s12 l6">
                    <label class="active">Direccion Sede</label>
                    <input class="form-control" name="convocatoria[convocatoria_sede]" type="text"
                           value="<?= isset($convocatoria) ? $convocatoria['convocatoria_sede'] : '' ?>">
                </div>

                <div class="input-field col s12 l6">
                    <label class="active">Numero de Contacto</label>
                    <input class="form-control" name="convocatoria[convocatoria_contacto]" type="tel"
                           value="<?= isset($convocatoria) ? $convocatoria['convocatoria_contacto'] : '' ?>">
                </div>

                <div class="input-field col s12 l6">

                    <input class="form-control" name="convocatoria[convocatoria_fecha_inicio]" type="date"
                           value="<?= isset($convocatoria) ? $convocatoria['convocatoria_fecha_inicio'] : '' ?>">
                    <label class="active">Fecha de incio del campeonato</label>
                </div>

                <div class="input-field col s12 l6">
                    <input class="form-control" name="convocatoria[convocatoria_fecha_fin]" type="date"
                           value="<?= isset($convocatoria) ? $convocatoria['convocatoria_fecha_fin'] : '' ?>">
                    <label class="active">Fecha de fin del campeonato</label>
                </div>

                <div class="input-field col s12 l6">
                    <label class="active">Cuota de recuperacion</label>
                    <input class="form-control" name="convocatoria[convocatoria_costo]" type="number"
                           value="<?= isset($convocatoria) ? $convocatoria['convocatoria_costo'] : '' ?>">
                </div>

                <div class=" col l12">
                    <div>
                        <label>Subir Convocatoria(PDF)</label>
                        <br>
                        <input type="file" class="form-control btn btn-small" id="nombre_archivo" name="nombre_archivo">
                    </div>
                    <div>
                        <label>Vista Previa</label>
                        <br>
                        <embed id="vista_preview" type=""
                               src="<?= (isset($convocatoria['archivo_pdf_ruta'])) ? $convocatoria['archivo_pdf_ruta'] : '' ?>"
                               width="100%" >
                    </div>
                    <div>
                        <a class="prefix hide waves-effect waves-green btn-flat blue-text" id="boton_guardar_archivo">Subir</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <a class="modal-action modal-close waves-effect waves-red btn-flat red-text btn"
           id="boton_cerrar_modal_convocatoria">Cancelar</a>
        <a class="modal-action modal-close waves-effect waves-green btn-flat blue-text btn" id="boton_guardar_convocatoria">Agregar</a>
    </div>
</div>
