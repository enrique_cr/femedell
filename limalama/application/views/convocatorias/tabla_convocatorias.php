<?php
?>

<table class="centered responsive-table striped highlight" id="tabla_convocatoria">
    <thead>
    <tr>
        <th>Nivel</th>
        <th>Nombre del Campeonato</th>
        <th>Estado</th>
        <th>Sede</th>
        <th>Contacto</th>
        <th>Fecha Inicio</th>
        <th>Fecha Fin</th>
        <th>Costo</th>
        <th>Ver PDF</th>
        <th colspan="2">Aciones</th>
    </tr>
    </thead>

    <tbody>
    <?php foreach ($convocatorias as $datos) {?>
        <tr>
            <td><?=$datos['nombre_nivel_convocatoria']?></td>
            <td><?=$datos['nombre_catalogo_campeonato']?></td>
            <td><?=$datos['nombre']?></td>
            <td><?=$datos['convocatoria_sede']?></td>
            <td><?=$datos['convocatoria_contacto']?></td>
            <td><?=$datos['convocatoria_fecha_inicio']?></td>
            <td><?=$datos['convocatoria_fecha_fin']?></td>
            <td><?=$datos['convocatoria_costo']?></td>
            <td>
                <a class="waves-effect waves-light btn-small blue" id="boton_ver_pdf"
                   href="<?=$datos['archivo_pdf_ruta']?>" target="_blank"><i class="material-icons">picture_as_pdf</i></a>
            </td>
            <td>
                <a class="waves-effect waves-light btn-small yellow" id="boton_editar_convocatoria"
                   data-id_convocatoria="<?=$datos['id_convocatoria']?>"><i class="material-icons">remove_red_eye</i></a>
            </td>
            <td>
                <a class="waves-effect waves-light btn-small red" id="boton_eliminar_convocatoria"
                   data-id_convocatoria="<?=$datos['id_convocatoria']?>"><i class="material-icons">block</i></a>
            </td>
        </tr>
    <?php }?>
    </tbody>
</table>
