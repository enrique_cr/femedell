<?php
 $this->load->view('default/header');
?>
    <div class="container">
        <div class="col s6 offset-s6">
            <h3 style="text-align: center" >Registro de las convocatorias</h3>
            <br>
            <div class="row">
                <div class="col s12 l11 input-field">
                    <input placeholder="Buscar convocatoria" class="validate" id="buscar_convocatoria" type="text" name="busqueda">
                    <a  class="waves-effect waves-light btn col l1 prefix" id="boton_buscar_convocatoria"><i class="material-icons">search</i></a>
                </div>
            </div>
        </div>
        <div align="right">
            <a id="abrir_modal_cat_convocatoria" class="waves-effect waves-light btn"><i class="material-icons left">add</i>Agregar</a>
        </div>
        <div class="row" id="contenedor_tabla_convocatoria">
            <?php
            $this->load->view("convocatorias/tabla_convocatorias"); ?>
        </div>
        <br>

        <div id="contenedor_modal_convocatoria">

        </div>
    </div>
    <div class="row" id="contenedor_modal_eliminar">
        <?php $this->load->view("convocatorias/modal/modal_eliminar_convocatoria");?>
    </div>
<?php
$this->load->view('default/footer');
?>

