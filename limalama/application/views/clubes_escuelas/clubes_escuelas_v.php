<?php
/**
 * Created by PhpStorm.
 * User: omar
 * Date: 2019-02-08
 * Time: 14:05
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view("default/header")?>
<div class="container">
    <div class="row">
        <h3 style="text-align: center" >Registro de Club o Escuela</h3>
        <div class="col s10 input-field">
            <input id="input_palabra_buscar" type="text" name="palabra_buscar" class="validate">
            <label for="input_palabra_buscar">Buscar</label>
        </div>
        <div class="col s2" align="right">
            <a class="waves-effect waves-light btn-small" id="boton_buscar_clubes_escuelas"><i class="material-icons">search</i>Buscar</a>
        </div>
    </div>
    <div class="row" align="right">
        <a class="waves-effect waves-light btn-small" id="boton_agregar_clubes_escuelas">
            <i class="material-icons left">add</i>Agregar
        </a>
    </div>
    <div class="row" id="contenedor_tabla_clubes_escuelas">
        <?php
        $data['clubes_escuelas'] = $clubes_escuelas;
        $this->load->view("clubes_escuelas/tabla_clubes_escuelas",$data);
        ?>
    </div>
    <div class="row" id="contenedor_modal_agregar_clubes_escuelas">
        <?php
        $data['catalogo_estados']=$catalogo_estados;
        $this->load->view("clubes_escuelas/modal_agregar_clubes_escuelas");
        ?>
    </div>
    <div class="row" id="contenedor_modal_confirmacion_eliminar">
        <?php $this->load->view("clubes_escuelas/modal_confirmar_eliminar");?>
    </div>
</div>
<?php $this->load->view("default/footer")?>