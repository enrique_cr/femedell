<?php
/**
 * Created by PhpStorm.
 * User: omar
 * Date: 2019-02-08
 * Time: 14:08
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?>
<table id="tabla_clubes_escuelas" class="highlight responsive-table">
    <thead>
    <tr>
        <th>Folio</th>
        <th>Nombre</th>
        <th>Dirección</th>
        <th>Acciones
        </th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($clubes_escuelas as $dato){?>
        <tr>
            <td><?=$dato['folio']?></td>
            <td><?=$dato['nombre']?></td>
            <td>
                <?=$dato['calle'].' No. '.$dato['numero_exterior'].', '. $dato['localidad'].', '.$dato['municipio']. ' '. $dato['estado']?>
            </td>
            <td>
                <a class="waves-effect waves-light yellow btn-small"
                   id="boton_editar_clubes_escuelas"
                   data-id_clubes_escuelas="<?= $dato['id_clubes_escuelas']?>">
                    <i class="tiny material-icons">edit</i>
                </a>
                <a class="waves-effect waves-light red btn-small"
                   id="boton_eliminar_clubes_escuelas"
                   data-id_clubes_escuelas="<?= $dato['id_clubes_escuelas']?>">
                    <i class="tiny material-icons">delete</i>
                </a>
            </td>
        </tr>
    <?php }?>
    </tbody>
</table>
