<?php
/**
 * Created by PhpStorm.
 * User: omar
 * Date: 2019-02-09
 * Time: 12:14
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container">
    <div id="modal_agregar_clubes_escuelas" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4>Clubes y/o Escuelas</h4>
            <p>
            <form id="form_agregar_modificar_clubes_escuelas" class="">
                <fieldset>
                    <legend>Datos generales</legend>
                    <div class=" col s4 input-field">
                        <input id="id_clubes_escuelas" type="text" name="id_clubes_escuelas" class="validate hide"
                               value="<?=(isset($clubes_escuelas['id_clubes_escuelas'])) ? $clubes_escuelas['id_clubes_escuelas'] :''?>">
                        <label for="input_folio" class="active">Folio</label>
                        <input id="input_folio" type="text" name="folio" required class="validate"
                               value="<?=(isset($clubes_escuelas['folio'])) ? $clubes_escuelas['folio'] :''?>">
                    </div>
                    <div class="col s8 input-field">
                        <label for="input_Nombre" class="active">Nombre</label>
                        <input id="input_Nombre" type="text" name="nombre" required class="validate"
                               value="<?=(isset($clubes_escuelas['nombre'])) ? $clubes_escuelas['nombre'] :''?>">
                    </div>
                </fieldset>
                <fieldset>
                    <legend>Datos de ubicación</legend>
                    <div class="col s4">
                        <label for="seleccionar_estado">Estado</label>
                        <select id="seleccionar_estado"
                                name="id_estado"
                                class="browser-default"
                                data-id_estado="<?=(isset($clubes_escuelas['id_estado']))?$clubes_escuelas['id_estado']:''?>"
                                required
                        >
                            <option value="" selected="selected" disabled="disabled">Elige...</option>
                            <?php foreach ($catalogo_estados as $estado){ ?>
                                <option
                                    <?=(isset($clubes_escuelas['id_estado'])&&($clubes_escuelas['id_estado']==$estado['id']))?'selected':''?>
                                        value="<?=$estado['id']?>"
                                >
                                    <?=$estado['nombre']?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col s4">
                        <label for="seleccionar_municipio">Municipio</label>
                        <select id="seleccionar_municipio"
                                name="id_municipio"
                                class="browser-default"
                                data-id_municipio="<?=(isset($clubes_escuelas['id_municipio']))?$clubes_escuelas['id_municipio']:''?>"
                                required
                        >
                            <option value="" selected="selected" disabled="disabled">Elige...</option>
                        </select>
                    </div>
                    <div class="col s4">
                        <label for="seleccionar_localidad">Localidad</label>
                        <select id="seleccionar_localidad"
                                name="id_localidad"
                                class="browser-default"
                                data-id_localidad="<?=(isset($clubes_escuelas['id_localidad']))?$clubes_escuelas['id_localidad']:''?>"
                                required
                        >
                            <option value="" selected="selected" disabled="disabled">Elige...</option>
                        </select>
                    </div>
                    <div class="col s10 input-field">
                        <label for="input_calle" class="active">Calle</label>
                        <input type="text" name="calle" required value="<?=(isset($clubes_escuelas['calle'])) ? $clubes_escuelas['calle'] :''?>" id="input_calle">
                    </div>
                    <div class="col s2 input-field">
                        <label for="input_numero" class="active">No.</label>
                        <input type="number" name="numero_exterior" required value="<?=(isset($clubes_escuelas['numero_exterior'])) ? $clubes_escuelas['numero_exterior'] :''?>" id="input_numero">
                    </div>
                </fieldset>
            </form>
            </p>
        </div>
        <div class="modal-footer">
            <a class="waves-effect waves-light btn-small" id="boton_guardar_clubes_escuelas">
                Guardar
            </a>
            <a id="boton_cerrar_modal_clubes_escuelas" class="waves-effect waves-red red darken-1 btn-small">
                Cancelar
            </a>
        </div>
    </div>
</div>
