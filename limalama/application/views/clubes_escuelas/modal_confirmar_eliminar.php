<?php
/**
 * Created by PhpStorm.
 * User: omar
 * Date: 2019-02-10
 * Time: 01:48
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Modal Structure -->
<div id="modal_confirmacion_eliminar" class="modal">
    <div class="modal-content">
        <h5>¿Desea eliminar el registro?</h5>
    </div>
    <div class="modal-footer">
        <a class="waves-effect waves-light btn-small"
           id="boton_confirmar_si_eliminar_clubes_escuelas"
           data-id_clubes_escuelas=""
        >
            Eliminar
        </a>
        <a class="modal-close waves-effect waves-red red darken-1 btn-small">
            Cancelar
        </a>
    </div>
</div>
