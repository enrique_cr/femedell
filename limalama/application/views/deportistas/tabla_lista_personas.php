<?php
/**
 * Created by PhpStorm.
 * User: toshiba Amilcar Sosa
 * Date: 10/02/2019
 * Time: 06:31 PM
 */
?>

<table class="highlight responsive-table">
    <thead>
    <tr>
        <th>Nombre</th>
        <th>Curp</th>
        <th>Grado</th>
        <th>Telefono</th>
        <th>Correo</th>
        <th>Responsable</th>
        <th>Telefono Responsable</th>
        <th>Tipo Usuario</th>
        <th>Estatus</th>
        <th>Acciones</th>
    </tr>
    </thead>

    <tbody>
    <?php if(isset($lista_personas) && is_array($lista_personas) && sizeof($lista_personas)):
    foreach ($lista_personas as $persona):?>
    <tr>
        <td><?= $persona->nombre .' '. $persona->apellido_paterno .' '. $persona->apellido_materno ?></td>
        <td><?= $persona->curp?></td>
        <td><?= $persona->nombre_catalogo_cintas?></td>
        <td><?= $persona->numero_telefono?></td>
        <td><?= $persona->correo?></td>
        <td><?= $persona->nombre_responsable .' '. $persona->apellido_paterno_responsable .' '. $persona->apellido_materno_responsable?></td>
        <td><?= $persona->telefono_responsable?></td>
        <td><?= $persona->nombre_catalogo_roles?></td>
        <td><?= $persona->nombre_catalogo_estatus?></td>
        <td>
            <a class="waves-effect waves-light yellow btn-small btn_modificar_persona" data-id_persona="<?= $persona->id_persona?>"><i class="tiny material-icons" >edit</i></a>
            <a class="waves-effect waves-light red btn-small btn_modal_eliminar_persona" data-id_persona="<?= $persona->id_persona?>"><i class="tiny material-icons" >delete</i></a>
        </td>
    </tr>
    <?php endforeach;
    endif;?>

    </tbody>
</table>
