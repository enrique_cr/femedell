<?php
/**
 * Created by PhpStorm.
 * User: toshiba Amilcar Sosa
 * Date: 07/02/2019
 * Time: 03:18 PM
 */
?>

<!-- Modal Structure -->
<div id="modal_agregar_modificar_persona" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h4>Agregar Deportista</h4>
        <form id="formulario_regitro_persona">
            <ul class="collapsible popout">
                <li class="active">
                    <div class="collapsible-header"><i class="material-icons">account_circle</i>Datos Personales</div>
                    <div class="collapsible-body">
                        <div class="row">
                            <div class="row">
                                <div class="input-field grey-text col s12 l12">
                                    <select id="rol_persona" name="rol_persona">
                                        <option value="" disabled selected>Seleccione una opción</option>
                                        <?php foreach ($roles as $rol): ?>
                                            <option value="<?= $rol->id_catalogo_roles ?>"
                                                <?= isset($persona) && $persona->id_catalogo_roles == $rol->id_catalogo_roles ? 'selected="selected"' : ''?>
                                            ><?= $rol->nombre_catalogo_roles ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <label for="estado_persona" class="">Rol</label>
                                </div>
                            </div>
                            <div class="input-field col s12 l4">
                                <input id="nombre" type="text" required class="validate" name="nombre_persona" value="<?= isset($persona) ? $persona->nombre : ''?>">
                                <label class="active" for="nombre">Nombre</label>
                            </div>
                            <div class="input-field col s12 l4">
                                <input id="apellido_paterno" name="apellido_paterno" type="text" required class="validate " value="<?= isset($persona) ? $persona->apellido_paterno : ''?>">
                                <label class="active" for="apellido_paterno">Apellido Paterno</label>
                            </div>
                            <div class="input-field col s12 l4">
                                <input id="apellido_materno" name="apellido_materno" type="text" required class="validate " value="<?= isset($persona) ? $persona->apellido_materno : ''?>">
                                <label class="active" for="apellido_materno">Apellido Materno</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 l4">
                                <label>Sexo</label>
                                <select class="active browser-default"
                                        name="id_sexo"
                                >
                                    <option value="" disabled selected>Selecciona una opcion</option>
                                    <option <?=(isset($persona->sexo)&&$persona->sexo == 'M')?'selected="selected"':'' ?> value="2">Hombre</option>
                                    <option <?=(isset($persona->sexo)&&$persona->sexo == 'F')?'selected="selected"':'' ?> value="1">Mujer</option>
                                </select>
                            </div>
                            <div class="input-field grey-text col s12 l4">
                                <input class="" type="date" id="fecha_nacimiento_persona" value="<?= isset($persona) ? $persona->fecha_nacimineto : ''?>"
                                       name="fecha_nacimiento_persona">
                                <label for="fecha_nacimiento_persona_persona" class="active">Fecha de Nacimiento</label>
                            </div>
                            <div class="input-field col s12 l4">
                                <input id="curp" name="curp" type="text"
                                       required class="validate " size=18
                                       maxlength=18 minlength="18" value="<?= isset($persona) ? $persona->curp : ''?>">
                                <label class="active" for="curp_persona">CURP</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 l6">
                                <input id="telefono_persona" name="telefono_persona" type="tel" required class="validate" value="<?= isset($persona) ? $persona->numero_telefono : ''?>">
                                <label class="active" for="telefono_persona">Telefono/Celular</label>
                            </div>
                            <div class="input-field col s12 l6">
                                <input id="email_persona" name="email_persona" type="email" required class="validate" value="<?= isset($persona) ? $persona->correo : ''?>">
                                <label class="active" for="email_persona">E-mail</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12 l4">
                                <input id="calle_persona" name="calle_persona" type="text" required class="validate" value="<?= isset($persona) ? $persona->calle : ''?>">
                                <label class="active" for="calle_persona">Calle</label>
                            </div>
                            <div class="input-field col s12 l2">
                                <input id="numero_domicilio_persona" name="numero_domicilio_persona" type="number"
                                       class="validate" value="<?= isset($persona) ? $persona->numero_exterior : ''?>">
                                <label class="active" for="numero_domicilio_persona">N°</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field grey-text col s12 l4">
                                <label for="seleccionar_estado" class="active">Estado</label>
                                <select id="seleccionar_estado"
                                        name="id_estado"
                                        class="browser-default"
                                        data-id_estado="<?=isset($persona)?$persona->id_estado:''?>"
                                >
                                    <option value="" selected="selected" disabled="disabled">Elige...</option>
                                    <?php foreach ($catalogo_estados as $estado){ ?>
                                        <option
                                            <?=(isset($persona)&&($persona->id_estado==$estado['id']))?'selected':''?>
                                                value="<?=$estado['id']?>"
                                        >
                                            <?=$estado['nombre']?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="input-field grey-text col s12 l4">
                                <label for="seleccionar_municipio" class="active">Municipio</label>
                                <select id="seleccionar_municipio"
                                        name="id_municipio"
                                        class="browser-default"
                                        data-id_municipio="<?= isset($persona)?$persona->id_municipio:''?>"
                                >
                                    <option value="" selected="selected" disabled="disabled">Elige...</option>
                                </select>
                            </div>
                            <div class="input-field grey-text col s12 l4">
                                 <label for="seleccionar_localidad" class="active">Localidad</label>
                                <select id="seleccionar_localidad"
                                        name="id_localidad"
                                        class="browser-default"
                                        data-id_localidad="<?= isset($persona)?$persona->id_localidad:''?>"
                                >
                                    <option value="" selected="selected" disabled="disabled">Elige...</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 l2">
                                <div>
                                    <label>Fotografia</label>
                                </div>
                                <div class="row">
                                    <input id="foto_persona" name="foto_persona" type="file" class="validate">
                                    <div class="input-field col s12 l6">
                                        <img src="<?= isset($persona)?base_url().'assets/img/users_img/'. $persona->url:''?>" id="img_foto_persona" class="rounded mx-auto d-block" height="150" width="150">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header"><i class="material-icons">account_balance</i>Datos Academicos</div>
                    <div class="collapsible-body">
                        <div class="row">
                            <div class="input-field grey-text col s12 l6">
                                <select id="escolaridad_persona" name="escolaridad_persona">
                                    <?php foreach ($escolaridades as $escolaridad): ?>
                                        <option value="<?= $escolaridad->id_catalogo_escolaridad ?>"<?= isset($persona) && $persona->id_catalogo_escolaridad == $escolaridad->id_catalogo_escolaridad ? 'selected="selected"' : ''?>><?= $escolaridad->nombre_catalogo_escolaridad ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <label for="escolaridad_persona" class="">Escolaridad</label>

                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field grey-text col s12 l6">
                                <select id="grado_persona" name="grado_persona">
                                    <option value="" disabled selected>Seleccione una opción</option>
                                    <?php foreach ($grados as $grado): ?>
                                        <option value="<?= $grado->id_catalogo_cintas ?>"<?= isset($persona) && $persona->id_catalogo_cintas == $grado->id_catalogo_cintas ? 'selected="selected"' : ''?>><?= $grado->nombre_catalogo_cintas ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <label for="Grado_persona" class="">Grado</label>
                            </div>
                            <div class="input-field grey-text col s12 l6">
                                <input class="" type="date" id="fecha_grado_persona" name="fecha_grado_persona" value="<?= isset($persona) ? $persona->fecha_grado : ''?>">
                                <label for="fecha_grado_persona" class="active">Fecha de optencion Grado</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field grey-text col s12 l6">
                                <select id="club_persona" name="club_persona">
                                    <option value="" disablgrado>Seleccione una opción</option>
                                    <?php foreach ($clubes as $dato): ?>
                                        <option value="<?= $dato->id_clubes_escuelas?>" <?= isset($persona) && $persona->id_clubes_escuelas == $dato->id_clubes_escuelas ? 'selected="selected"' : ''?>><?= $dato->nombre ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <label for="club_persona" class="">Club</label>
                            </div>
                            <div id="contenedor_instructor" class="input-field grey-text col s12 l6" >
                                <select id="instructor_persona" name="instructor_persona" >
                                    <option value="" disablgrado>Seleccione una opción</option>
                                    <?php foreach ($instructores as $dato): ?>
                                        <option value="<?= $dato->id_persona?>" <?= isset($persona) && $persona->id_instructor == $dato->id_persona ? 'selected="selected"' : ''?> > <?= $dato->nombre ?> </option>
                                    <?php endforeach; ?>
                                </select>
                                <label for="instructor_persona" class="">Instructor</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field grey-text col s12 l6">
                                <select id="estatus_persona" name="estatus_persona">
<!--                                    <option value="" disablgrado>Seleccione una opción</option>-->
                                    <?php foreach ($estatus as $dato): ?>
                                        <option value="<?= $dato->id_catalogo_estatus ?>" <?= isset($persona) && $persona->id_catalgo_estatus == $dato->id_catalogo_estatus ? 'selected="selected"' : ''?>><?= $dato->nombre_catalogo_estatus ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <label for="estatus_persona" class="">Estatus de Activadad</label>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header"><i class="material-icons">add_box</i>Datos Medicos</div>
                    <div class="collapsible-body">
                        <div class="row">
                            <div class="input-field grey-text col s12 l4">
                                <input class="" type="number" id="peso_persona" name="peso_persona" value="<?= isset($persona) ? $persona->peso: ''?>">
                                <label for="peso_persona" class="active">Peso(Kg)</label>
                            </div>
                            <div class="input-field grey-text col s12 l4">
                                <input class="" type="number" id="altura_persona" name="altura_persona"value="<?= isset($persona) ? $persona->estatura : ''?>">
                                <label for="altura_persona" class="active">Altura(cm)</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field grey-text col s12 l6">
                                <select id="sangre_persona" name="sangre_persona">
                                    <option value="" disabled selected>Seleccione una opción</option>
                                    <?php foreach ($tipos as $dato): ?>
                                        <option value="<?= $dato->id_catalogo_tipo_sangre ?>"
                                            <?= isset($persona) && $persona->id_catalogo_tipo_sangre == $dato->id_catalogo_tipo_sangre ? 'selected="selected"' : ''?>><?= $dato->nombre_catalogo_tipo_sangre ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <label for="sangre_persona" class="active">Tipo de Sangre</label>
                            </div>
                            <div class="input-field grey-text col s12 l6">
                                <input class="" type="text" id="alergias_persona" name="alergias_persona"value="<?= isset($persona) ? $persona->alergias : ''?>">
                                <label for="alergias_persona" class="active">Alergias</label>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header"><i class="material-icons">contact_phone</i>Datos de Contacto</div>
                    <div class="collapsible-body">
                        <div class="row">
                            <div class="input-field col s12 l4">
                                <input id="nombre_contacto" name="nombre_contacto" type="text" class="validate " value="<?= isset($persona) ? $persona->nombre_responsable : ''?>">
                                <label class="active" for="nombre_contacto">Nombre</label>
                            </div>
                            <div class="input-field col s12 l4">
                                <input id="apellido_paterno_contacto" name="apellido_paterno_contacto" type="text"
                                       class="validate "value="<?= isset($persona) ? $persona->apellido_paterno_responsable : ''?>">
                                <label class="active" for="apellido_paterno_contacto">Apellido Paterno</label>
                            </div>
                            <div class="input-field col s12 l4">
                                <input id="apellido_materno_contacto" name="apellido_materno_contacto" type="text"
                                       class="validate " value="<?= isset($persona) ? $persona->apellido_materno_responsable : ''?>">
                                <label class="active" for="apellido_materno_contacto">Apellido Materno</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6 l6">
                                <input id="telefono_contacto" name="telefono_contacto" type="tel" class="validate"
                                       value="<?= isset($persona) ? $persona->telefono_responsable : ''?>">
                                <label class="active" for="telefono_contacto">Telefono/Celular</label>
                            </div>
                            <div class="input-field col s6 l6">
                                <select id="parentesco_contacto" name="parentesco_contacto">
                                    <option value="" disabled selected>Seleccione una opción</option>
                                    <?php foreach ($parentescos as $dato): ?>
                                        <option value="<?= $dato->id_catalogo_parentescos ?>"
                                            <?= isset($persona) && $persona->id_parentesco_responsable == $dato->id_catalogo_parentescos ? 'selected="selected"' : ''?>
                                        >
                                            <?= $dato->parentesco ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                                <label for="parentesco_contacto" class="">Parentesco</label>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </form>
    </div>
    <div class="modal-footer">
        <button class="btn waves-effect red btn-small btn_cerar_modal_registro_persona" type="button" name="action">
            Cancelar
        </button>
        <button class="btn waves-effect waves-light btn-small btn_guardar_perosna" type="submit" name="action" data-id_persona="<?= isset($persona) ?$persona->id_persona:''?>"> Guardar
            <i class="material-icons right"></i>
        </button>
    </div>
</div>