<?php
/**
 * Created by PhpStorm.
 * User: toshiba Amilcar Sosa
 * Date: 11/02/2019
 * Time: 10:01 AM
 */
?>

<div id="modal_eliminar_persona" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h4>Eliminar_persona</h4>
        <p>¿Desea eliminar a <?= $persona->nombre?>?</p>
    </div>
    <div class="modal-footer">
        <button class="btn waves-effect red btn-small btn_cerar_modal_eliminar_persona" type="button" name="action">
            Cancelar
        </button>
        <button class="btn waves-effect waves-light btn-small btn_eliminar_persona" type="submit" name="action"
                data-id_persona="<?= isset($persona) ? $persona->id_persona : '' ?>"> eliminar
            <i class="material-icons right"></i>
        </button>
    </div>
</div>
