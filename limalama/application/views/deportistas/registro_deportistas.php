<?php
/**
 * Created by PhpStorm.
 * User: Amilcar Sosa
 * Date: 07/02/2019
 * Time: 11:21 AM
 */

?>

<?php $this->load->view('default/header') ?>
    <div id="container" class="container">
        <h3 style="text-align: center" >Registro de personas</h3>
        <br>
        <div class="row">
            <input type="text" id="barra_busqueda_personsa" class="col l11" placeholder="Buscar por curp">
            <a class="waves-effect waves-light btn  prefix" id="buscar_persona"><i class="material-icons">search</i></a>
        </div>
        <div align="right">
            <a class="waves-effect waves-light btn" id="agregar_modificar_deportista"><i class="material-icons left">add</i>Agregar</a>
        </div>
        <div id="contenedor_tabla_personas"></div>
        <div id="contenedor_modal_deportistas"></div>

    </div>


<?php $this->load->view('default/footer') ?>