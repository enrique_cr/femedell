<?php
/**
 * Created by PhpStorm.
 * User: omar
 * Date: 2019-02-13
 * Time: 14:02
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row" id="form_registro_combate">
    <fieldset>
        <legend class="">Competencia <?='Estatal / Nacional'?></legend>
        <div class="col s12">
            <div>
                <label>Sede: </label>
                <h6><?='Estado'?></h6>
            </div>
        </div>
        <div class="col s4">
            <label>Ramas</label>
            <select class="browser-default"
                    name="id_ramas"
                    required>
                <option value="" disabled readonly selected="">Elige...</option>
                <option value="1">Varonil</option>
                <option value="2">Femenil</option>
            </select>
        </div>
        <div class="col s4">
            <label>Modalidad</label>
            <select class="browser-default"
                    id="id_tipos_competencia"
                    name="id_tipos_competencia"
                    required>
                <option value="" disabled selected>Elige...</option>
                <option selected value="1">Individual</option>
                <option disabled value="2">Equipo</option>
            </select>
        </div>
        <div class="col s4" style="display: none">
            <label>Modalidad</label>
            <select class="browser-default"
                    id="id_categoria"
                    name="id_categoria">
                <option value="" disabled selected>Elige...</option>
                <option disabled value="1">Infantil</option>
                <option disabled value="2">Juvenil</option>
                <option disabled value="3">Adultos</option>
                <option disabled value="4">Principiantes</option>
                <option disabled value="5">Intermedios</option>
                <option disabled value="6">Avanzados</option>
            </select>
        </div>
        <div class="col s4">
            <label>Tipo</label>
            <select class="browser-default"
                    name="id_tipo_continua"
                    id ="id_tipo_continua"
                    multiple
                    required
            >
                <option value="1">Combate</option>
                <option value="2">Formas</option>
            </select>
        </div>
        <div class="col s4" id="select_id_combate" style="display: none">
            <label>Combate</label>
            <select class="browser-default competencia"
                    name="id_combate"
                    id="id_combate"
                    multiple
                    required
            >
                <option value="1">Por Puntos</option>
                <option value="2">Continua (Light contact)</option>
                <option value="3">Full contact</option>
            </select>
        </div>
        <div class="col s4" id="select_id_forma" style="display: none">
            <label>Formas</label>
            <select class="browser-default competencia"
                    name="id_forma"
                    id="id_forma"
                    multiple
                    required
            >
                <option value="1">Tradicional(Estricta)</option>
                <option value="2">Creativa musical</option>
                <option value="3">Armas</option>
            </select>
        </div>
    </fieldset>

    <fieldset>
        <legend>Cuota de recuperación</legend>
            <div class="input-field col s3">
                <label class="active">Categoria base ($)</label>
                <input type="number" id="costo_combate" value="<?='500.00'?>" name="costo_combate" readonly/>
            </div>
            <div class="input-field col s3">
                <label class="active">Categoria adicional ($)</label>
                <input type="number" id="costo_combate_adicional" value="<?='100.00'?>" name="costo_combate_adicional" readonly/>
            </div>
            <div class="input-field col s6">
                <label class="active">Total ($)</label>
                <input type="number" id="costo_combate_total" value="" name="costo_combate_total" readonly/>
            </div>
        <div>
            <div class="col s4">
                <label>Forma de Pago</label>
                <select class="browser-default active"
                        name="id_forma_pago"
                        id="id_forma_pago"
                        required
                >
                    <option value="" disabled selected>Elige...</option>
                    <option value="1">Efectivo(dia del evento)</option>
                    <option value="3">Deposito</option>
                </select>

            </div>
            <div class="col s6" id="select_id_banco" style="display: none">
                <label>Deposito</label>
                <select
                        class="browser-default active"
                        name="id_banco"
                        id="select_catalogo_banco"
                >
                    <option value="" disabled selected>Elige...</option>
                    <?php foreach ($catalogo_bancos as $banco) { ?>
                        <option value="<?= $banco['id_catalogo_bancos'] ?>" >
                            Banco: <?= $banco['nombre_banco'] ?> <br>
                            No. Cuenta: <?= $banco['numero_cuenta'] ?><br>
                            Clabe: <?= $banco['cuenta_clabe'] ?> <br>
                        </option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </fieldset>
</div>
