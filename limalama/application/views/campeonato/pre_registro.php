<?php
/**
 * Created by PhpStorm.
 * User: omar
 * Date: 2019-02-13
 * Time: 12:20
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('default/header'); ?>

<div class="container">
    <div class="row">

        <div class="row" id="contenedor_modal_pregunta_inicial">
            <?php $this->load->view('campeonato/modal_pregunta_incial'); ?>
        </div>
        <form id="pre_registro_combate">
            <div class="row" id="contenedor_formulario_datos_personales">

            </div>

            <div hidden class="row" id="contenedor_formulario_datos_combate">

            </div>
        </form>
    </div>
    <div class="row col s4">
        <a class="waves-effect waves-light btn right"
           id="boton_guardar_pre_registro_combate"
           style="display:none"
        >
            <i class="material-icons left">save</i>Guardar</a>
    </div>
    <div class="row col s4">
        <a class="waves-effect blue darken-1 btn right"
           id="boton_generar_ficha_pre_registro"
           style="display:none"
           data-id_registro=""
           href="" target="_blank"
        >
            <i class="material-icons left">picture_as_pdf</i>Ficha PDF</a>
    </div>
</div>

<?php $this->load->view('default/footer'); ?>
