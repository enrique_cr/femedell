<?php
/**
 * Created by PhpStorm.
 * User: omar
 * Date: 2019-02-17
 * Time: 09:55
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('default/header.php');?>
    <style type="text/css">
        #grafica {
            width: 90%;
            height: 500px ;
            border: 1px solid lightgray;
        }

        /*#grafica_2 {
            width: 90%;
            height: 500px ;
            border: 1px solid lightgray;
        }*/
    </style>
    <div class="container">
        <div class="row">
            <div id="grafica"></div>
        </div>

        <div class="row">
            <div id="grafica_2"></div>
        </div>

        <div class="row">
            <a class="waves-effect waves-light btn"
               id="boton_ver_grafica">Construir Gráfica</a>

            <a class="waves-effect  yellow darken-1 btn"
               id="boton_ver_grafica_pdf"
               href="" target="_blank"
               style="display: none"> PDF
                <i class="material-icons">visibility</i></a>
        </div>
    </div>


<?php $this->load->view('default/footer.php');?>