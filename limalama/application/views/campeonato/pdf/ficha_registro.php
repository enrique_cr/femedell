<?php
/**
 * Created by PhpStorm.
 * User: toshiba Amilcar Sosa
 * Date: 14/02/2019
 * Time: 01:09 PM
 */
?>
<table border="1">
    <tr>
        <td>
            <h4 style="text-align: center">Datos Personales</h4>
            <table>
                <tr>
                    <td width="90%">
                        <b>Nombre: </b><?= $datos['participante'] . ' ' . $datos['apellido_paterno'] . ' ' . $datos['apellido_materno'] ?>
                    </td>
                    <td width="09%" style="text-align: right"><b>Edad:</b><?= $edad ?></td>
                </tr>
            </table>
            <table>
                <tr>
                    <td width="33%"><b>Sexo: </b><?= $datos['sexo'] ?></td>
                    <td width="33%"><b>Categoria: </b><?= '' ?></td>
                    <td width="33%" style="text-align: right"><b>Fecha de
                            Nacimiento:</b><?= $datos['fecha_nacimineto'] ?></td>
                </tr>
            </table>
            <table>
                <tr>
                    <td width="33%"><b>Estatura: </b><?= $datos['estatura'] ?></td>
                    <td width="33%"><b>Peso: </b><?= $datos['peso'] ?>kg</td>
                    <td width="33%" style="text-align: right"><b>Cinturon: </b> <?= $datos['nombre_catalogo_cintas'] ?>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td width="50%">
                        <b>Escuela: </b><?= $datos['club_escuela_otro'] == null ? $datos['club'] : $datos['club_escuela_otro'] ?>
                    </td>
                    <td width="49%"><b>Profesor:</b><?= $datos['instructor_otro'] ?></td>
                </tr>
            </table>
            <table>
                <tr>
                    <td><b>Ciudad: </b><?= $datos['localidad'] ?></td>
                    <td><b>Estado: </b><?= $datos['estado'] ?></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br><br>
<table border="1">
    <tr>
        <td>
            <h4 style="text-align: center">Datos de Participación</h4>
            <table>
                <tr>
                    <td>
                        <?php if (isset($persona_has_competencia_tipo) && is_array($persona_has_competencia_tipo) && sizeof($persona_has_competencia_tipo)): ?>
                            <?php foreach ($persona_has_competencia_tipo as $tipo): ?>
                                <?php if ($tipo['id_competencia_tipo'] == 2): ?>
                                <br>
                                    <b><?= $tipo['tipo']?>: </b>
                                    <?php if (isset($persona_has_formas) && is_array($persona_has_formas) && sizeof($persona_has_formas)): ?>
                                        <?php foreach ($persona_has_formas as $modalidad): ?>
                                            <?= $modalidad['nombre_catalogo_modalidades'] ?>,
                                        <?php endforeach;endif; ?>
                                <?php endif; ?>
                                <?php if ($tipo['id_competencia_tipo'] == 1): ?>
                                    <b><?= $tipo['tipo']?>: </b>
                                    <?php if (isset($persona_has_combate) && is_array($persona_has_combate) && sizeof($persona_has_combate)): ?>
                                        <?php foreach ($persona_has_combate as $modalidad): ?>
                                            <?= $modalidad['nombre_catalogo_combates'] ?>,
                                        <?php endforeach;endif; ?>
                                <?php endif; ?>
                            <?php endforeach;endif; ?>


                    </td>
                </tr>
            </table>
            <br>
        </td>
    </tr>
</table>
<br><br>
<table border="1">
    <tr>
        <td>
            <h4 style="text-align: center">Datos de Pago</h4>
            <table>
                <tr>
                    <td><b>Total a pagar: $</b><?= $datos['costo'] ?></td>
                    <td><b>Forma de pago: </b><?= $datos['forma_pago'] ?> </td>
                    <?php if ($datos['id_banco'] == null || $datos['id_banco'] == ''): ?>
                        <td style="text-align: left"> En dia del evento</td>
                    <?php else: ?>
                        <td><b>Cuenta: </b><?= $datos['numero_cuenta'] ?></td>
                        <td><b>Banco: </b><?= $datos['nombre_banco'] ?></td>
                    <?php endif; ?>
                </tr>
            </table>
            <br>
        </td>
    </tr>
</table>
<br><br>
<table style="text-align: center">
    <tr>
        <td>Nombre y Firma</td>
        <td></td>
        <td>Si es menor de edad</td>
    </tr>
    <tr>
        <td>____________________</td>
        <td></td>
        <td>____________________</td>
    </tr>
    <tr>
        <td>Competidor</td>
        <td></td>
        <td>Padre o Tutor</td>
    </tr>
</table>
<br>
<br>
<br>
---------------------------------------------------------------------------------------------------------------------------------------------------------

<br>
<br>
<br>
<table border="1">
    <tr>
        <td>
            <h4 style="text-align: center">Datos Personales</h4>
            <table>
                <tr>
                    <td width="90%">
                        <b>Nombre: </b><?= $datos['participante'] . ' ' . $datos['apellido_paterno'] . ' ' . $datos['apellido_materno'] ?>
                    </td>
                    <td width="09%" style="text-align: right"><b>Edad:</b><?= $edad ?></td>
                </tr>
            </table>
            <table>
                <tr>
                    <td width="33%"><b>Sexo: </b><?= $datos['sexo'] ?></td>
                    <td width="33%"><b>Categoria: </b><?= '' ?></td>
                    <td width="33%" style="text-align: right"><b>Fecha de
                            Nacimiento:</b><?= $datos['fecha_nacimineto'] ?></td>
                </tr>
            </table>
            <table>
                <tr>
                    <td width="33%"><b>Estatura: </b><?= $datos['estatura'] ?></td>
                    <td width="33%"><b>Peso: </b><?= $datos['peso'] ?>kg</td>
                    <td width="33%" style="text-align: right"><b>Cinturon: </b> <?= $datos['nombre_catalogo_cintas'] ?>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td width="50%">
                        <b>Escuela: </b><?= $datos['club_escuela_otro'] == null ? $datos['club'] : $datos['club_escuela_otro'] ?>
                    </td>
                    <td width="49%"><b>Profesor:</b><?= $datos['instructor_otro'] ?></td>
                </tr>
            </table>
            <table>
                <tr>
                    <td><b>Ciudad: </b><?= $datos['localidad'] ?></td>
                    <td><b>Estado: </b><?= $datos['estado'] ?></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br><br>
<table border="1">
    <tr>
        <td>
            <h4 style="text-align: center">Datos de Participación</h4>
            <table>
                <tr>
                    <td>
                        <?php if (isset($persona_has_competencia_tipo) && is_array($persona_has_competencia_tipo) && sizeof($persona_has_competencia_tipo)): ?>
                            <?php foreach ($persona_has_competencia_tipo as $tipo): ?>
                                <?php if ($tipo['id_competencia_tipo'] == 2): ?>
                                    <br>
                                    <b><?= $tipo['tipo']?>: </b>
                                    <?php if (isset($persona_has_formas) && is_array($persona_has_formas) && sizeof($persona_has_formas)): ?>
                                        <?php foreach ($persona_has_formas as $modalidad): ?>
                                            <?= $modalidad['nombre_catalogo_modalidades'] ?>,
                                        <?php endforeach;endif; ?>
                                <?php endif; ?>
                                <?php if ($tipo['id_competencia_tipo'] == 1): ?>
                                    <b><?= $tipo['tipo']?>: </b>
                                    <?php if (isset($persona_has_combate) && is_array($persona_has_combate) && sizeof($persona_has_combate)): ?>
                                        <?php foreach ($persona_has_combate as $modalidad): ?>
                                            <?= $modalidad['nombre_catalogo_combates'] ?>,
                                        <?php endforeach;endif; ?>
                                <?php endif; ?>
                            <?php endforeach;endif; ?>


                    </td>
                </tr>
            </table>
            <br>
        </td>
    </tr>
</table>
<br><br>
<table style="text-align: center">
    <tr>
        <td>Nombre y Firma</td>
        <td></td>
        <td>Si es menor de edad</td>
    </tr>
    <tr>
        <td>____________________</td>
        <td></td>
        <td>____________________</td>
    </tr>
    <tr>
        <td>Competidor</td>
        <td></td>
        <td>Padre o Tutor</td>
    </tr>
</table>