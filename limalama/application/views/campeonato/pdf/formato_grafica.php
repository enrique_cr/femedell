<?php
/**
 * Created by PhpStorm.
 * User: omar
 * Date: 2019-02-21
 * Time: 16:22
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
    table.table {
        border-collapse: collapse;
        width: 100%;
    }
</style>
<div class="container" id="modal_grafica_img_preview">
    <div class="row">
        <table class="table" border="0">
            <tr>
                <td colspan="2" style="text-align: center;">
                    Asociación Polinesia de Artes Marciales Lima Lama de Tlaxcala A. C<br>
                        Afiliada a la Federación Mexicana de LimaLama A. C.
                </td>
            </tr>
            <tr>
                <td width="50%">
                    CATEGORIA DE:_________ A________ AÑOS PESO:_____ RAMA:______ DIVISIÓN:__________
                </td>
                <td width="30%">
                    Pelea Continua []<br>
                    Peleaa por puntos[]
                </td>
                <td width="20%">
                    Area_________
                </td>
            </tr>
        </table>
    </div>
    <div class="row" id="preview_imagen_grafica">
        <img id="imagen_grafica" src="<?=(isset($nombre))?base_url().'assets/img/pdf_grafica/'.$nombre:''?>" >
    </div>

    <div class="row">
        <p>JUEZ CENTRAL._____________________</p>
        <p>2do JUEZ._________________________</p>
        <p>3er JUEZ._________________________</p>
    </div>
</div>