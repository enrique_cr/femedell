<?php
/**
 * Created by PhpStorm.
 * User: toshiba Amilcar Sosa
 * Date: 19/02/2019
 * Time: 08:47 AM
 */
?>
<?php $this->load->view('default/header') ?>
<div class="container">
    <form id="form_pre_registro_alumnos_competencia">
    <div class="row">
    <fieldset>
        <legend class="">Alumnos</legend>
        <div class="row col s10">
                <select id="estado_busqueda"
                        name="estado_busqueda"
                        class="browser-default"
                        data-id_estado="<?=isset($persona)?$persona->id_estado:''?>"
                >
                    <option value="" selected="selected" disabled="disabled">Elige...</option>
                    <?php foreach ($catalogo_estados as $estado){ ?>
                        <option
                            <?=(isset($persona)&&($persona->id_estado==$estado['id']))?'selected':''?>
                            value="<?=$estado['id']?>"
                        >
                            <?=$estado['nombre']?>
                        </option>
                    <?php } ?>
                </select>
        </div>
        <div class="col s2">
            <a class="waves-effect waves-light btn" disabled="true" id="buscar_alumnos"><i class="material-icons">search</i></a>
        </div>
        <div  class="row" id="contenedor_tabla_alumnos" style="overflow: scroll; width: 100%; height: 200px">
        </div>
    </fieldset>
    </div>
    <div class="row" id="contenedor_pre_registro_alumnos" style="display: none">
        <fieldset>
            <legend>Registro</legend>
            <div class="row">
                <?php
                $data['catalogo_bancos']=$catalogo_bancos;
                $this->load->view('campeonato/registro_combate',$data)
                ;?>
            </div>
        </fieldset>
    </div>
        <div class="row col s4">
            <a class="waves-effect waves-light btn right"
               id="boton_guardar_pre_registro_alumnos"
               style="display:none"
            >
                <i class="material-icons left">save</i>Guardar</a>
        </div>
        <div class="row col s4">
            <a class="waves-effect blue darken-1 btn right"
               id="boton_generar_ficha_pre_registro_alumnos"
               style="display:none"
               data-id_registro=""
               href="" target="_blank"
            >
                <i class="material-icons left">picture_as_pdf</i>Ficha PDF</a>
        </div>
    </form>
    <div class="row">
        <div id="modal_aviso_alumnos_ya_registrados" class="modal bottom-sheet">
            <div class="modal-content">
                <h5 id="mensaje_modal_aviso_alumnos_ya_registrados" class="deep-orange-text accent-3-text">
                    ...
                </h5>
                <div id="contenido_modal_aviso_alumnos_ya_registrados">

                </div>
            </div>
            <div class="modal-footer">
                <a class="modal-close waves-effect waves-green btn-flat">Cerrar</a>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('default/footer') ?>