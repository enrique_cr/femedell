<?php
/**
 * Created by PhpStorm.
 * User: toshiba Amilcar Sosa
 * Date: 19/02/2019
 * Time: 08:57 AM
 */
?>
<table class="highlight responsive-table" id="tabla_pre_registro_alumnos">
    <thead>
    <tr>
        <th></th>
        <th>Nombre</th>
        <th>CURP</th>
        <th>Grado</th>
        <th>Tipo Usuario</th>
        <th>Estatus</th>
        <th>seleccionar</th>
    </tr>
    </thead>

    <tbody>
    <?php if(isset($lista_personas) && is_array($lista_personas) && sizeof($lista_personas)):
        foreach ($lista_personas as $persona):?>
            <tr>
                <td><img src="<?= isset($persona) ? base_url() .'assets/img/users_img/' . $persona->url : '' ?>"
                         id="img_foto_persona" class="rounded mx-auto d-block" height="50" width="50"></td>
                <td><?= $persona->nombre .' '. $persona->apellido_paterno .' '. $persona->apellido_materno ?></td>
                <td><?= $persona->curp?></td>
                <td><?= $persona->nombre_catalogo_cintas?></td>
                <td><?= $persona->nombre_catalogo_roles?></td>
                <td><?= $persona->nombre_catalogo_estatus?></td>
                <td style="text-align: center">
                    <label>
                        <input type="checkbox" class="checkbox_pre_registro" data-id_alumno="<?= $persona->id_persona?>" />
                        <span></span>
                    </label>
                </td>
            </tr>
        <?php endforeach;
    endif;?>
    </tbody>
</table>

