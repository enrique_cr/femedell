<?php
/**
 * Created by PhpStorm.
 * User: toshiba Amilcar Sosa
 * Date: 13/02/2019
 * Time: 01:58 PM
 */
?>
<div id="form_registr_invitado" class="form-control">
    <fieldset>
        <legend>Datos personales</legend>
        <div class="row">
            <div>
                <label>Fotografia</label>
            </div>
            <div class="row">
                <div class="col s4">
                    <img src="<?= isset($persona) ? base_url() .'assets/img/users_img/' . $persona->url : '' ?>"
                         id="img_foto_persona" class="rounded mx-auto d-block" height="150" width="150">
                </div>
                <div class="col s8">
                    <input id="foto_persona" name="foto_persona" type="file"
                           class="validate file-field input-field" <?= $readonly ?> >
                </div>
            </div>
        </div>
        <div class="row">
            <input id="id_persona" type="text" class="hide" name="id_persona" <?= $readonly ?>
                   value="<?= isset($persona) ? $persona->id_persona : '' ?>">
            <div class="input-field col s12 l4">
                <input id="nombre" type="text" class="active" name="nombre_persona" required <?= $readonly ?>
                       value="<?= isset($persona) ? $persona->nombre : '' ?>">
                <label class="active" for="nombre">Nombre</label>
            </div>
            <div class="input-field col s12 l4">
                <input id="apellido_paterno" name="apellido_paterno" type="text" class="active " required <?= $readonly ?>
                       value="<?= isset($persona) ? $persona->apellido_paterno : '' ?>">
                <label class="active" for="apellido_paterno">Apellido Paterno</label>
            </div>
            <div class="input-field col s12 l4">
                <input id="apellido_materno" name="apellido_materno" type="text" class="active" required <?= $readonly ?>
                       value="<?= isset($persona) ? $persona->apellido_materno : '' ?>">
                <label class="active" for="apellido_materno">Apellido Materno</label>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12 l4">
                <input id="curp" type="text" class="active" name="curp" required <?= $readonly ?>
                       value="<?= isset($persona) ? $persona->curp : '' ?>">
                <label class="active" for="curp">Curp</label>
            </div>

            <div class="col s12 l4">
                <label>Sexo</label>
                <select class="active browser-default"
                        name="id_sexo"
                        <?=$disabled?>
                        required
                >
                    <option value="" disabled selected>Selecciona una opcion</option>
                    <option <?=(isset($persona->sexo)&&$persona->sexo == 'M')?'selected="selected"':'' ?> value="2">Hombre</option>
                    <option <?=(isset($persona->sexo)&&$persona->sexo == 'F')?'selected="selected"':'' ?> value="1">Mujer</option>
                </select>
            </div>
            <div class="input-field col s6 l4">
                <input class="active" type="date" id="nacimiento_invitado" <?= $readonly ?>
                       value="<?= isset($persona) ? $persona->fecha_nacimineto : '' ?>"
                       name="nacimiento_invitado"
                       required
                >
                <label for="nacimiento_invitado" class="active">Fecha de Nacimiento</label>

            </div>
        </div>
        <div class="row">
            <div class="input-field grey-text col s12 l4">
                <input class="active" type="number" id="peso_persona" name="peso_persona" required <?= $readonly ?>
                       value="<?= isset($persona) ? $persona->peso : '' ?>">
                <label for="peso_persona" class="active">Peso(kg)</label>
            </div>
            <div class="input-field grey-text col s12 l4">
                <input class="active" type="number" id="altura_persona" name="altura_persona" required <?= $readonly ?>
                       value="<?= isset($persona) ? $persona->estatura : '' ?>">
                <label for="altura_persona" class="active">Altura(cm)</label>
            </div>
            <div class="col s12 l4">
                <label>Cinturon</label>
                <select class="active browser-default"
                        name="id_cinturon"
                    <?=$disabled?>
                        required
                >
                    <option value="" disabled selected>Selecciona una opcion</option>
                    <?php foreach ($grados as $grado): ?>
                        <option value="<?= $grado->id_catalogo_cintas ?>"<?= isset($persona) && $persona->id_catalogo_cintas == $grado->id_catalogo_cintas ? 'selected="selected"' : '' ?>><?= $grado->nombre_catalogo_cintas ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s6 l6">
                <input id="escuela_invitado" type="text" class="active" name="escuela_invitado" <?= $readonly ?>
                       value="<?= isset($persona) ? $persona->club : '' ?>">
                <label class="active" for="escuela_invitado">Escuela</label>
            </div>
            <div class="input-field col s6 l6">
                <input id="profesor_invitado" type="text" class="active" name="profesor_invitado" <?= $readonly ?>
                       value="<?= isset($instructor) ? $persona->nombre : '' ?>">
                <label class="active" for="profesor_invitado">Profesor / Instructor</label>
            </div>
        </div>
        <div class="row">
            <div class="grey-text col s12 l4">
                <label for="seleccionar_estado" class="active">Estado</label>
                <select id="seleccionar_estado"
                        name="id_estado"
                        class="browser-default"
                        data-id_estado="<?= isset($persona) ? $persona->id_estado : '' ?>"
                        required
                    <?=$disabled?>
                >
                    <option value="" selected="selected" disabled="disabled">Elige...</option>
                    <?php foreach ($catalogo_estados as $estado) { ?>
                        <option
                            <?= (isset($persona) && ($persona->id_estado == $estado['id'])) ? 'selected' : '' ?>
                                value="<?= $estado['id'] ?>"
                        >
                            <?= $estado['nombre'] ?>
                        </option>
                    <?php } ?>
                </select>
            </div>

            <div class="grey-text col s12 l4">
                <label for="seleccionar_municipio" class="active">Municipio</label>
                <select id="seleccionar_municipio"
                        name="id_municipio"
                        class="browser-default"
                        data-id_municipio="<?= isset($persona) ? $persona->id_municipio : '' ?>"
                        required
                    <?=$disabled?>
                >
                    <option value="" selected="selected" disabled="disabled">Elige...</option>
                </select>
            </div>
            <div class="grey-text col s12 l4">
                <label for="seleccionar_localidad" class="active">Localidad</label>
                <select id="seleccionar_localidad"
                        name="id_localidad"
                        class="browser-default"
                        data-id_localidad="<?= isset($persona) ? $persona->id_localidad : '' ?>"
                        required
                    <?=$disabled?>
                >
                    <option value="" selected="selected" disabled="disabled">Elige...</option>
                </select>
            </div>
        </div>
    </fieldset>
</div>