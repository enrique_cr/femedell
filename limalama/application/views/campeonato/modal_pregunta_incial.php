<?php
/**
 * Created by PhpStorm.
 * User: omar
 * Date: 2019-02-13
 * Time: 12:26
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div id="modal_pregunta_inicial" class="modal">
    <div class="modal-content">
        <h6>¿Está registrado en el sistema?</h6>
        <form class="form-control" id="form_pregunta_inicial">
            <div class="row">
                <p>
                    <label>
                        <input class="with-gap" name="res_pregunta" type="radio" value="si" />
                        <span>SI</span>
                    </label>
                </p>
                <p>
                    <label>
                        <input class="with-gap" name="res_pregunta" type="radio" value="no"/>
                        <span>NO</span>
                    </label>
                </p>
            </div>
            <div class="row" id="bloque_buscar_curp" style="display:none">

                <label for="input_modal_buscar_curp">Ingresa tu curp</label>
                <input id="input_modal_buscar_curp" type="text" maxlength="18" name="input_modal_buscar_curp"/>

                <div id="mensaje_validacion_curp"></div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <a class="waves-effect yellow darken-4 btn-small"
           style="display:none"
           id="boton_modal_verificar_curp">Verificar</a>

        <a class=" waves-effect waves-green btn-small"
           disabled="true"
           style="display: none"
           data-id_tipo="1"
           id="boton_modal_continuar">Continuar</a>

        <a class="waves-effect  green darken-1 btn-small"
           style="display:none"
           data-id_tipo="2"
           id="boton_modal_continuar_invitado">Invitado</a>

    </div>
</div>
