<?php
/**
 * Created by PhpStorm.
 * User: omar
 * Date: 2019-02-06
 * Time: 14:15
 */
?>
<ul id="slide-out" class="sidenav sidenav-fixed">
    <!-- Datos de usuario -->
    <li>
        <div class="user-view border">
            <div class="background">
                <a href=""><img src="<?=base_url()?>assets/img/softura.png"></a>
            </div>
            <a href="<?=base_url()?>"><img class="circle" src="<?=base_url()?>assets/img/limalama.gif"></a>
            <a href=""><span class="white-text name">usuario </span></a>
            <a href=""><span class="white-text email">usuario@mail.com</span></a>

        </div>
    </li>
    <!--Menu-->
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
            <li>
                <a class="collapsible-header">Altas en el sistemas<i class="material-icons">arrow_drop_down</i></a>
                <div class="collapsible-body">
                    <ul>
                        <li>
                            <a href="<?=  base_url();?>clubes_escuelas/clubes_escuelas_c/clubes_escuelas">Registro Clubes y Escuelas</a>
                        </li>
                        <li>
                            <a href="<?=  base_url();?>deportistas/registro_deportistas">Registro de Personas</a>

                        </li>
                        <li>
                            <a href="<?=  base_url();?>convocatoria/Convocatoria">Registro Convocatorias</a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
            <li>
                <a class="collapsible-header">Campeonato<i class="material-icons">arrow_drop_down</i></a>
                <div class="collapsible-body">
                    <ul>
                        <li><a href="<?=  base_url();?>campeonato/registro">Pre-Registro</a></li>
                        <li><a href="<?=  base_url();?>campeonato/registro/registro_alumnos">Pre-Registro Alumnos</a></li>
                        <li><a href="<?=  base_url();?>campeonato/grafica">Grafica</a></li>
                        <li><a href="#!">En Construccion</a></li>
                        <li><a href="#!">En Construccion</a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
            <li>
                <a class="collapsible-header">En Construccion<i class="material-icons">arrow_drop_down</i></a>
                <div class="collapsible-body">
                    <ul>
                        <li><a href="#!">En Construccion</a></li>
                        <li><a href="#!">En Construccion</a></li>
                        <li><a href="#!">En Construccion</a></li>
                        <li><a href="#!">En Construccion</a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </li>
</ul>
<a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
