<?php
/**
 * Created by PhpStorm.
 * User: omar
 * Date: 2019-02-01
 * Time: 15:17
 */
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--bootstrap--
    <link rel="stylesheet" href="<?=base_url()?>assets/bootstrap/css/bootstrap.min.css">
    -->
    <!--materialize-->
    <link type="text/css" rel="stylesheet" href="<?=base_url()?>assets/materialize/css/materialize.min.css"  media="screen,projection"/>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Sistema-->
    <script>
    var base_url = '<?=base_url()?>';
    </script>
    <!--VIS-->
    <script type="text/javascript" src="<?=base_url()?>assets/vis/vis.js"></script>
    <link href="<?=base_url()?>assets/vis/vis.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/css/estilo_carrusel.css" rel="stylesheet" type="text/css" />
    <title>LIMALAMA</title>
    <style>
        header, main, footer{
            padding-left: 17%;
        }

        @media only screen and (max-width : 992px) {
            header, main, footer{
                padding-left: 0;
            }
        }
        label.error {
            color: red;
        }
        select.error, textarea.error, input.error{
            border-color: red;
        }

    </style>
</head>
<body>
<main>
<?php $this->load->view('default/sidenav')?>