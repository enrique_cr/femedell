<?php
/**
 * Created by PhpStorm.
 * User: omar
 * Date: 2019-02-01
 * Time: 15:17
 */
?>
<!-- Extras -->
<script type="text/javascript" src="<?= base_url() ?>assets/jquery/jquery-3.3.1.min.js"></script>
<script type="text/javascript" href="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.slides.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/carrusel.js"></script>

<script type="text/javascript" src="<?= base_url() ?>assets/jquery/jquery.validate.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/jquery/jquery.validate.min.js"></script>

<script type="text/javascript"
        href="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>
<script type="text/javascript"
        href="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
<!--boostrap--
<script type="text/javascript" src="<?= base_url() ?>assets/bootstrap/js/bootstrap.min.js"></script>
-->
<script defer src="https://use.fontawesome.com/releases/v5.2.0/js/all.js"
        integrity="sha384-4oV5EgaV02iISL2ban6c/RmotsABqE4yZxZLcYMAdG7FAPsyHYAPpywE9PJo+Khy"
        crossorigin="anonymous"></script>
<!--Materialize-->
<script type="text/javascript" src="<?= base_url() ?>assets/materialize/js/materialize.min.js"></script>

<script>var base_url = '<?=base_url()?>';</script>

<!--Sistema-->
<script type="text/javascript" src="<?= base_url() ?>assets/js/principal.js"></script>

<?php if(isset($scripts)) :	foreach($scripts as $js) : ?>
    <script src="<?php echo $js ?>" type="text/javascript" ></script>
<?php endforeach; endif ?>
</main>
</body>
</html>
