<?php
class modelo_convocatorias extends CI_Model {

    public function __construct() {
        parent:: __construct();

    }

    public function obtener_catalogo_nivel_convocatoria(){
        $query = $this->db->get('catalogo_nivel_convocatoria');
        return $query->result();
    }

    public function obtener_catalogo_campeonato(){
        $query = $this->db->get('catalogo_campeonato');
        return $query->result();
    }

    public function obtener_catalogo_categorias(){
        $query = $this->db->get('catalogo_categorias');
        return $query->result();
    }

    public function obtener_catalogo_formas(){
        $query = $this->db->get('catalogo_formas');
        return $query->result();
    }

    public function obtener_catalogo_ramas(){
        $query = $this->db->get('catalogo_ramas');
        return $query->result();
    }

    public function obtener_catalogo_tipos_competicion(){
        $query = $this->db->get('catalogo_tipos_competicion');
        return $query->result();
    }

    public function obtener_catalogo_combates(){
        $query = $this->db->get('catalogo_combates');
        return $query->result();
    }

    public function obtener_catalogo_combate_continua(){
        $query = $this->db->get('catalogo_combate_continua');
        return $query->result();
    }

    public function obtener_catalogo_estados(){
        $query = $this->db->query("SELECT es.id, es.nombre FROM estados es");
        return $query->result_array();
    }

    public function obtener_convocatoria_has_campeonato($id_convocatoria){
        $consulta = "
            select 
                * 
            from convocatorias_has_catalogo_campeonato chc 
                inner join catalogo_campeonato cc on cc.id_catalogo_campeonato = chc.id_catalogo_campeonato
            where chc.id_convocatorias = $id_convocatoria";
        $query = $this->db->query($consulta);
        return $query->result();
    }

    public function obtener_convocatoria_has_categorias($id_convocatoria){
        $consulta = "
            select 
                * 
            from convocatorias_has_catalogo_categorias chc 
                inner join catalogo_categorias cc on cc.id_catalogo_categorias = chc.id_catalogo_categorias
            where chc.id_convocatorias = $id_convocatoria";
        $query = $this->db->query($consulta);
        return $query->result();
    }

    public function obtener_convocatoria_has_formas($id_convocatoria){
        $consulta = "
            select 
                * 
            from convocatorias_has_catalogo_formas chf 
                inner join catalogo_formas cc on cc.id_catalogo_modalidades = chf.id_catalogo_modalidades
            where chf.id_convocatorias = $id_convocatoria";
        $query = $this->db->query($consulta);
        return $query->result();
    }

    public function obtener_convocatoria_has_ramas($id_convocatoria){
        $consulta = "
            select 
                * 
            from convocatorias_has_catalogo_ramas chr
                inner join catalogo_ramas cr on cr.id_catalogo_ramas = chr.id_catalogo_ramas
            where chr.id_convocatorias = $id_convocatoria";
        $query = $this->db->query($consulta);
        return $query->result();
    }

    public function obtener_convocatoria_has_tipos_competicion($id_convocatoria){
        $consulta = "
            select 
                * 
            from convocatorias_has_catalogo_tipos_competicion chc 
                inner join catalogo_tipos_competicion cc on cc.id_catalogo_tipos_competicion = chc.id_catalogo_tipos_competicion
            where chc.id_convocatorias = $id_convocatoria";
        $query = $this->db->query($consulta);
        return $query->result();
    }

    public function obtener_convocatoria_has_combates($id_convocatoria){
        $consulta = "
            select 
                * 
            from convocatorias_has_catalogo_combates chc 
                inner join catalogo_combates cc on cc.id_catalogo_combates = chc.id_catalogo_combates
            where chc.id_convocatorias = $id_convocatoria";
        $query = $this->db->query($consulta);
        return $query->result();
    }

    public function obtener_convocatoria_has_combate_contiua($id_convocatoria){
        $consulta = "
            select 
                * 
            from convocatorias_has_catalogo_combate_continua chc 
                inner join catalogo_combate_continua cc on cc.id_catalogo_continua = chc.id_catalogo_combate_continua
            where chc.id_convocatorias = $id_convocatoria";
        $query = $this->db->query($consulta);
        return $query->result();
    }

    public function obtener_convocatoria_by_id($id_convocatoria){
        $this->db->trans_start();
        $consulta_por_id = $this->db->query("select co.*, ap.*, es.id, es.nombre
            from convocatorias co 
            inner join archivo_pdf ap on ap.archivo_pdf_id_convocatoria = co.id_convocatoria
            left join estados es on es.id = co.convocatoria_id_estado
            left join catalogo_nivel_convocatoria cnc on cnc.id_nivel_convocatoria = co.convocatoria_id_nivel
            where co.id_convocatoria = $id_convocatoria;");
        $this->db->trans_complete();
        return $consulta_por_id->row_array();
    }

    public function obtener_convocatorias($busqueda=null){
        if($busqueda == '' || $busqueda==null){
            $this->db->trans_start();
            $consulta=$this->db->query("
                select 
                co.*, ap.*, es.nombre, cnc.nombre_nivel_convocatoria, chc.*, ca.nombre_catalogo_campeonato
                from convocatorias co 
                inner join archivo_pdf ap on ap.archivo_pdf_id_convocatoria = co.id_convocatoria 
                left join estados es on es.id = co.convocatoria_id_estado
                left join catalogo_nivel_convocatoria cnc on cnc.id_nivel_convocatoria = co.convocatoria_id_nivel
                inner join convocatorias_has_catalogo_campeonato chc on chc.id_convocatorias = co.id_convocatoria
                inner join catalogo_campeonato ca on ca.id_catalogo_campeonato = chc.id_catalogo_campeonato;
            ");
            $this->db->trans_complete();
        }else{
            $this->db->trans_start();
            $consulta=$this->db->query(" 
                select 
                co.*, ap.*, es.nombre, cnc.nombre_nivel_convocatoria, chc.*, ca.nombre_catalogo_campeonato
                from convocatorias co 
                inner join archivo_pdf ap on ap.archivo_pdf_id_convocatoria = co.id_convocatoria 
                left join estados es on es.id = co.convocatoria_id_estado
                left join catalogo_nivel_convocatoria cnc on cnc.id_nivel_convocatoria = co.convocatoria_id_nivel
                inner join convocatorias_has_catalogo_campeonato chc on chc.id_convocatorias = co.id_convocatoria
                inner join catalogo_campeonato ca on ca.id_catalogo_campeonato = chc.id_catalogo_campeonato
                where cc.nombre_catalogo_campeonato like '%". $busqueda."%';
            ");
            $this->db->trans_complete();
        }
        return $consulta->result_array();
    }

    public function guardar_convocatoria($post){
//        var_dump($post['convocatoria']);exit();
        if(isset($post['convocatoria']['id_convocatoria']) && $post['convocatoria']['id_convocatoria'] != ''){
            //para un actualiza
            $id_convocatoria = $post['convocatoria']['id_convocatoria'];
//            var_dump($id_convocatoria);exit();
            $this->db->where('id_convocatoria',$id_convocatoria);
            $this->db->update('convocatorias',$post['convocatoria']);
            $this->guardar_convocatoria_has_campeonato($id_convocatoria,$post['campeonatos']);
            $this->guardar_convocatoria_has_categorias($id_convocatoria,$post['categorias']);
            $this->guardar_convocatoria_has_forma($id_convocatoria,$post['formas']);
            $this->guardar_convocatoria_has_rama($id_convocatoria,$post['ramas']);
            $this->guardar_convocatoria_has_tipos_competicion($id_convocatoria,$post['tipos']);
            $this->guardar_convocatoria_has_combates($id_convocatoria,$post['combates']);
            $this->guardar_convocatoria_has_combate_continua($id_convocatoria,$post['continua']);

            $this->insertar_pdf($id_convocatoria,$post['ruta']);
        }else{

            //para guardar una nueva
            $this->db->insert('convocatorias',$post['convocatoria']);
            $id_convocatoria = $this->db->insert_id();
            $this->guardar_convocatoria_has_campeonato($id_convocatoria,$post['campeonatos']);
            $this->guardar_convocatoria_has_categorias($id_convocatoria,$post['categorias']);
            $this->guardar_convocatoria_has_forma($id_convocatoria,$post['formas']);
            $this->guardar_convocatoria_has_rama($id_convocatoria,$post['ramas']);
            $this->guardar_convocatoria_has_tipos_competicion($id_convocatoria,$post['tipos']);
            $this->guardar_convocatoria_has_combates($id_convocatoria,$post['combates']);
            $this->guardar_convocatoria_has_combate_continua($id_convocatoria,$post['continua']);

            $this->insertar_pdf($id_convocatoria,$post['ruta']);
        }return true;
    }

    private function guardar_convocatoria_has_campeonato($id_convocatoria,$array_campeonato){
        $this->db->where('id_convocatorias',$id_convocatoria);
        $this->db->delete('convocatorias_has_catalogo_campeonato');
        foreach ($array_campeonato as $campeonato){
            $insert['id_convocatorias'] = $id_convocatoria;
            $insert['id_catalogo_campeonato'] = $campeonato;
            $this->db->insert('convocatorias_has_catalogo_campeonato',$insert);
        }return true;
    }

    private function guardar_convocatoria_has_categorias($id_convocatoria,$array_categorias){
        $this->db->where('id_convocatorias',$id_convocatoria);
        $this->db->delete('convocatorias_has_catalogo_categorias');
        foreach ($array_categorias as $categorias){
            $insert['id_convocatorias'] = $id_convocatoria;
            $insert['id_catalogo_categorias'] = $categorias;
            $this->db->insert('convocatorias_has_catalogo_categorias',$insert);
        }return true;
    }

    private function guardar_convocatoria_has_forma($id_convocatoria,$array_formas){
        $this->db->where('id_convocatorias',$id_convocatoria);
        $this->db->delete('convocatorias_has_catalogo_formas');
        foreach ($array_formas as $forma){
            $insert['id_convocatorias'] = $id_convocatoria;
            $insert['id_catalogo_modalidades'] = $forma;
            $this->db->insert('convocatorias_has_catalogo_formas',$insert);
        }return true;
    }

    private function guardar_convocatoria_has_rama($id_convocatoria,$array_ramas){
        $this->db->where('id_convocatorias',$id_convocatoria);
        $this->db->delete('convocatorias_has_catalogo_ramas');
        foreach ($array_ramas as $rama){
            $insert['id_convocatorias'] = $id_convocatoria;
            $insert['id_catalogo_ramas'] = $rama;
            $this->db->insert('convocatorias_has_catalogo_ramas',$insert);
        }return true;
    }

    private function guardar_convocatoria_has_tipos_competicion($id_convocatoria,$array_tipos){
        $this->db->where('id_convocatorias',$id_convocatoria);
        $this->db->delete('convocatorias_has_catalogo_tipos_competicion');
        foreach ($array_tipos as $tipos){
            $insert['id_convocatorias'] = $id_convocatoria;
            $insert['id_catalogo_tipos_competicion'] = $tipos;
            $this->db->insert('convocatorias_has_catalogo_tipos_competicion',$insert);
        }return true;
    }

    private function guardar_convocatoria_has_combates($id_convocatoria,$array_combates){
        $this->db->where('id_convocatorias',$id_convocatoria);
        $this->db->delete('convocatorias_has_catalogo_combates');
        foreach ($array_combates as $combates){
            $insert['id_convocatorias'] = $id_convocatoria;
            $insert['id_catalogo_combates'] = $combates;
            $this->db->insert('convocatorias_has_catalogo_combates',$insert);
        }return true;
    }

    private function guardar_convocatoria_has_combate_continua($id_convocatoria,$array_continua){
        $this->db->where('id_convocatorias',$id_convocatoria);
        $this->db->delete('convocatorias_has_catalogo_combate_continua');
        foreach ($array_continua as $continua){
            $insert['id_convocatorias'] = $id_convocatoria;
            $insert['id_catalogo_combate_continua'] = $continua;
            $this->db->insert('convocatorias_has_catalogo_combate_continua',$insert);
        }return true;
    }

    private function insertar_pdf($id,$archivo){
        try{
            $insertar_pdf = array(
                'archivo_pdf_id_convocatoria' => $id,
                'archivo_pdf_ruta' => $archivo
            );
            $this->db->insert('archivo_pdf',$insertar_pdf);
        }catch (exception $e){
            return false;
        }
    }

    public function eliminar_convocatoria($id_convocatoria){
        $this->db->trans_start();

        $this->db->where('id_convocatorias',$id_convocatoria);
        $this->db->delete('convocatorias_has_catalogo_campeonato');

        $this->db->where('id_convocatorias',$id_convocatoria);
        $this->db->delete('convocatorias_has_catalogo_formas');

        $this->db->where('id_convocatorias',$id_convocatoria);
        $this->db->delete('convocatorias_has_catalogo_categorias');

        $this->db->where('id_convocatorias',$id_convocatoria);
        $this->db->delete('convocatorias_has_catalogo_ramas');

        $this->db->where('id_convocatorias',$id_convocatoria);
        $this->db->delete('convocatorias_has_catalogo_tipos_competicion');

        $this->db->where('id_convocatorias',$id_convocatoria);
        $this->db->delete('convocatorias_has_catalogo_combates');

        $this->db->where('id_convocatorias',$id_convocatoria);
        $this->db->delete('convocatorias_has_catalogo_combate_continua');

        $this->db->where('archivo_pdf_id_convocatoria',$id_convocatoria);
        $this->db->delete('archivo_pdf');

        $this->db->where('id_convocatoria', $id_convocatoria);
        $this->db->delete('convocatorias');

        $this->db->trans_complete();
        return true;
    }
}