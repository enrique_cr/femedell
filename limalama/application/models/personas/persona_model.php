<?php
/**
 * Created by PhpStorm->
 * User: toshiba Amilcar Sosa
 * Date: 08/02/2019
 * Time: 12:26 PM
 */

class persona_model extends CI_Model
{
    public function get_personas($busqueda = false)
    {
        $this->db->select("p.id_persona,nombre,
         p.apellido_paterno,p.apellido_materno,
         p.curp,
         cc.nombre_catalogo_cintas,
         c.correo,
         t.numero_telefono,
         p.nombre_responsable,
         p.apellido_paterno_responsable,
         p.apellido_materno_responsable,
         p.telefono_responsable,
         ce.nombre_catalogo_estatus,
         cr.nombre_catalogo_roles")
            ->from("persona p")
            ->join("correos c", "p.id_persona = c.id_persona")
            ->join("telefonos t", "p.id_persona = t.id_persona")
            ->join("rol_asignacion r", "p.id_persona = r.id_persona")
            ->join("catalogo_estatus ce", "p.id_catalgo_estatus = ce.id_catalogo_estatus")
            ->join("catalogo_roles cr", "r.id_catalogo_roles = cr.id_catalogo_roles")
            ->join("catalogo_cintas cc", "p.id_catalogo_cintas = cc.id_catalogo_cintas");
        if ($busqueda!= false){
            $this->db->like('p.curp',$busqueda['palabra_buscar']);
        }
        $resultados = $this->db->get();
        return $resultados->result();
    }

    public function get_persona_id($id)
    {
        $this->db->select("p.*,
        c.correo,
         t.numero_telefono,
         d.*,
         f.url,
         r.id_catalogo_roles")
            ->from("persona p")
            ->join("correos c", "p.id_persona = c.id_persona")
            ->join("telefonos t", "p.id_persona = t.id_persona")
            ->join("rol_asignacion r", "p.id_persona = r.id_persona")
            ->join("domicilios d", "p.id_domicilio = d.id_domicilio")
            ->join("fotografias f","p.id_persona = f.id_persona")
            ->where("p.id_persona", $id);
        $resultado = $this->db->get();
        return $resultado->row();
    }

    public function get_persona_curp($curp)
    {
        $this->db->select("p.*,
        c.correo,
         t.numero_telefono,
         d.*,
         f.url,
         r.id_catalogo_roles,
         ce.nombre as club")
            ->from("persona p")
            ->join("correos c", "p.id_persona = c.id_persona")
            ->join("telefonos t", "p.id_persona = t.id_persona")
            ->join("rol_asignacion r", "p.id_persona = r.id_persona")
            ->join("domicilios d", "p.id_domicilio = d.id_domicilio")
            ->join("fotografias f", "p.id_persona = f.id_persona")
            ->join("clubes_escuelas ce", "p.id_clubes_escuelas = ce.id_clubes_escuelas","left")
            ->where("p.curp", $curp);
        $resultado = $this->db->get();

        if ($resultado->num_rows == null){
            $this->db->select("p.*,
         d.*,
         f.url,
         ce.nombre as club")
                ->from("persona p")
                ->join("domicilios d", "p.id_domicilio = d.id_domicilio")
                ->join("fotografias f", "p.id_persona = f.id_persona")
                ->join("clubes_escuelas ce", "p.id_clubes_escuelas = ce.id_clubes_escuelas","left")
                ->where("p.curp", $curp);
            $resultado = $this->db->get();
        }
        return $resultado->row();
    }

    public function guardar_persona($datos)
    {
        try {
            $this->db->trans_start();
            $domicilio = array(
                'calle' => isset($datos['calle_persona'])?$datos['calle_persona']:null,
                'id_estado' => isset($datos['id_estado'])?$datos['id_estado']:null,
                'id_municipio' => isset($datos['id_municipio'])?$datos['id_municipio']:null,
                'id_localidad' => isset($datos['id_localidad'])?$datos['id_localidad']:null,
                'numero_exterior' => isset($datos['numero_domicilio_persona'])?$datos['numero_domicilio_persona']:null,
                'codigo_postal' => 90114
            );
            $this->db->insert('domicilios', $domicilio);
            $id_domicilio = $this->db->insert_id();
            $persona = array(
                'nombre' => isset($datos['nombre_persona'])?$datos['nombre_persona']:null,
                'apellido_paterno' => isset($datos['apellido_paterno'])?$datos['apellido_paterno']:null,
                'apellido_materno' => isset($datos['apellido_materno'])?$datos['apellido_materno']:null,
                'curp' => isset($datos['curp'])?$datos['curp']:null,
                'sexo' => isset($datos['id_sexo'])?$datos['id_sexo']:null,
                'id_domicilio' => $id_domicilio,
                'id_catalogo_escolaridad' => isset($datos['escolaridad_persona'])?$datos['escolaridad_persona']:null,
                'id_catalgo_estatus' => isset($datos['estatus_persona'])?$datos['estatus_persona']:1,
                'id_catalogo_cintas' => isset($datos['grado_persona'])?$datos['grado_persona']:null,
                'fecha_grado' => isset($datos['fecha_grado_persona'])?$datos['fecha_grado_persona']:null,
                'id_clubes_escuelas' => isset($datos['club_persona'])?$datos['club_persona']:null,
                'id_instructor' => isset($datos['instructor_personsa'])?$datos['instructor_personsa']:'',
                'fecha_nacimineto' => isset($datos['fecha_nacimiento_persona'])?$datos['fecha_nacimiento_persona']:null,
                'peso' => isset($datos['peso_persona'])?$datos['peso_persona']:null,
                'estatura' => isset($datos['altura_persona'])?$datos['altura_persona']:null,
                'id_catalogo_tipo_sangre' => isset($datos['sangre_persona'])?$datos['sangre_persona']:null,
                'alergias' => isset($datos['alergias_persona'])?$datos['alergias_persona']:null,
                'nombre_responsable' => isset($datos['nombre_contacto'])?$datos['nombre_contacto']:null,
                'apellido_paterno_responsable' => isset($datos['apellido_paterno_contacto'])?$datos['apellido_paterno_contacto']:null,
                'apellido_materno_responsable' => isset($datos['apellido_materno_contacto'])?$datos['apellido_materno_contacto']:null,
                'id_parentesco_responsable' => isset($datos['parentesco_contacto'])?$datos['parentesco_contacto']:null,
                'telefono_responsable' => isset($datos['telefono_contacto'])?$datos['telefono_contacto']:null,
                'fecha_alta' => date('Y-m-d')
            );
            $this->db->insert('persona', $persona);
            $id_persona = $this->db->insert_id();
            $correo = array(
                'id_persona' => $id_persona,
                'correo' => isset($datos['email_persona'])?$datos['email_persona']:null
            );
            $this->db->insert('correos', $correo);
            $telefonos = array(
                'numero_telefono' => isset($datos['telefono_persona'])?$datos['telefono_persona']:null,
                'id_persona' => $id_persona
            );
            $this->db->insert('telefonos', $telefonos);

            $rol = array(
                'id_catalogo_roles' => isset($datos['rol_persona'])?$datos['rol_persona']:null,
                'id_persona' => $id_persona
            );
            $this->db->insert('rol_asignacion', $rol);
            $fotografias = array(
                'url' => isset($datos['url'])?$datos['url']:'',
                'id_persona' => $id_persona
            );
            $this->db->insert('fotografias', $fotografias);
            $this->db->trans_complete();
            return 'Guardado con exito';
        } catch (Exception $e) {
            $this->db->trans_rollback();
            return 'Error al guardar';
        }
    }
    public function actualizar_persona($id_persona,$datos)
    {
        try {
            $this->db->trans_start();

            $consulta = "
            	UPDATE 
                persona p, 
                domicilios dom 
                SET
                dom.calle = '".$datos['calle_persona'] ."', 
                dom.id_estado = '".$datos['id_estado'] ."',
                dom.id_municipio = '".$datos['id_municipio'] ."',
                dom.id_localidad = '".$datos['id_localidad'] ."',
                dom.numero_exterior = '".$datos['numero_domicilio_persona'] ."'
                WHERE 
                p.id_persona = '".$id_persona."' and 
                p.id_domicilio=dom.id_domicilio;
            ";
            $this->db->query($consulta);
            $persona = array(
                'nombre' => $datos['nombre_persona'],
                'apellido_paterno' => $datos['apellido_paterno'],
                'apellido_materno' => $datos['apellido_materno'],
                'curp' => $datos['curp'],
                'sexo' => isset($datos['id_sexo'])?$datos['id_sexo']:null,
                'id_catalogo_escolaridad' => $datos['escolaridad_persona'],
                'id_catalgo_estatus' => $datos['estatus_persona'],
                'id_catalogo_cintas' => $datos['grado_persona'],
                'fecha_grado' => $datos['fecha_grado_persona'],
                'id_clubes_escuelas' => $datos['club_persona'],
                'id_instructor' => 1,
                'fecha_nacimineto' => $datos['fecha_nacimiento_persona'],
                'peso' => $datos['peso_persona'],
                'estatura' => $datos['altura_persona'],
                'id_catalogo_tipo_sangre' => $datos['sangre_persona'],
                'alergias' => $datos['alergias_persona'],
                'nombre_responsable' => $datos['nombre_contacto'],
                'apellido_paterno_responsable' => $datos['apellido_paterno_contacto'],
                'apellido_materno_responsable' => $datos['apellido_materno_contacto'],
                'id_parentesco_responsable' => $datos['parentesco_contacto'],
                'telefono_responsable' => $datos['telefono_contacto'],
            );
            $this->db->where('id_persona',$id_persona);
            $this->db->update('persona',$persona);
//            $id_persona = $this->db->insert_id();
            $correo = array(
//                'id_persona' => $id_persona,
                'correo' => $datos['email_persona']
            );
            $this->db->where('id_persona',$id_persona);
            $this->db->update('correos', $correo);
            $telefonos = array(
                'numero_telefono' => $datos['telefono_persona'],
//                'id_persona' => $id_persona
            );
            $this->db->where('id_persona',$id_persona);
            $this->db->update('telefonos', $telefonos);


            if( isset($datos['url'])) {
                $fotografias = array(
                    'url' => $datos['url'],
//                'id_persona' => $id_persona
                );
                $this->db->where('id_persona', $id_persona);
                $this->db->update('fotografias', $fotografias);
            }
            $rol = array(
                'id_catalogo_roles' => $datos['rol_persona'],
//                'id_persona' => $id_persona
            );
            $this->db->where('id_persona',$id_persona);
            $this->db->update('rol_asignacion', $rol);

            $this->db->trans_complete();
            return 'Guardado con exito';
        } catch (Exception $e) {
            $this->db->trans_rollback();
            return 'Error al guardar';
        }
    }
    public function eliminar_persona($id_persona)
    {
        try {
            $this->db->trans_start();

//            $this->db->delete('domicilios');
            $this->db->where('id_persona',$id_persona);
            $this->db->delete('correos');
            $this->db->where('id_persona',$id_persona);
            $this->db->delete('telefonos');
            $this->db->where('id_persona',$id_persona);
            $this->db->delete('rol_asignacion');
            $this->db->where('id_persona',$id_persona);
            $this->db->delete('fotografias');
            $this->db->where('id_persona',$id_persona);
            $this->db->delete('persona');

            $this->db->trans_complete();
            return 'Eliminado con exito';
        } catch (Exception $e) {
            $this->db->trans_rollback();
            return 'Error al Eliminar';
        }

    }
    public function obtener_catalogo_municipios_by_id_estado($id_estado){
        $query= $this->db->query("SELECT * FROM municipios m where m.estado_id=$id_estado");

        return $query->result_array();
    }
}