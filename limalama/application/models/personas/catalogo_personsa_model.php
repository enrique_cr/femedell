<?php
/**
 * Created by PhpStorm.
 * User: toshiba Amilcar Sosa
 * Date: 07/02/2019
 * Time: 11:39 PM
 */

class catalogo_personsa_model extends CI_Model
{
    public function get_estados(){
        $estados = $this->db->get('estados');
        return $estados->result_array();
    }
    public function get_clubes(){
        $clubes = $this->db->get('clubes_escuelas');
        return $clubes->result();
    }
    public function get_instructores(){
        $this->db->select('p.id_persona, p.nombre')
            ->from('persona p')
            ->join('rol_asignacion r','r.id_persona = p.id_persona')
            ->where('r.id_catalogo_roles',2);
        $datos = $this->db->get();
        return $datos->result();
    }
    public function get_instructores_id($id){
        $this->db->select('p.id_persona, p.nombre')
            ->from('persona p')
            ->join('rol_asignacion r','r.id_persona = p.id_persona')
            ->where('r.id_catalogo_roles',2)
            ->where('p.id_persona',$id);
        $datos = $this->db->get();
        return $datos->result();
    }
    public function get_escolaridad(){
        $escolaridades = $this->db->get('catalogo_escolaridad');
        return $escolaridades->result();
    }
    public function get_grado(){
        $grados = $this->db->get('catalogo_cintas');
        return $grados->result();
    }
    public function get_estatus(){
        $estatus = $this->db->get('catalogo_estatus');
        return $estatus->result();
    }
    public function get_tipo_sangre()
    {
        $tipo_sangre = $this->db->get('catalogo_tipo_sangre');
        return $tipo_sangre->result();
    }
    public function get_parentesco()
    {
        $parentesco = $this->db->get('catalogo_parentescos');
        return $parentesco->result();
    }
    public function get_roles()
    {
        $roles = $this->db->get('catalogo_roles');
        return $roles->result();
    }
}