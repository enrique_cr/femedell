<?php
/**
 * Created by PhpStorm.
 * User: omar
 * Date: 2019-02-13
 * Time: 20:18
 */

defined('BASEPATH') OR exit('No direct script access allowed');
class registro_model extends CI_Model {
    function __construct(){
        parent::__construct();
    }

    public function verificar_curp($curp){
        $query = $this->db->query("SELECT * FROM persona WHERE curp like '".$curp."';");

        if($query->num_rows() == 1){
            return true;
        }
        else{
            return false;
        }
    }

    public function verificar_curp_competencia($curp){
        $query = $this->db->query("SELECT * FROM persona p WHERE p.curp like '".$curp."' and p.procedencia_registro=2");

        if($query->num_rows() == 1){
            return true;
        }
        else{
            return false;
        }
    }
    public function guardar_datos_pre_registro($datos){

        $datos['id_tipo_continua'] = explode(',', $datos['select_id_tipo']);
        $datos['id_combate'] = explode(',', $datos['select_id_combate']);
        $datos['id_forma'] = explode(',', $datos['select_id_forma']);

        if (isset($datos['ids_alumnos']) && $datos['ids_alumnos'] != '') {
            $datos['id_alumnos'] = explode(',', $datos['ids_alumnos']);
        } else {
            $datos['id_alumnos'][] = $datos['id_persona'];
        }
        foreach ($datos['id_alumnos'] as $i =>$id_persona) {

            $datos['id_persona'] = $id_persona;
            $insert_id_persona = $datos['id_persona'];

            $this->db->trans_start();

            if ($datos['id_persona'] == '' || $datos['id_persona'] == null) {
                $domicilio = array(
                    'id_estado' => $datos['id_estado'],
                    'id_municipio' => $datos['id_municipio'],
                    'id_localidad' => $datos['id_localidad']
                );
                $this->db->insert('domicilios', $domicilio);
                $insert_id_domicilio = $this->db->insert_id();

                $persona = array(
                    'nombre' => $datos['nombre_persona'],
                    'apellido_paterno' => $datos['apellido_paterno'],
                    'apellido_materno' => $datos['apellido_materno'],
                    'curp' => $datos['curp'],
                    'id_domicilio' => $insert_id_domicilio,
                    'sexo' => $datos['id_sexo'],
                    'fecha_nacimineto' => $datos['nacimiento_invitado'],
                    'peso' => $datos['peso_persona'],
                    'estatura' => $datos['altura_persona'],
                    'id_catalogo_cintas' => $datos['id_cinturon'],
                    'club_escuela_otro' => $datos['escuela_invitado'],
                    'instructor_otro' => $datos['profesor_invitado'],
                    'fecha_alta' => date('Y-m-d'),
                    'procedencia_registro' => 2
                );
                $this->db->insert('persona', $persona);
                $insert_id_persona = $this->db->insert_id();

                $fotografias = array(
                    'url' => isset($datos['url']) ? $datos['url'] : '',
                    'id_persona' => $insert_id_persona
                );
                $this->db->insert('fotografias', $fotografias);
            }
            $clasifica = array(
                'id_persona' => $insert_id_persona,
                'id_catalogo_ramas' => $datos['id_ramas'],
                'id_catalogo_tipos_competicion' => $datos['id_tipos_competencia'],
                'id_catalogo_categorias' => 2,
                'fecha' => date('Y-m-d')
            );
            $this->db->insert('competencia_clasifica', $clasifica);

            $pagos = array(
                'id_persona' => $insert_id_persona,
                'costo' => $datos['costo_combate_total'],
                'id_forma_pago' => $datos['id_forma_pago'],
                'id_banco' => isset($datos['id_banco'])?$datos['id_banco']:null,
                'fecha_alta' => date('Y-m-d')
            );
            $this->db->insert('competencia_pagos', $pagos);

            foreach ($datos['id_tipo_continua'] as $tipo) {
                $competencia_tipo = array(
                    'id_persona' => $insert_id_persona,
                    'id_competencia_tipo' => $tipo,
                    'fecha_alta' => date('Y-m-d')
                );
                $this->db->insert('persona_has_competencia_tipo', $competencia_tipo);
            }

            foreach ($datos['id_combate'] as $combate) {
                if ($combate != '') {
                    $competencia_combate = array(
                        'id_persona' => $insert_id_persona,
                        'id_catalogo_combates' => $combate,
                        'fecha_alta' => date('Y-m-d')
                    );
                    $this->db->insert('persona_has_combate', $competencia_combate);
                }
            }

            foreach ($datos['id_forma'] as $forma) {
                if ($forma != '') {
                    $competencia_forma = array(
                        'id_persona' => $insert_id_persona,
                        'id_catalogo_formas' => $forma,
                        'fecha_alta' => date('Y-m-d')
                    );
                    $this->db->insert('persona_has_formas', $competencia_forma);
                }
            }
            $persona_pre_registro = array(
                'id_persona' => $insert_id_persona,
                'id_convocatoria' => null,
                'fecha_registro' => date('Y-m-d H:m:s')
            );
            $this->db->insert('persona_pre_registro', $persona_pre_registro);

            $retorno[$i]=$insert_id_persona;
            $this->db->trans_complete();
            $this->db->trans_commit();
        }
        return $retorno;
    }

    public function obtener_datos_persona_pre_registro($id_registro){
        $consulta = "
            SELECT
            p.nombre as participante,
            foto.*,
            p.*,
            ce.nombre as club,
            ppr.*,
            dom.*,
            es.nombre as estado,
            mun.nombre as municipio,
            loca.nombre as localidad,
            ces.*,
            caci.nombre_catalogo_cintas,
            cocla.*,
            copa.*,
            caba.*,
            cafopa.forma_pago
            FROM 
            persona_pre_registro ppr 
            INNER JOIN persona p on p.id_persona = ppr.id_persona
            INNER JOIN fotografias foto on foto.id_persona =p.id_persona
            INNER JOIN domicilios dom on dom.id_domicilio = p.id_domicilio
            INNER JOIN estados es on es.id = dom.id_estado
            INNER JOIN municipios mun on mun.id = dom.id_municipio
            INNER JOIN localidades loca on loca.id = dom.id_localidad
            LEFT JOIN catalogo_escolaridad ces on ces.id_catalogo_escolaridad = p.id_catalogo_escolaridad
            LEFT JOIN clubes_escuelas ce on ce.id_clubes_escuelas = p.id_clubes_escuelas
            INNER JOIN catalogo_cintas caci on caci.id_catalogo_cintas = p.id_catalogo_cintas
            INNER JOIN competencia_clasifica cocla on cocla.id_persona = p.id_persona
            INNER JOIN competencia_pagos copa on copa.id_persona=p.id_persona
            INNER JOIN catalogo_formas_pago cafopa on cafopa.id_catalogo_formas_pago = copa.id_forma_pago
            LEFT JOIN catalogo_bancos caba on caba.id_catalogo_bancos = copa.id_banco
            WHERE ppr.id_persona = $id_registro ;
        ";

        $query= $this->db->query($consulta);


        return $query->row_array();
    }

    public function obtener_datos_competencia_persona($id_registro){
        $consulta =" SELECT 
            phct.*,
            cct.*
            FROM persona_has_competencia_tipo phct
            INNER JOIN catalogo_competencia_tipo cct on cct.id_catalogo_competencia_tipo = phct.id_competencia_tipo
            where phct.id_persona=$id_registro ;";

        $query=$this->db->query($consulta);

        return $query->result_array();

    }
    public function obtener_datos_combate_persona($id_registro){
        $consulta = " SELECT 
            phc.*,
            cc.*
            FROM persona_has_combate phc
            INNER JOIN catalogo_combates cc on cc.id_catalogo_combates = phc.id_catalogo_combates
            WHERE phc.id_persona=$id_registro;
        ";
        $query = $this->db->query($consulta);

        return $query->result_array();
    }

    public function obtener_datos_formas_persona($id_registro){
        $consulta = " SELECT 
            phf.*,
            cf.*
            FROM persona_has_formas phf
            INNER JOIN catalogo_formas cf on cf.id_catalogo_modalidades = phf.id_catalogo_formas
            WHERE phf.id_persona=$id_registro ;";

        $query = $this->db->query($consulta);

        return $query->result_array();
    }

    public function obtener_catalogo_bancos(){
        $query = $this->db->get('catalogo_bancos');

        return $query->result_array();
    }
    public function get_alumnos($id_estado){
        $this->db->select("p.*,
        c.correo,
         t.numero_telefono,
         cc.nombre_catalogo_cintas,
         d.*,
         f.url,
         r.id_catalogo_roles,
         ce.nombre_catalogo_estatus,
         cr.nombre_catalogo_roles")
            ->from("persona p")
            ->join("correos c", "p.id_persona = c.id_persona")
            ->join("telefonos t", "p.id_persona = t.id_persona")
            ->join("rol_asignacion r", "p.id_persona = r.id_persona")
            ->join("domicilios d", "p.id_domicilio = d.id_domicilio")
            ->join("catalogo_cintas cc", "p.id_catalogo_cintas = cc.id_catalogo_cintas")
            ->join("catalogo_estatus ce", "p.id_catalgo_estatus = ce.id_catalogo_estatus")
            ->join("fotografias f","p.id_persona = f.id_persona")
            ->join("catalogo_roles cr", "r.id_catalogo_roles = cr.id_catalogo_roles")
            ->where("d.id_estado", $id_estado['id_estado']);
        $resultado = $this->db->get();
        return $resultado->result();
    }
    public function verificar_pre_registro_alumnos($datos){
        if(isset($datos['ids_alumnos']) && $datos['ids_alumnos']!=''){
            $datos['id_alumnos'] = explode(',', $datos['ids_alumnos']);

            foreach ($datos['id_alumnos'] as $id_alumno){
                $this->db->trans_start();
                $query = $this->db->query("select p.nombre, p.apellido_paterno, p.apellido_materno, p.curp  
                  from persona_pre_registro ppr 
                  INNER JOIN persona p on ppr.id_persona = p.id_persona 
                  WHERE ppr.id_persona=$id_alumno");
                if($query->num_rows()>0){
                    $retorno['datos'][] = $query->result_array();
                    $retorno['status'] = true;
                    $retorno['error'] = 0;
                    break;
                    return $retorno;

                }else{
                    $retorno['datos'] = null;
                    $retorno['status'] = false;
                    $retorno['error'] = 0;

                }
                $this->db->trans_complete();
            }
        }else{
            $retorno['status'] = true;
            $retorno['error'] = 1;
            $retorno['datos'] = null;
        }
        return $retorno;
    }

    public function verificar_pre_registro_persona($id_persona){
        if($id_persona != '' || $id_persona!=null){
            $query = $this->db->query("select * 
                from persona_pre_registro ppr 
                where ppr.id_persona = $id_persona;");

            if($query->num_rows() > 0){
                return false;
            }
            else{
                return true;
            }
        }else{
            return true;
        }
    }
}