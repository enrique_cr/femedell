<?php
/**
 * Created by PhpStorm.
 * User: omar
 * Date: 2019-02-17
 * Time: 10:14
 */

defined('BASEPATH') OR exit('No direct script access allowed');
class grafica_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    public function obtener_competidores(){
        $consulta = "
            SELECT 
            p.nombre,
            f.url
            FROM
            persona_pre_registro prr INNER JOIN persona p on p.id_persona=prr.id_persona
            LEFT JOIN fotografias f on f.id_persona=p.id_persona;
        ";
        $query= $this->db->query($consulta);
        $re=array();
        foreach ($query->result_array() as $index=> $dato){
            $re[$index]['nombre']=$dato['nombre'];
            //$re[$index]['foto']=base_url().'assets/img/users_img/'.$dato['url'];
            $re[$index]['foto']=base_url().'assets/img/users_img/'.'cuadro.png';
            $re[$index]['id_grafica']=1;//Valor de ejemplo...
        }

        return $re;
    }
    public function guardar_imagen_grafico_combates($ruta, $nombre, $id_grafico){
        if(!$this->verificar_id_grafico($id_grafico)){
            $insertar = array(
                'id_grafico' => $id_grafico,
                'ruta_imagen' => $ruta,
                'nombre_archivo' => $nombre
            );
            if($this->db->insert('imagenes_graficos', $insertar)){
                return true;
            }
        }
        return false;
    }
    public function verificar_id_grafico($id_grafico){
        $query=$this->db->query("SELECT * FROM imagenes_graficos WHERE id_grafico = $id_grafico");
        if($query->num_rows()>0){
            return true;
        }
        return false;
    }
}