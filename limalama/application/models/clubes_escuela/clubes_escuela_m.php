<?php
/**
 * Created by PhpStorm.
 * User: omar
 * Date: 2019-02-08
 * Time: 13:03
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Clubes_escuela_m extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    public function agregar_modificar_clubes_escuelas($datos){
        $datos['fecha_alta']=date('Y-m-d');
        if($datos['id_clubes_escuelas']<>'' || $datos['id_clubes_escuelas']!=null){//actualizar
            $consulta = "
            	UPDATE 
                clubes_escuelas ce, 
                domicilios dom 
                SET
                ce.folio = '".$datos['folio'] ."', 
                ce.nombre = '".$datos['nombre'] ."', 
                dom.calle = '".$datos['calle'] ."', 
                dom.id_estado = '".$datos['id_estado'] ."',
                dom.id_municipio = '".$datos['id_municipio'] ."',
                dom.id_localidad = '".$datos['id_localidad'] ."',
                dom.numero_exterior = '".$datos['numero_exterior'] ."'
                WHERE 
                ce.id_clubes_escuelas = '".$datos['id_clubes_escuelas'] ."' and 
                ce.id_domicilio=dom.id_domicilio;
            ";
            $this->db->query($consulta);
            return true;
        }else{
            $insertar_domicilio = array(
                'calle' => $datos['calle'],
                'id_estado' => $datos['id_estado'],
                'id_municipio' => $datos['id_municipio'],
                'id_localidad' => $datos['id_localidad'],
                'numero_exterior' => $datos['numero_exterior']
            );
            $this->db->trans_start();
            $this->db->insert('domicilios',$insertar_domicilio);
            $insert_id = $this->db->insert_id();

            $insertar = array(
                'folio' => $datos['folio'],
                'nombre' => $datos['nombre'],
                'id_domicilio' =>$insert_id,
                'fecha' => $datos['fecha_alta']
            );
            $this->db->insert('clubes_escuelas',$insertar);
            $this->db->trans_complete();
        }
            return true;
    }
    public function consultar_clubes_escuelas($palabra_buscar=null){
        if($palabra_buscar =='' || $palabra_buscar==null){
            $query=$this->db->query("
                SELECT 
                    ce.id_clubes_escuelas id_clubes_escuelas,
                    ce.folio,folio,
                    ce.nombre nombre,
                    dom.calle calle,
                    dom.numero_exterior numero_exterior,
                    es.nombre estado,
                    mun.nombre municipio,
                    lo.nombre localidad
                FROM
                    clubes_escuelas ce,
                    domicilios dom,
                    estados es,
                    municipios mun,
                    localidades lo
                WHERE
                    ce.id_domicilio = dom.id_domicilio
                        AND es.id = dom.id_estado
                        AND mun.id = dom.id_municipio
                        AND lo.id = dom.id_localidad;
          ");
        }else{
            $query=$this->db->query("
                SELECT 
                    ce.id_clubes_escuelas id_clubes_escuelas,
                    ce.folio,folio,
                    ce.nombre nombre,
                    dom.calle calle,
                    dom.numero_exterior numero_exterior,
                    es.nombre estado,
                    mun.nombre municipio,
                    lo.nombre localidad
                FROM
                    clubes_escuelas ce,
                    domicilios dom,
                    estados es,
                    municipios mun,
                    localidades lo
                WHERE
                    ce.id_domicilio = dom.id_domicilio
                        AND es.id = dom.id_estado
                        AND mun.id = dom.id_municipio
                        AND lo.id = dom.id_localidad
                        AND ce.nombre LIKE '%".$palabra_buscar."%';
          ");
        }
        return $query->result_array();
    }

    public function consultar_clubes_escuelas_by_id($id_clubes_escuelas){
        $query=$this->db->query("
                SELECT 
                    ce.id_clubes_escuelas id_clubes_escuelas,
                    ce.folio,folio,
                    ce.nombre nombre,
                    dom.calle calle,
                    dom.numero_exterior numero_exterior,
                    es.nombre estado,
                    es.id id_estado,
                    mun.id id_municipio,
                    lo.id id_localidad,
                    mun.nombre municipio,
                    lo.nombre localidad
                FROM
                    clubes_escuelas ce,
                    domicilios dom,
                    estados es,
                    municipios mun,
                    localidades lo
                WHERE
                    ce.id_domicilio = dom.id_domicilio
                        AND es.id = dom.id_estado
                        AND mun.id = dom.id_municipio
                        AND lo.id = dom.id_localidad
                        AND ce.id_clubes_escuelas = $id_clubes_escuelas
          ");

        return $query->row_array();
    }

    public function eliminar_clubes_escuelas($id_clubes_escuelas){
        $query = $this->db->query("SELECT id_domicilio FROM clubes_escuelas WHERE id_clubes_escuelas = $id_clubes_escuelas");
        $domicilio=$query->row_array();
        $this->db->trans_start();

        $this->db->where('id_clubes_escuelas', $id_clubes_escuelas);
        $this->db->delete('clubes_escuelas');

        if($query->num_rows()>0){
            $this->db->where('id_domicilio', $domicilio['id_domicilio']);
            $this->db->delete('domicilios');
        }

        $this->db->trans_complete();
        return true;
    }

    public function obtener_catalogo_estados(){
        $query=$this->db->query("SELECT * FROM estados");

        return $query->result_array();
    }

    public function obtener_catalogo_municipios_by_id_estado($id_estado){
        $query= $this->db->query("SELECT * FROM municipios m where m.estado_id=$id_estado");

        return $query->result_array();
    }
    public function obtener_catalogo_localidades_by_id_municipio($id_municipio){
      $query=$this->db->query("SELECT * FROM localidades l where l.municipio_id=$id_municipio");

      return $query->result_array();
    }
}