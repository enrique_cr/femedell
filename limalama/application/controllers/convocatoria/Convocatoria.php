<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Convocatoria extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('convocatoria/modelo_convocatorias');
    }

    public function index(){
        $datos_vista['scripts']= array(base_url().'assets/js/convocatoria.js');

        $datos_vista['catalogo_nivel_convocatoria'] = $this->modelo_convocatorias->obtener_catalogo_nivel_convocatoria();
        $datos_vista['catalogo_campeonato'] = $this->modelo_convocatorias->obtener_catalogo_campeonato();
        $datos_vista['catalogo_categorias'] = $this->modelo_convocatorias->obtener_catalogo_categorias();
        $datos_vista['catalogo_formas'] = $this->modelo_convocatorias->obtener_catalogo_formas();
        $datos_vista['catalogo_ramas'] = $this->modelo_convocatorias->obtener_catalogo_ramas();
        $datos_vista['catalogo_tipos_competicion'] = $this->modelo_convocatorias->obtener_catalogo_tipos_competicion();
        $datos_vista['catalogo_combates'] = $this->modelo_convocatorias->obtener_catalogo_combates();
        $datos_vista['catalogo_combate_continua'] = $this->modelo_convocatorias->obtener_catalogo_combate_continua();
        $datos_vista['catalogo_estados'] = $this->modelo_convocatorias->obtener_catalogo_estados();
        $datos_vista['convocatorias'] = $this->modelo_convocatorias->obtener_convocatorias();

        $this->load->view('convocatorias/catalogo_convocatoria',$datos_vista);
    }

    public function consultar_convocatorias(){
        if(isset($_POST['palabra_buscar'])){
            $palabra_buscar=$_POST['palabra_buscar'];
        }
        if($palabra_buscar == '' || $palabra_buscar == null){
            $datos_vista['convocatorias'] = $this->modelo_convocatorias->obtener_convocatorias();
            //var_dump($datos_vista);exit();
        }
        else{
            $datos_vista['convocatorias'] = $this->modelo_convocatorias->obtener_convocatorias($palabra_buscar);
        }
//       var_dump($datos_vista);exit();
        $this->load->view('convocatorias/tabla_convocatorias',$datos_vista);
    }

    public function abrir_modal($id=null){


        if($id!=null){
            $this->editar_convocatoria($id);
            //var_dump($datos_vista);exit;
        }else{
            $datos_vista['catalogo_nivel_convocatoria'] = $this->modelo_convocatorias->obtener_catalogo_nivel_convocatoria();
            $datos_vista['catalogo_campeonato'] = $this->modelo_convocatorias->obtener_catalogo_campeonato();
            $datos_vista['catalogo_categorias'] = $this->modelo_convocatorias->obtener_catalogo_categorias();
            $datos_vista['catalogo_formas'] = $this->modelo_convocatorias->obtener_catalogo_formas();
            $datos_vista['catalogo_ramas'] = $this->modelo_convocatorias->obtener_catalogo_ramas();
            $datos_vista['catalogo_tipos_competicion'] = $this->modelo_convocatorias->obtener_catalogo_tipos_competicion();
            $datos_vista['catalogo_combates'] = $this->modelo_convocatorias->obtener_catalogo_combates();
            $datos_vista['catalogo_combate_continua'] = $this->modelo_convocatorias->obtener_catalogo_combate_continua();
            $datos_vista['catalogo_estados'] = $this->modelo_convocatorias->obtener_catalogo_estados();
            $datos_vista['convocatorias'] = $this->modelo_convocatorias->obtener_convocatorias();

            $this->load->view('convocatorias/modal/modal_cat_convocatoria',$datos_vista);
        }

    }

    public function consultar_convocatoria_por_id(){
        $id_convocatoria=$_POST['id_convocatoria'];
        $datos_vista['catalogo_campeonato'] = $this->modelo_convocatorias->obtener_catalogo_campeonato();
        $datos_vista['catalogo_formas'] = $this->modelo_convocatorias->obtener_catalogo_formas();
        $datos_vista['catalogo_categorias'] = $this->modelo_convocatorias->obtener_catalogo_categorias();
        $datos_vista['catalogo_ramas'] = $this->modelo_convocatorias->obtener_catalogo_ramas();
        $datos_vista['catalogo_tipos_competicion'] = $this->modelo_convocatorias->obtener_catalogo_tipos_competicion();
        $datos_vista['catalogo_combates'] = $this->modelo_convocatorias->obtener_catalogo_combates();
        $datos_vista['catalogo_combate_continua'] = $this->modelo_convocatorias->obtener_catalogo_combate_continua();
        $datos_vista['catalogo_estados'] = $this->modelo_convocatorias->obtener_catalogo_estados();
        $datos_vista['convocatoria']=$this->modelo_convocatorias->obtener_convocatoria_by_id($id_convocatoria);
        $this->load->view('convocatorias/modal/modal_cat_convocatoria',$datos_vista);
    }

    public function agregar_modificar_convocatoria(){
        $post = $this->input->post();
//        var_dump($post);exit;
        //pdf
        $archivo = $_FILES['nombre_archivo'];
        $config['upload_path'] = "assets/img/pdf_convocatoria";
        $config['file_name'] = str_replace(' ', '_', $archivo['name']);
        $config['allowed_types'] = "*";
        $config['max_size'] = "50000";
        $config['max_width'] = "2000";
        $config['max_height'] = "2000";

        $this->load->library('upload',$config);

        if (!$this->upload->do_upload('nombre_archivo')){
            $datos_vista['uploadError'] = $this->upload->display_errors();
        }else{
            $datos_vista['uploadSuccess'] = $this->upload->data();
            $post['ruta']=base_url().'assets/img/pdf_convocatoria'.'/'.$config['file_name'];
        }

        if($this->modelo_convocatorias->guardar_convocatoria($post)){
                $respuesta['mensaje']="Datos guardados con èxito";
                $respuesta['estado']=true;
        } else {
            echo 'No se guardo uuu';
        }
        echo json_encode($respuesta);
    }

    public function editar_convocatoria($id_convocatoria){

        $datos_vista['catalogo_nivel_convocatoria'] = $this->modelo_convocatorias->obtener_catalogo_nivel_convocatoria();
        $datos_vista['catalogo_campeonato'] = $this->modelo_convocatorias->obtener_catalogo_campeonato();
        $datos_vista['catalogo_categorias'] = $this->modelo_convocatorias->obtener_catalogo_categorias();
        $datos_vista['catalogo_formas'] = $this->modelo_convocatorias->obtener_catalogo_formas();
        $datos_vista['catalogo_ramas'] = $this->modelo_convocatorias->obtener_catalogo_ramas();
        $datos_vista['catalogo_tipos_competicion'] = $this->modelo_convocatorias->obtener_catalogo_tipos_competicion();
        $datos_vista['catalogo_combates'] = $this->modelo_convocatorias->obtener_catalogo_combates();
        $datos_vista['catalogo_combate_continua'] = $this->modelo_convocatorias->obtener_catalogo_combate_continua();
        $datos_vista['catalogo_estados'] = $this->modelo_convocatorias->obtener_catalogo_estados();
        $datos_vista['convocatoria'] = $this->modelo_convocatorias->obtener_convocatoria_by_id($id_convocatoria);
        $datos_vista['convocatoria_has_campeonato'] = $this->modelo_convocatorias->obtener_convocatoria_has_campeonato($id_convocatoria);
        $datos_vista['convocatoria_has_campeonato_ids'] = array();
        $datos_vista['convocatoria_has_categorias'] = $this->modelo_convocatorias->obtener_convocatoria_has_categorias($id_convocatoria);
        $datos_vista['convocatoria_has_categorias_ids'] = array();
        $datos_vista['convocatoria_has_formas'] = $this->modelo_convocatorias->obtener_convocatoria_has_formas($id_convocatoria);
        $datos_vista['convocatoria_has_formas_ids'] = array();
        $datos_vista['convocatoria_has_ramas'] = $this->modelo_convocatorias->obtener_convocatoria_has_ramas($id_convocatoria);
        $datos_vista['convocatoria_has_ramas_ids'] = array();
        $datos_vista['convocatoria_has_tipos_competicion'] = $this->modelo_convocatorias->obtener_convocatoria_has_tipos_competicion($id_convocatoria);
        $datos_vista['convocatoria_has_tipos_competicion_ids'] = array();
        $datos_vista['convocatoria_has_combates'] = $this->modelo_convocatorias->obtener_convocatoria_has_combates($id_convocatoria);
        $datos_vista['convocatoria_has_combates_ids'] = array();
        $datos_vista['convocatoria_has_combate_continua'] = $this->modelo_convocatorias->obtener_convocatoria_has_combate_contiua($id_convocatoria);
        $datos_vista['convocatoria_has_combate_continua_ids'] = array();
//        var_dump($datos_vista['catalogo_nivel_convocatoria']);exit();

        foreach ($datos_vista['convocatoria_has_campeonato'] as $ccid){
            array_push($datos_vista['convocatoria_has_campeonato_ids'],$ccid->id_catalogo_campeonato);
        }

        foreach ($datos_vista['convocatoria_has_categorias'] as $ccid){
            array_push($datos_vista['convocatoria_has_categorias_ids'],$ccid->id_catalogo_categorias);
        }

        foreach ($datos_vista['convocatoria_has_formas'] as $cfid){
            array_push($datos_vista['convocatoria_has_formas_ids'],$cfid->id_catalogo_modalidades);
        }

        foreach ($datos_vista['convocatoria_has_ramas'] as $chr){
            array_push($datos_vista['convocatoria_has_ramas_ids'],$chr->id_catalogo_ramas);
        }

        foreach ($datos_vista['convocatoria_has_tipos_competicion'] as $ctcid){
            array_push($datos_vista['convocatoria_has_tipos_competicion_ids'],$ctcid->id_catalogo_tipos_competicion);
        }

        foreach ($datos_vista['convocatoria_has_combates'] as $ccid){
            array_push($datos_vista['convocatoria_has_combates_ids'],$ccid->id_catalogo_combates);
        }

        foreach ($datos_vista['convocatoria_has_combate_continua'] as $cccid){
            array_push($datos_vista['convocatoria_has_combate_continua_ids'],$cccid->id_catalogo_combate_continua);
        }


//        return $datos_vista;
        //var_dump($datos_vista);
        $this->load->view('convocatorias/modal/modal_cat_convocatoria',$datos_vista);
    }

    public function eliminar_convocatoria(){
        $id_convocatoria=$_POST['id_convocatoria'];

        if($this->modelo_convocatorias->eliminar_convocatoria($id_convocatoria)){
            $respuesta['mensaje']='Se Elimino Convocatoria';
            $respuesta['estado']=true;

            echo json_encode($respuesta);
        }
    }
}