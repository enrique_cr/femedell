<?php
/**
 * Created by PhpStorm.
 * User: omar
 * Date: 2019-02-13
 * Time: 12:15
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class registro extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('campeonato/registro_model');
        $this->load->model('personas/catalogo_personsa_model');
        $this->load->model('personas/persona_model');
        $this->load->library('Pdf');
    }

    public function index()
    {
        $data['scripts'] = array(
            base_url() . 'assets/js/campeonato.js'
        );
        $this->load->view('campeonato/pre_registro', $data);
    }

    public function verificar_curp()
    {
        $curp = $_POST['curp'];

        if ($this->registro_model->verificar_curp($curp)) {
            $respuesta['estatus'] = true;
            $respuesta['mensaje'] = "Verificacion con éxito";
        } else {
            $respuesta['estatus'] = false;
            $respuesta['mensaje'] = "No se encontro el curp en el sistema";
        }

        echo json_encode($respuesta);
    }

    public function obtener_formularios_registro()
    {
        $curp = $_POST['curp'];
        $id_tipo = $_POST['id_tipo'];

        $data['catalogo_estados'] = $this->catalogo_personsa_model->get_estados();
        $data['clubes'] = $this->catalogo_personsa_model->get_clubes();
        $data['instructores'] = $this->catalogo_personsa_model->get_instructores();
        $data['grados'] = $this->catalogo_personsa_model->get_grado();
        $data['catalogo_bancos'] = $this->registro_model->obtener_catalogo_bancos();

        switch ($id_tipo) {
            case 1://registrdo
                $data['readonly'] = 'readonly = "true"';
                $data['disabled'] = 'disabled';
                $data['persona'] = $this->persona_model->get_persona_curp($curp);
//                var_dump($data['persona']);exit;
                $this->load->view('campeonato/registro_invitado', $data);
                $this->load->view('campeonato/registro_combate', $data);
                break;
            case 2://Invitado
                $data['readonly'] = '';
                $data['disabled'] = '';
                $this->load->view('campeonato/registro_invitado', $data);
                $this->load->view('campeonato/registro_combate', $data);
                break;
        }
    }

    public function guardar_datos_pre_registro()
    {
        $datos = $this->input->post();
        $foto_persona = $_FILES['foto_persona'];
        $user_img_profile = $datos['curp'] . '_' . $foto_persona['name'];

        $config['upload_path'] = './assets/img/users_img';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['file_name'] = $user_img_profile;
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('foto_persona')) {
            $subido = array('archivo' => $this->upload->data());
            $datos['url'] = $subido['archivo']['file_name'];
        } else {
            $this->session->set_flashdata('message_error', $this->upload->display_errors());
            $respuesta['mensaje'] = 'error al subir la imagen';
            $respuesta['tipo'] = 'error';
            $respuesta['data'] = '';
        }
//                var_dump($datos['url']);exit();
        if ($this->registro_model->verificar_curp_competencia($datos['curp'])) {
            $respuesta['estatus'] = false;
            $respuesta['mensaje'] = 'El CURP ya se encuentra registrado en el sistema';
            $respuesta['id_nuevo_registro'] = null;
        } else {
            if ($this->registro_model->verificar_pre_registro_persona($datos['id_persona'])) {
                $id_nuevo_registro = $this->registro_model->guardar_datos_pre_registro($datos);

                if ($id_nuevo_registro != null) {
                    $respuesta['estatus'] = true;
                    $respuesta['mensaje'] = 'Datos Guardados con éxito';
                    $str = serialize($id_nuevo_registro);
                    $url = urlencode($str);
                    $respuesta['id_nuevo_registro'] = $url;

                } else {
                    $respuesta['estatus'] = false;
                    $respuesta['mensaje'] = 'Error al guardar la información';
                    $respuesta['id_nuevo_registro'] = null;
                }
            } else {
                $respuesta['estatus'] = false;
                $respuesta['mensaje'] = '!Lo sentimos ! pero la siguente persona ya se encuentra registrada ';
                $respuesta['id_nuevo_registro'] = null;
            }
        }

        echo json_encode($respuesta);
    }

    public function generar_ficha_pdf($id_nuevo_registro)
    {
        $this->generar_vista_ficha_pre_registro($id_nuevo_registro);
    }

    public function generar_vista_ficha_pre_registro($id_nuevo_registro)
    {
        $personas = unserialize(urldecode($id_nuevo_registro));
        $pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetTitle('Ficha Pre-rigistro');
        $pdf->SetSubject('TCPDF Tutorial');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        $pdf->SetFontSize('10px');
        $pdf->SetMargins(20, 35, 20);
        $logo = 'logoFemedell.jpg';

        // set default header data
        $pdf->SetHeaderData($logo, 10, 'Ficha de Registro', 'campeonato');

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        foreach ($personas as $persona):

            $data['datos'] = $this->registro_model->obtener_datos_persona_pre_registro($persona);
            $data['persona_has_competencia_tipo'] = $this->registro_model->obtener_datos_competencia_persona($persona);
            $data['persona_has_combate'] = $this->registro_model->obtener_datos_combate_persona($persona);
            $data['persona_has_formas'] = $this->registro_model->obtener_datos_formas_persona($persona);


            $cumpleanos = new DateTime($data['datos']['fecha_nacimineto']);
            $hoy = new DateTime();
            $diff = $hoy->diff($cumpleanos);
            $data['edad'] = $diff->y;

            $html_vista = $this->load->view('campeonato/pdf/ficha_registro', $data);

//            $html = $this->output->get_output($html_vista);
            $html=$html_vista->output->final_output;
            $html_vista->output->final_output='';
            $pdf->AddPage();
            $pdf->writeHTML($html, true, false, true, false, '');
            $pdf->lastPage();

        endforeach;
        //Guardar el archivo

        $pash = FCPATH . 'assets/img/fichas_pre_registro/';
        $nombre_archivo = 'ficha.pdf';
        $full_pash = $pash . $nombre_archivo;
        $funcion = 'FI';


        /*
        I: send the file inline to the browser (default).
        D: send to the browser and force a file download with the name given by name.
        F: save to a local server file with the name given by name.
        S: return the document as a string (name is ignored).
        FI: equivalent to F + I option.
        FD: equivalent to F + D option.
        E: return the document as base64 mime multi-part email attachment (RFC 2045)
        */

        $pdf->Output($full_pash, $funcion);
        $this->enviar_correo($full_pash);

    }

    public function enviar_correo($full_pash)
    {
        $this->load->library('email');
//
        $this->email->from('lawlietsosa@gmail.com', 'Lima lama');
        $this->email->to();
        $this->email->attach($full_pash);
        $this->email->subject('Email Test');
        $this->email->message('Testing the email class.');

        if ($this->email->send()) {
            redirect(base_url());
        }

    }

    public function registro_alumnos()
    {
        $data['scripts'] = array(
            base_url() . 'assets/js/campeonato.js'
        );
        $data['catalogo_estados'] = $this->catalogo_personsa_model->get_estados();
        $data['catalogo_bancos'] = $this->registro_model->obtener_catalogo_bancos();
        $this->load->view('campeonato/registro_alumnos', $data);
    }

    public function get_alumnos()
    {
        $id_estado = $this->input->post();
        $data['lista_personas'] = $this->registro_model->get_alumnos($id_estado);
        $this->load->view('campeonato/tabla_alumnos', $data);
    }

    public function guardar_datos_pre_registro_alumnos()
    {
        $datos = $this->input->post();
        $verifica = $this->registro_model->verificar_pre_registro_alumnos($datos);
        if (!$verifica['status']) {
            $ids_persona = $this->registro_model->guardar_datos_pre_registro($datos);
            $respuesta['estatus'] = true;
            $respuesta['mensaje'] = "Deportistas Registrados con Éxito";
            $str = serialize($ids_persona);
            $url = urlencode($str);
            $respuesta['datos'] = $url;
        } else {
            $respuesta['estatus'] = false;
            $respuesta['mensaje'] = "!Lo sentimos! pero los siguientes alumnos ya estan inscritos";
            $respuesta['datos'] = $verifica['datos'];
            $respuesta['error'] = 0;

            if ($verifica['error'] == 1) {
                $respuesta['mensaje'] = "!Error! No hay Alumnos seleccionados";
                $respuesta['error'] = 1;
            }

        }
        echo json_encode($respuesta);
    }
}