<?php
/**
 * Created by PhpStorm.
 * User: omar
 * Date: 2019-02-17
 * Time: 10:06
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class grafica extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('campeonato/grafica_model');
        $this->load->library('Pdf');
        $this->load->helper('file');

    }

    public function index(){
        $data['scripts']= array(
            base_url().'assets/js/grafica.js'
        );
        $this->load->view('campeonato/grafica',$data);
    }

    public function obtener_competidores(){
        $oponentes = $this->grafica_model->obtener_competidores();

        echo json_encode($oponentes);
    }

    public function dataURL_imagen(){
        $post = $this->input->post();
        $img = $post['dataurl'];
        $id_grafico = $post['id_grafico'];


        $img = str_replace("data:image/png;base64,", '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);

        //$im = imagecreatefromstring($data);
        //imagejpeg($im, FCPATH . 'assets/img/pdf_grafica/grafica.png');
        //imagedestroy($im); //liberar memoria

        //file_put_contents(FCPATH . 'assets/img/pdf_grafica/grafica.png',$data);
        $nombre_imagen = 'grafico_'.$id_grafico.'.png';
        $ruta_save = 'assets/img/pdf_grafica/'.$nombre_imagen;
        $ruta_pash = './assets/img/pdf_grafica/'.$nombre_imagen;

        if(write_file($ruta_pash,$data)){
            $this->grafica_model->guardar_imagen_grafico_combates($ruta_save, $nombre_imagen, $id_grafico);

            $respuesta['nombre']=$nombre_imagen;
            $respuesta ['mensaje'] = 'Imagen guardada correctamente';
            $respuesta['estatus']=true;
        }else{
            $respuesta['ruta']='';
            $respuesta ['mensaje'] = 'Error al subir archvio';
            $respuesta['estatus']=false;
        }

        echo json_encode($respuesta);
    }

    public function crear_pdf_grafica($nombre){

        /*set_time_limit(9999999999);
        ini_set('memory_limit', '1024M');
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);*/

        $pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetTitle('Enfrentamientos');
        $pdf->SetSubject('TCPDF Tutorial');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        $pdf->SetFontSize('10px');
        $pdf->SetMargins(20, 35, 20);
        $logo = 'logoFemedell.jpg';

        // set default header data
        $pdf->SetHeaderData($logo, 10, 'Enfrentamientos', 'campeonato');

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $dato['nombre']=$nombre;

        $html_vista = $this->load->view('campeonato/pdf/formato_grafica', $dato);

        //$html_vista = "<div>Datos extras...</div>";
        //$html_vista = $html_vista. "<div>Datos extras... mas</div>";
        $html=$html_vista->output->final_output;

        $pdf->AddPage();
        $pdf->writeHTML($html, true, false, true, false, '');
        //Guardar el archivo

        $pash = FCPATH . 'assets/img/pdf_grafica/';
        $nombre_archivo = 'grafica.pdf';
        $full_pash = $pash . $nombre_archivo;
        $funcion = 'FI';


        /*
        I: send the file inline to the browser (default).
        D: send to the browser and force a file download with the name given by name.
        F: save to a local server file with the name given by name.
        S: return the document as a string (name is ignored).
        FI: equivalent to F + I option.
        FD: equivalent to F + D option.
        E: return the document as base64 mime multi-part email attachment (RFC 2045)
        */

        $pdf->Output($full_pash, $funcion);
        $this->enviar_correo($full_pash);
    }
}