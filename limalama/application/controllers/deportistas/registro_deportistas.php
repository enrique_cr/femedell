<?php
/**
 * Created by PhpStorm.
 * User: toshiba Amilcar Sosa
 * Date: 07/02/2019
 * Time: 11:33 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class registro_deportistas extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('personas/catalogo_personsa_model');
        $this->load->model('personas/persona_model');

    }

    public function index(){
        $data['scripts']= array(
            base_url().'assets/js/deportistas.js'
//            base_url().'assets/js/clubes_escuelas.js'
        );
        $this->load->view('deportistas/registro_deportistas',$data);
    }

    public function agregar_modificar_deportista($id = false)
    {
        $data['catalogo_estados'] = $this->catalogo_personsa_model->get_estados();
        $data['escolaridades'] = $this->catalogo_personsa_model->get_escolaridad();
        $data['grados'] = $this->catalogo_personsa_model->get_grado();
        $data['estatus'] = $this->catalogo_personsa_model->get_estatus();
        $data['tipos'] = $this->catalogo_personsa_model->get_tipo_sangre();
        $data['parentescos'] = $this->catalogo_personsa_model->get_parentesco();
        $data['roles'] = $this->catalogo_personsa_model->get_roles();
        $data['clubes'] = $this->catalogo_personsa_model->get_clubes();
        $data['instructores'] = $this->catalogo_personsa_model->get_instructores();
        if ($id != false) {
            $data['persona'] = $this->persona_model->get_persona_id($id);
        }
        $this->load->view('deportistas/modal_deportistas/modal_agregar_deportista', $data);
    }

    public function buscar_personas()
    {
        $datos=$this->input->post();
        if($datos=='' || $datos==null){
            $data['lista_personas'] = $this->persona_model->get_personas();
        }else{
            $data['lista_personas'] = $this->persona_model->get_personas($datos);
        }
        $this->load->view('deportistas/tabla_lista_personas', $data);
    }

    public function guardar_persona($id_persona = false)
    {
        $datos = $this->input->post();
        $curp=$_POST['curp'];
        $archivo = $_FILES['foto_persona'];
        $user_img_profile = $curp . '_' . $archivo['name'];

        $config['upload_path'] = './assets/img/users_img';
//              'allowed_types' => "gif|jpg|jpeg|png|iso|dmg|zip|rar|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|rtf|sxc|sxi|txt|exe|avi|mpeg|mp3|mp4|3gp",
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['file_name'] = $user_img_profile;
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('foto_persona')) {
            $subido = array('archivo'=>$this->upload->data());
            $datos['url']=$subido['archivo']['file_name'];
        }else {
            $this->session->set_flashdata('message_error', $this->upload->display_errors());
            $respuesta['mensaje'] = 'error';
            $respuesta['tipo'] = 'error';
            $respuesta['data'] = '';
        }
        try {
            if ($id_persona != false) {
                $respuesta['mensaje'] = $this->persona_model->actualizar_persona($id_persona, $datos);
            } else {
                $respuesta['mensaje'] = $this->persona_model->guardar_persona($datos);
            }

            $respuesta['tipo'] = 'exito';
            $respuesta['data'] = '';
        }catch (Exception $e){

        }
        echo json_encode($respuesta);
    }

    public function confirmar_eliminar_persona($id)
    {
        $data['persona']=$this->persona_model->get_persona_id($id);
        $this->load->view('deportistas/modal_deportistas/modal_eliminar_persona',$data);
    }

    public function eliminar_persona($id_persona)
    {
        try {
            $respuesta['mensaje'] = $this->persona_model->eliminar_persona($id_persona);

        } catch (Exception $e) {
            $respuesta['mensaje'] = 'A ocurrido un erro al guardar ' . $e;
            $respuesta['tipo'] = 'error';
            $respuesta['data'] = '';
        }
        $respuesta['tipo'] = 'exito';
        $respuesta['data'] = '';

        echo json_encode($respuesta);
    }
}