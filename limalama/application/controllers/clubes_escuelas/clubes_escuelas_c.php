<?php
/**
 * Created by PhpStorm.
 * User: omar
 * Date: 2019-02-08
 * Time: 13:46
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Clubes_escuelas_c extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('clubes_escuela/clubes_escuela_m');
    }


    public function clubes_escuelas(){
        $data['scripts']=$this->cargar_scripts();
        $data['clubes_escuelas'] = $this->clubes_escuela_m->consultar_clubes_escuelas();
        $data['catalogo_estados'] = $this->clubes_escuela_m->obtener_catalogo_estados();
        $this->load->view('clubes_escuelas/clubes_escuelas_v',$data);
    }

    public function consultar_clubes_escuelas(){
        if(isset($_POST['palabra_buscar'])){
            $palabra_buscar=$_POST['palabra_buscar'];
        }
        if($palabra_buscar == '' || $palabra_buscar == null){
            $data['clubes_escuelas'] = $this->clubes_escuela_m->consultar_clubes_escuelas();
        }
        else{
            $data['clubes_escuelas']=$this->clubes_escuela_m->consultar_clubes_escuelas($palabra_buscar);
        }
        $this->load->view('clubes_escuelas/tabla_clubes_escuelas',$data);
    }

    public function agregar_modificar_clubes_escuelas(){
        $datos=$this->input->post();
        if($this->clubes_escuela_m->agregar_modificar_clubes_escuelas($datos)){
            $respuesta['mensaje']="Datos guardados con èxito";
            $respuesta['estado']=true;
        }
        echo json_encode($respuesta);
    }

    public function eliminar_clubes_escuelas(){

        $id_clubes_escuelas=$_POST['id_clubes_escuelas'];

        if($this->clubes_escuela_m->eliminar_clubes_escuelas($id_clubes_escuelas)){
            $respuesta['mensaje']='Registro eliminado';
            $respuesta['estado']=true;

            echo json_encode($respuesta);
        }
    }

    public function consultar_clubes_escuelas_by_id(){
        $id_clubes_escuelas=$_POST['id_clubes_escuelas'];
        $data['catalogo_estados'] = $this->clubes_escuela_m->obtener_catalogo_estados();
        $data['clubes_escuelas']=$this->clubes_escuela_m->consultar_clubes_escuelas_by_id($id_clubes_escuelas);
        $this->load->view('clubes_escuelas/modal_agregar_clubes_escuelas',$data);
    }

    public function obtener_catalogo_municipios_by_id_estado(){
        $id_estado=$_POST['id_estado'];

        $data['catalogo_municipios'] = $this->clubes_escuela_m->obtener_catalogo_municipios_by_id_estado($id_estado);
        echo json_encode($data);
    }

    public function obtener_catalogo_localidades_by_id_municipio(){
        $id_municipio=$_POST['id_municipio'];
        $data['catalogo_localidades'] = $this->clubes_escuela_m->obtener_catalogo_localidades_by_id_municipio($id_municipio);

        echo json_encode($data);
    }

    function cargar_scripts(){
        $scripts = array(
            base_url() . 'assets/js/clubes_escuelas.js'
        );
        return $scripts;
    }
}