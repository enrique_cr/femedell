CREATE SCHEMA IF NOT EXISTS `femedell` DEFAULT CHARACTER SET utf8 ;
USE `femedell` ;


CREATE TABLE IF NOT EXISTS `femedell`.`catalogo_campeonato` (
  `id_catalogo_campeonato` INT NOT NULL AUTO_INCREMENT,
  `nombre_catalogo_campeonato` VARCHAR(45) NULL,
  PRIMARY KEY (`id_catalogo_campeonato`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `femedell`.`catalogo_categorias` (
  `id_catalogo_categorias` INT NOT NULL AUTO_INCREMENT,
  `nombre_catalogo_categorias` VARCHAR(45) NULL,
  PRIMARY KEY (`id_catalogo_categorias`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `femedell`.`catalogo_formas` (
  `id_catalogo_modalidades` INT NOT NULL AUTO_INCREMENT,
  `nombre_catalogo_modalidades` VARCHAR(45) NULL,
  PRIMARY KEY (`id_catalogo_modalidades`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `femedell`.`catalogo_modalidades` (
  `id_catalogo_modalidades` INT NOT NULL,
  `nombre_catalogo_modalidades` VARCHAR(45) NULL,
  PRIMARY KEY (`id_catalogo_modalidades`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `femedell`.`catalogo_ramas` (
  `id_catalogo_ramas` INT NOT NULL AUTO_INCREMENT,
  `nombre_catalogo_ramas` VARCHAR(45) NULL,
  PRIMARY KEY (`id_catalogo_ramas`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `femedell`.`catalogo_tipos_competicion` (
  `id_catalogo_tipos_competicion` INT NOT NULL AUTO_INCREMENT,
  `nombre_catalogo_tipos_competicion` VARCHAR(45) NULL,
  PRIMARY KEY (`id_catalogo_tipos_competicion`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `femedell`.`catalogo_combates` (
  `id_catalogo_combates` INT NOT NULL AUTO_INCREMENT,
  `nombre_catalogo_combates` VARCHAR(45) NULL,
  PRIMARY KEY (`id_catalogo_combates`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `femedell`.`catalogo_combate_continua` (
  `id_catalogo_continua` INT NOT NULL AUTO_INCREMENT,
  `nombre_catalogo_continua` VARCHAR(45) NULL,
  PRIMARY KEY (`id_catalogo_continua`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `femedell`.`catalogo_modalidades` (
  `id_catalogo_modalidades` INT NOT NULL,
  `nombre_catalogo_modalidades` VARCHAR(45) NULL,
  PRIMARY KEY (`id_catalogo_modalidades`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `femedell`.`catalogo_estatus` (
  `id_catalogo_estatus` INT NOT NULL AUTO_INCREMENT,
  `nombre catalogo_estatus` VARCHAR(45) NULL,
  PRIMARY KEY (`id_catalogo_estatus`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `femedell`.`catalogo_cintas` (
  `id_catalogo_cintas` INT NOT NULL AUTO_INCREMENT,
  `nombre_catalogo_cintas` VARCHAR(45) NULL,
  PRIMARY KEY (`id_catalogo_cintas`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `femedell`.`catalogo_escolaridad` (
  `id_catalogo_escolaridad` INT NOT NULL AUTO_INCREMENT,
  `nombre_catalogo_escolaridad` VARCHAR(45) NULL,
  PRIMARY KEY (`id_catalogo_escolaridad`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `femedell`.`domicilios` (
  `id_domicilio` INT NOT NULL,
  `calle` VARCHAR(45) NULL,
  `id_estado` INT NULL,
  `id_municipio` INT NULL,
  `id_localidad` INT NULL,
  `numero_exterior` VARCHAR(45) NULL,
  `codigo_postal` INT NULL,
  PRIMARY KEY (`id_domicilio`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `femedell`.`catalogo_tipo_sangre` (
  `id_catalogo_tipo_sangre` INT NOT NULL AUTO_INCREMENT,
  `nombre_catalogo_tipo_sangre` VARCHAR(45) NULL,
  PRIMARY KEY (`id_catalogo_tipo_sangre`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `femedell`.`catalogo_parentescos` (
  `id_catalogo_parentescos` INT NOT NULL,
  `parentesco` VARCHAR(45) NULL,
  PRIMARY KEY (`id_catalogo_parentescos`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `femedell`.`persona` (
  `id_persona` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) NULL,
  `apellido_paterno` VARCHAR(45) NULL,
  `apellido_materno` VARCHAR(45) NULL,
  `curp` VARCHAR(18) NULL,
  `id_domicilio` INT NULL,
  `id_catalogo_escolaridad` INT NULL,
  `id_catalgo_estatus` INT NULL,
  `grado` VARCHAR(200) NULL,
  `fecha_grado` DATE NULL,
  `club_asociado` VARCHAR(100) NULL,
  `id_catalogo_cintas` INT NULL,
  `id_instructor` INT(11) NULL,
  `fecha_nacimineto` DATE NULL,
  `peso` DOUBLE NULL,
  `estatura` DOUBLE NULL,
  `id_catalogo_tipo_sangre` INT NULL,
  `alergias` VARCHAR(500) NULL,
  `nombre_responsable` VARCHAR(100) NULL,
  `id_parentesco_responsable` INT NULL,
  `telefono_responsable` INT(10) NULL,
  `fecha_alta` DATETIME NULL,
  PRIMARY KEY (`id_persona`),
  INDEX `fk_deportistas_domicilios1_idx` (`id_domicilio` ASC),
  INDEX `fk_deportistas_estatus_idx` (`id_catalgo_estatus` ASC),
  INDEX `fk_deportistas_cintas_idx` (`id_catalogo_cintas` ASC),
  INDEX `fk_deportistas_tipo_sangre_idx` (`id_catalogo_tipo_sangre` ASC),
  INDEX `fk_id_catalogo_escolaridad_idx` (`id_catalogo_escolaridad` ASC),
  INDEX `fk_id_parentesco_responsable_idx` (`id_parentesco_responsable` ASC),
  CONSTRAINT `fk_persona_domicilios1`
    FOREIGN KEY (`id_domicilio`)
    REFERENCES `femedell`.`domicilios` (`id_domicilio`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_persona_estatus`
    FOREIGN KEY (`id_catalgo_estatus`)
    REFERENCES `femedell`.`catalogo_estatus` (`id_catalogo_estatus`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_persona_cintas`
    FOREIGN KEY (`id_catalogo_cintas`)
    REFERENCES `femedell`.`catalogo_cintas` (`id_catalogo_cintas`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_persona_tipo_sangre`
    FOREIGN KEY (`id_catalogo_tipo_sangre`)
    REFERENCES `femedell`.`catalogo_tipo_sangre` (`id_catalogo_tipo_sangre`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_catalogo_escolaridad`
    FOREIGN KEY (`id_catalogo_escolaridad`)
    REFERENCES `femedell`.`catalogo_escolaridad` (`id_catalogo_escolaridad`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_parentesco_responsable`
    FOREIGN KEY (`id_parentesco_responsable`)
    REFERENCES `femedell`.`catalogo_parentescos` (`id_catalogo_parentescos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `femedell`.`telefonos` (
  `id_telefono` INT NOT NULL AUTO_INCREMENT,
  `numero_telefono` INT NULL,
  `id_persona` INT NOT NULL,
  PRIMARY KEY (`id_telefono`),
  INDEX `fk_telefonos_deportistas_idx` (`id_persona` ASC),
  CONSTRAINT `fk_telefonos_deportistas`
    FOREIGN KEY (`id_persona`)
    REFERENCES `femedell`.`persona` (`id_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `femedell`.`catalogo_roles` (
  `id_catalogo_roles` INT NOT NULL AUTO_INCREMENT,
  `nombre_catalogo_roles` VARCHAR(45) NULL,
  PRIMARY KEY (`id_catalogo_roles`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `femedell`.`correos` (
  `id_correos` INT(11) NOT NULL,
  `id_persona` INT(11) NULL,
  `correo` VARCHAR(200) NULL,
  PRIMARY KEY (`id_correos`),
  INDEX `fk_id_deportista_idx` (`id_persona` ASC),
  CONSTRAINT `fk_id_deportista`
    FOREIGN KEY (`id_persona`)
    REFERENCES `femedell`.`persona` (`id_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `femedell`.`rol_asignacion` (
  `id_rol_asignacion` INT(11) NOT NULL,
  `id_catalogo_roles` INT(11) NULL,
  `id_persona` INT(11) NULL,
  PRIMARY KEY (`id_rol_asignacion`),
  INDEX `fk_id_catalogo_roles_idx` (`id_catalogo_roles` ASC),
  INDEX `fk_id_persona_idx` (`id_persona` ASC),
  CONSTRAINT `fk_id_catalogo_roles`
    FOREIGN KEY (`id_catalogo_roles`)
    REFERENCES `femedell`.`catalogo_roles` (`id_catalogo_roles`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_persona`
    FOREIGN KEY (`id_persona`)
    REFERENCES `femedell`.`persona` (`id_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `femedell`.`fotografias` (
  `id_fotografias` INT NOT NULL AUTO_INCREMENT,
  `id_persona` INT(11) NULL,
  `url` LONGTEXT NULL,
  PRIMARY KEY (`id_fotografias`),
  INDEX `fk_id_persona_idx` (`id_persona` ASC),
  CONSTRAINT `fk_id_persona_idx`
    FOREIGN KEY (`id_persona`)
    REFERENCES `femedell`.`persona` (`id_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

ALTER TABLE `femedell`.`correos` 
CHANGE COLUMN `id_correos` `id_correos` INT(11) NOT NULL AUTO_INCREMENT ;

START TRANSACTION;
USE `femedell`;
INSERT INTO `femedell`.`catalogo_campeonato` (`id_catalogo_campeonato`, `nombre_catalogo_campeonato`) VALUES (DEFAULT, 'TAMAITI');
INSERT INTO `femedell`.`catalogo_campeonato` (`id_catalogo_campeonato`, `nombre_catalogo_campeonato`) VALUES (DEFAULT, 'REY KAMEHAMEHA');
INSERT INTO `femedell`.`catalogo_campeonato` (`id_catalogo_campeonato`, `nombre_catalogo_campeonato`) VALUES (DEFAULT, 'COPA CINTAS NEGRA');
INSERT INTO `femedell`.`catalogo_campeonato` (`id_catalogo_campeonato`, `nombre_catalogo_campeonato`) VALUES (DEFAULT, 'REY TLATUANI CUAUHTEMOC');

COMMIT;


START TRANSACTION;
USE `femedell`;
INSERT INTO `femedell`.`catalogo_categorias` (`id_catalogo_categorias`, `nombre_catalogo_categorias`) VALUES (DEFAULT, 'Infantil');
INSERT INTO `femedell`.`catalogo_categorias` (`id_catalogo_categorias`, `nombre_catalogo_categorias`) VALUES (DEFAULT, 'juvenil');
INSERT INTO `femedell`.`catalogo_categorias` (`id_catalogo_categorias`, `nombre_catalogo_categorias`) VALUES (DEFAULT, 'Adultos');
INSERT INTO `femedell`.`catalogo_categorias` (`id_catalogo_categorias`, `nombre_catalogo_categorias`) VALUES (DEFAULT, 'Principiantes');
INSERT INTO `femedell`.`catalogo_categorias` (`id_catalogo_categorias`, `nombre_catalogo_categorias`) VALUES (DEFAULT, 'Intermedios');
INSERT INTO `femedell`.`catalogo_categorias` (`id_catalogo_categorias`, `nombre_catalogo_categorias`) VALUES (DEFAULT, 'Avanzados');

COMMIT;


START TRANSACTION;
USE `femedell`;
INSERT INTO `femedell`.`catalogo_formas` (`id_catalogo_modalidades`, `nombre_catalogo_modalidades`) VALUES (DEFAULT, 'Tradicional');
INSERT INTO `femedell`.`catalogo_formas` (`id_catalogo_modalidades`, `nombre_catalogo_modalidades`) VALUES (DEFAULT, 'Estricta');
INSERT INTO `femedell`.`catalogo_formas` (`id_catalogo_modalidades`, `nombre_catalogo_modalidades`) VALUES (DEFAULT, 'Creativa');
INSERT INTO `femedell`.`catalogo_formas` (`id_catalogo_modalidades`, `nombre_catalogo_modalidades`) VALUES (DEFAULT, 'Armas');

COMMIT;


START TRANSACTION;
USE `femedell`;
INSERT INTO `femedell`.`catalogo_ramas` (`id_catalogo_ramas`, `nombre_catalogo_ramas`) VALUES (DEFAULT, 'Varonil');
INSERT INTO `femedell`.`catalogo_ramas` (`id_catalogo_ramas`, `nombre_catalogo_ramas`) VALUES (DEFAULT, 'Femenil');

COMMIT;


START TRANSACTION;
USE `femedell`;
INSERT INTO `femedell`.`catalogo_tipos_competicion` (`id_catalogo_tipos_competicion`, `nombre_catalogo_tipos_competicion`) VALUES (1, 'Individual');
INSERT INTO `femedell`.`catalogo_tipos_competicion` (`id_catalogo_tipos_competicion`, `nombre_catalogo_tipos_competicion`) VALUES (2, 'Equipos');

COMMIT;


START TRANSACTION;
USE `femedell`;
INSERT INTO `femedell`.`catalogo_combates` (`id_catalogo_combates`, `nombre_catalogo_combates`) VALUES (DEFAULT, 'Puntos');
INSERT INTO `femedell`.`catalogo_combates` (`id_catalogo_combates`, `nombre_catalogo_combates`) VALUES (DEFAULT, 'Continua');

COMMIT;


START TRANSACTION;
USE `femedell`;
INSERT INTO `femedell`.`catalogo_combate_continua` (`id_catalogo_continua`, `nombre_catalogo_continua`) VALUES (DEFAULT, 'Light contact');
INSERT INTO `femedell`.`catalogo_combate_continua` (`id_catalogo_continua`, `nombre_catalogo_continua`) VALUES (DEFAULT, 'Full contact');
INSERT INTO `femedell`.`catalogo_combate_continua` (`id_catalogo_continua`, `nombre_catalogo_continua`) VALUES (DEFAULT, 'king boxing');
INSERT INTO `femedell`.`catalogo_combate_continua` (`id_catalogo_continua`, `nombre_catalogo_continua`) VALUES (DEFAULT, 'MMA');

COMMIT;


START TRANSACTION;
USE `femedell`;
INSERT INTO `femedell`.`catalogo_estatus` (`id_catalogo_estatus`, `nombre catalogo_estatus`) VALUES (DEFAULT, 'Vigente');
INSERT INTO `femedell`.`catalogo_estatus` (`id_catalogo_estatus`, `nombre catalogo_estatus`) VALUES (DEFAULT, 'No Vigente');

COMMIT;


START TRANSACTION;
USE `femedell`;
INSERT INTO `femedell`.`catalogo_cintas` (`id_catalogo_cintas`, `nombre_catalogo_cintas`) VALUES (DEFAULT, 'Blanca');
INSERT INTO `femedell`.`catalogo_cintas` (`id_catalogo_cintas`, `nombre_catalogo_cintas`) VALUES (DEFAULT, 'Naranja');
INSERT INTO `femedell`.`catalogo_cintas` (`id_catalogo_cintas`, `nombre_catalogo_cintas`) VALUES (DEFAULT, 'Morada');
INSERT INTO `femedell`.`catalogo_cintas` (`id_catalogo_cintas`, `nombre_catalogo_cintas`) VALUES (DEFAULT, 'Azul');
INSERT INTO `femedell`.`catalogo_cintas` (`id_catalogo_cintas`, `nombre_catalogo_cintas`) VALUES (DEFAULT, 'Verde');
INSERT INTO `femedell`.`catalogo_cintas` (`id_catalogo_cintas`, `nombre_catalogo_cintas`) VALUES (DEFAULT, 'Cafe');
INSERT INTO `femedell`.`catalogo_cintas` (`id_catalogo_cintas`, `nombre_catalogo_cintas`) VALUES (DEFAULT, 'Cafe 1er grado');
INSERT INTO `femedell`.`catalogo_cintas` (`id_catalogo_cintas`, `nombre_catalogo_cintas`) VALUES (DEFAULT, 'Cafe 2do grado');
INSERT INTO `femedell`.`catalogo_cintas` (`id_catalogo_cintas`, `nombre_catalogo_cintas`) VALUES (DEFAULT, 'Cafe 3er grado');
INSERT INTO `femedell`.`catalogo_cintas` (`id_catalogo_cintas`, `nombre_catalogo_cintas`) VALUES (DEFAULT, 'Negra');
INSERT INTO `femedell`.`catalogo_cintas` (`id_catalogo_cintas`, `nombre_catalogo_cintas`) VALUES (DEFAULT, 'Negra 1er grado');
INSERT INTO `femedell`.`catalogo_cintas` (`id_catalogo_cintas`, `nombre_catalogo_cintas`) VALUES (DEFAULT, 'Negra 2do grado');
INSERT INTO `femedell`.`catalogo_cintas` (`id_catalogo_cintas`, `nombre_catalogo_cintas`) VALUES (DEFAULT, 'Negra 3er grado');
INSERT INTO `femedell`.`catalogo_cintas` (`id_catalogo_cintas`, `nombre_catalogo_cintas`) VALUES (DEFAULT, 'Negra 4to grado');
INSERT INTO `femedell`.`catalogo_cintas` (`id_catalogo_cintas`, `nombre_catalogo_cintas`) VALUES (DEFAULT, 'Negra 5to grado');
INSERT INTO `femedell`.`catalogo_cintas` (`id_catalogo_cintas`, `nombre_catalogo_cintas`) VALUES (DEFAULT, 'Negra 6to grado');
INSERT INTO `femedell`.`catalogo_cintas` (`id_catalogo_cintas`, `nombre_catalogo_cintas`) VALUES (DEFAULT, 'Negra 7to grado');

COMMIT;


START TRANSACTION;
USE `femedell`;
INSERT INTO `femedell`.`catalogo_escolaridad` (`id_catalogo_escolaridad`, `nombre_catalogo_escolaridad`) VALUES (DEFAULT, 'Prescolar');
INSERT INTO `femedell`.`catalogo_escolaridad` (`id_catalogo_escolaridad`, `nombre_catalogo_escolaridad`) VALUES (DEFAULT, 'Primaria');
INSERT INTO `femedell`.`catalogo_escolaridad` (`id_catalogo_escolaridad`, `nombre_catalogo_escolaridad`) VALUES (DEFAULT, 'Secundaria');
INSERT INTO `femedell`.`catalogo_escolaridad` (`id_catalogo_escolaridad`, `nombre_catalogo_escolaridad`) VALUES (DEFAULT, 'Preparatoria');
INSERT INTO `femedell`.`catalogo_escolaridad` (`id_catalogo_escolaridad`, `nombre_catalogo_escolaridad`) VALUES (DEFAULT, 'Licenciatura');
INSERT INTO `femedell`.`catalogo_escolaridad` (`id_catalogo_escolaridad`, `nombre_catalogo_escolaridad`) VALUES (DEFAULT, 'Maestria');
INSERT INTO `femedell`.`catalogo_escolaridad` (`id_catalogo_escolaridad`, `nombre_catalogo_escolaridad`) VALUES (DEFAULT, 'Doctorado');

COMMIT;

START TRANSACTION;
USE `femedell`;
INSERT INTO `femedell`.`catalogo_tipo_sangre` (`id_catalogo_tipo_sangre`, `nombre_catalogo_tipo_sangre`) VALUES (DEFAULT, 'A positiva');
INSERT INTO `femedell`.`catalogo_tipo_sangre` (`id_catalogo_tipo_sangre`, `nombre_catalogo_tipo_sangre`) VALUES (DEFAULT, 'A Negativo');
INSERT INTO `femedell`.`catalogo_tipo_sangre` (`id_catalogo_tipo_sangre`, `nombre_catalogo_tipo_sangre`) VALUES (DEFAULT, 'B Positivo');
INSERT INTO `femedell`.`catalogo_tipo_sangre` (`id_catalogo_tipo_sangre`, `nombre_catalogo_tipo_sangre`) VALUES (DEFAULT, 'B Negativo');
INSERT INTO `femedell`.`catalogo_tipo_sangre` (`id_catalogo_tipo_sangre`, `nombre_catalogo_tipo_sangre`) VALUES (DEFAULT, 'O Positivo');
INSERT INTO `femedell`.`catalogo_tipo_sangre` (`id_catalogo_tipo_sangre`, `nombre_catalogo_tipo_sangre`) VALUES (DEFAULT, 'O Negativo');
INSERT INTO `femedell`.`catalogo_tipo_sangre` (`id_catalogo_tipo_sangre`, `nombre_catalogo_tipo_sangre`) VALUES (DEFAULT, 'AB Positivo');
INSERT INTO `femedell`.`catalogo_tipo_sangre` (`id_catalogo_tipo_sangre`, `nombre_catalogo_tipo_sangre`) VALUES (DEFAULT, 'AB Negativo');

COMMIT;


START TRANSACTION;
USE `femedell`;
INSERT INTO `femedell`.`catalogo_parentescos` (`id_catalogo_parentescos`, `parentesco`) VALUES (1, 'Padre o Tutor');
INSERT INTO `femedell`.`catalogo_parentescos` (`id_catalogo_parentescos`, `parentesco`) VALUES (2, 'Esposa(o)');
INSERT INTO `femedell`.`catalogo_parentescos` (`id_catalogo_parentescos`, `parentesco`) VALUES (3, 'Hermana(o)');
INSERT INTO `femedell`.`catalogo_parentescos` (`id_catalogo_parentescos`, `parentesco`) VALUES (4, 'Abuela(o)');
INSERT INTO `femedell`.`catalogo_parentescos` (`id_catalogo_parentescos`, `parentesco`) VALUES (5, 'Tia(o)');
INSERT INTO `femedell`.`catalogo_parentescos` (`id_catalogo_parentescos`, `parentesco`) VALUES (6, 'Hija(o)');

COMMIT;


START TRANSACTION;
USE `femedell`;
INSERT INTO `femedell`.`catalogo_roles` (`id_catalogo_roles`, `nombre_catalogo_roles`) VALUES (DEFAULT, 'Deportista');
INSERT INTO `femedell`.`catalogo_roles` (`id_catalogo_roles`, `nombre_catalogo_roles`) VALUES (DEFAULT, 'Instructor');

COMMIT;

