CREATE TABLE `femedell`.`clubes_escuelas` (
  `id_clubes_escuelas` INT(11) NOT NULL AUTO_INCREMENT,
  `folio` VARCHAR(250) NULL,
  `nombre` VARCHAR(200) NULL,
  `id_domicilio` INT(11) NULL,
  `fecha` DATETIME NULL,
  PRIMARY KEY (`id_clubes_escuelas`),
  INDEX `fk_id_domicilio_idx` (`id_domicilio` ASC),
  CONSTRAINT `fk_id_domicilioce`
    FOREIGN KEY (`id_domicilio`)
    REFERENCES `femedell`.`domicilios` (`id_domicilio`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
    ENGINE=InnoDB ;
    


ALTER TABLE `femedell`.`persona` 
DROP FOREIGN KEY `fk_persona_domicilios1`;

ALTER TABLE `femedell`.`persona` 
DROP INDEX `fk_deportistas_domicilios1_idx` ;

ALTER TABLE `femedell`.`domicilios` 
CHANGE COLUMN `id_domicilio` `id_domicilio` INT(11) NOT NULL AUTO_INCREMENT ;


ALTER TABLE `femedell`.`persona` 
ADD INDEX `fk_id_domicilio_idx` (`id_domicilio` ASC);
ALTER TABLE `femedell`.`persona` 
ADD CONSTRAINT `fk_id_domicilio`
  FOREIGN KEY (`id_domicilio`)
  REFERENCES `femedell`.`domicilios` (`id_domicilio`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


/**NUEVO**/
ALTER TABLE `femedell`.`persona` 
ADD COLUMN `sexo` ENUM('F', 'M') NULL AFTER `fecha_alta`,
ADD COLUMN `instructor_otro` VARCHAR(250) NULL AFTER `sexo`,
ADD COLUMN `club_escuela_otro` VARCHAR(250) NULL AFTER `instructor_otro`,
ADD COLUMN `procedencia_registro` ENUM('1', '2') NULL AFTER `club_escuela_otro`;

CREATE TABLE `femedell`.`registro_combates` (
  `id_combates` INT NOT NULL AUTO_INCREMENT,
  `id_catalogo_categorias` INT(11) NULL COMMENT 'tabla catalogo_categorias',
  `id_catalogo_formas` INT(11) NULL COMMENT 'Tabla catalogo_formas',
  `id_catalogo_ramas` INT(11) NULL,
  `id_catalogo_tipos_competicion` INT(11) NULL COMMENT 'Tabla catalogo_tipos_competicion',
  `id_catalogo_combates` INT(11) NULL COMMENT 'Tabla catalogo_combates',
  `id_combate_continua` INT(11) NULL COMMENT 'Tabla catalogo_cobates_continua',
  `fecha_registro` DATETIME NULL,
  `costo` INT(11) NULL,
  PRIMARY KEY (`id_combates`));

ALTER TABLE `femedell`.`registro_combates` 
ADD INDEX `fk_id_catalgo_categorias_idx` (`id_catalogo_categorias` ASC),
ADD INDEX `fk_id_catalogo_formas_idx` (`id_catalogo_formas` ASC),
ADD INDEX `rc_fk_id_catalogo_ramas_idx` (`id_catalogo_ramas` ASC),
ADD INDEX `rc_fk_id_catalogos_tipo_competicion_idx` (`id_catalogo_tipos_competicion` ASC),
ADD INDEX `rc_fk_id_catalogo_combates_idx` (`id_catalogo_combates` ASC),
ADD INDEX `rc_fk_id_combate_continua_idx` (`id_combate_continua` ASC);
ALTER TABLE `femedell`.`registro_combates` 
ADD CONSTRAINT `rc_fk_id_catalgo_categorias`
  FOREIGN KEY (`id_catalogo_categorias`)
  REFERENCES `femedell`.`catalogo_categorias` (`id_catalogo_categorias`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `rc_fk_id_catalogo_formas`
  FOREIGN KEY (`id_catalogo_formas`)
  REFERENCES `femedell`.`catalogo_formas` (`id_catalogo_modalidades`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `rc_fk_id_catalogo_ramas`
  FOREIGN KEY (`id_catalogo_ramas`)
  REFERENCES `femedell`.`catalogo_ramas` (`id_catalogo_ramas`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `rc_fk_id_catalogos_tipo_competicion`
  FOREIGN KEY (`id_catalogo_tipos_competicion`)
  REFERENCES `femedell`.`catalogo_tipos_competicion` (`id_catalogo_tipos_competicion`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `rc_fk_id_catalogo_combates`
  FOREIGN KEY (`id_catalogo_combates`)
  REFERENCES `femedell`.`catalogo_combates` (`id_catalogo_combates`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `rc_fk_id_combate_continua`
  FOREIGN KEY (`id_combate_continua`)
  REFERENCES `femedell`.`catalogo_combate_continua` (`id_catalogo_continua`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
  
  CREATE TABLE `femedell`.`personas_has_registro_combates` (
  `id_personas_has_registro_combates` INT NOT NULL AUTO_INCREMENT,
  `id_persona` INT(11) NULL DEFAULT NULL,
  `id_registro_combate` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_personas_has_registro_combates`),
  INDEX `phrc_fk_id_persona_idx` (`id_persona` ASC),
  INDEX `phrc_fk_id_registro_combate_idx` (`id_registro_combate` ASC),
  CONSTRAINT `phrc_fk_id_persona`
    FOREIGN KEY (`id_persona`)
    REFERENCES `femedell`.`persona` (`id_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `phrc_fk_id_registro_combate`
    FOREIGN KEY (`id_registro_combate`)
    REFERENCES `femedell`.`registro_combates` (`id_combates`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
    CREATE TABLE `femedell`.`catalogo_bancos` (
  `id_catalogo_bancos` INT NOT NULL AUTO_INCREMENT,
  `beneficiario` VARCHAR(250) NULL,
  `nombre_banco` VARCHAR(250) NULL,
  `numero_cuenta` VARCHAR(100) NULL,
  `numero_sucursal` INT(20) NULL,
  `numero_plaza` VARCHAR(100) NULL,
  `cuenta_clabe` VARCHAR(100) NULL,
  PRIMARY KEY (`id_catalogo_bancos`));
  
  ALTER TABLE `femedell`.`registro_combates` 
ADD COLUMN `id_banco` INT(11) NULL DEFAULT NULL AFTER `costo`,
ADD INDEX `rc_fk_id_banco_idx` (`id_banco` ASC);
ALTER TABLE `femedell`.`registro_combates` 
ADD CONSTRAINT `rc_fk_id_banco`
  FOREIGN KEY (`id_banco`)
  REFERENCES `femedell`.`catalogo_bancos` (`id_catalogo_bancos`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

INSERT INTO `femedell`.`catalogo_bancos` (`beneficiario`, `nombre_banco`, `numero_cuenta`, `numero_sucursal`, `numero_plaza`, `cuenta_clabe`) VALUES ('Centro Mexicano para la Filantropía, A.C.', 'BBVA Bancomer', '0443010597', '4638', '001', '012 180 0044 3010597 1');

/*Ejecutar nuevo 19/febrero / 2019*/
drop table personas_has_registro_combates;
drop table registro_combates;

CREATE TABLE `femedell`.`competencia_clasifica` (
  `id_competencia_clasifica` INT NOT NULL AUTO_INCREMENT,
  `id_persona` INT NULL,
  `id_catalogo_ramas` INT NULL,
  `id_catalogo_tipos_competicion` INT NULL,
  `id_catalogo_categorias` INT NULL,
  `fecha` DATETIME NULL,
  PRIMARY KEY (`id_competencia_clasifica`),
  INDEX `fk_cocla_id_persona_idx` (`id_persona` ASC),
  INDEX `fk_cocla_id_cataologo_ramas_idx` (`id_catalogo_ramas` ASC),
  INDEX `fk_cocla_id_catalogo_tipos_competicion_idx` (`id_catalogo_tipos_competicion` ASC),
  INDEX `fk_cocla_id_catalogo_categorias_idx` (`id_catalogo_categorias` ASC),
  CONSTRAINT `fk_cocla_id_persona`
    FOREIGN KEY (`id_persona`)
    REFERENCES `femedell`.`persona` (`id_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cocla_id_cataologo_ramas`
    FOREIGN KEY (`id_catalogo_ramas`)
    REFERENCES `femedell`.`catalogo_ramas` (`id_catalogo_ramas`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cocla_id_catalogo_tipos_competicion`
    FOREIGN KEY (`id_catalogo_tipos_competicion`)
    REFERENCES `femedell`.`catalogo_tipos_competicion` (`id_catalogo_tipos_competicion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cocla_id_catalogo_categorias`
    FOREIGN KEY (`id_catalogo_categorias`)
    REFERENCES `femedell`.`catalogo_categorias` (`id_catalogo_categorias`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `femedell`.`catalogo_formas_pago` (
  `id_catalogo_formas_pago` INT NOT NULL AUTO_INCREMENT,
  `forma_pago` VARCHAR(45) NULL,
  PRIMARY KEY (`id_catalogo_formas_pago`));

INSERT INTO `femedell`.`catalogo_formas_pago` (`forma_pago`) VALUES ('Efectivo');
INSERT INTO `femedell`.`catalogo_formas_pago` (`forma_pago`) VALUES ('Tarjeta');
INSERT INTO `femedell`.`catalogo_formas_pago` (`forma_pago`) VALUES ('Deposito');
INSERT INTO `femedell`.`catalogo_formas_pago` (`forma_pago`) VALUES ('Transferencia');

CREATE TABLE `femedell`.`competencia_pagos` (
  `id_competencia_pagos` INT NOT NULL AUTO_INCREMENT,
  `id_persona` INT NULL,
  `costo` INT NULL,
  `id_forma_pago` INT NULL,
  `id_banco` INT NULL,
  `fecha_alta` DATETIME NULL,
  PRIMARY KEY (`id_competencia_pagos`),
  INDEX `fk_copa_id_persona_idx` (`id_persona` ASC),
  INDEX `fk_copa_id_forma_pago_idx` (`id_forma_pago` ASC),
  INDEX `fk_copa_id_banco_idx` (`id_banco` ASC),
  CONSTRAINT `fk_copa_id_persona`
    FOREIGN KEY (`id_persona`)
    REFERENCES `femedell`.`persona` (`id_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_copa_id_forma_pago`
    FOREIGN KEY (`id_forma_pago`)
    REFERENCES `femedell`.`catalogo_formas_pago` (`id_catalogo_formas_pago`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_copa_id_banco`
    FOREIGN KEY (`id_banco`)
    REFERENCES `femedell`.`catalogo_bancos` (`id_catalogo_bancos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `femedell`.`catalogo_competencia_tipo` (
  `id_catalogo_competencia_tipo` INT NOT NULL AUTO_INCREMENT,
  `tipo` VARCHAR(45) NULL,
  PRIMARY KEY (`id_catalogo_competencia_tipo`));

INSERT INTO `femedell`.`catalogo_competencia_tipo` (`tipo`) VALUES ('Combate');
INSERT INTO `femedell`.`catalogo_competencia_tipo` (`tipo`) VALUES ('Formas');

CREATE TABLE `femedell`.`persona_has_competencia_tipo` (
  `id_persona_has_competencia_tipo` INT NOT NULL AUTO_INCREMENT,
  `id_persona` INT NULL,
  `id_competencia_tipo` INT NULL,
  `fecha_alta` DATETIME NULL,
  PRIMARY KEY (`id_persona_has_competencia_tipo`),
  INDEX `fk_phct_id_persona_idx` (`id_persona` ASC),
  INDEX `fk_phct_id_competencia_tipo_idx` (`id_competencia_tipo` ASC),
  CONSTRAINT `fk_phct_id_persona`
    FOREIGN KEY (`id_persona`)
    REFERENCES `femedell`.`persona` (`id_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_phct_id_competencia_tipo`
    FOREIGN KEY (`id_competencia_tipo`)
    REFERENCES `femedell`.`catalogo_competencia_tipo` (`id_catalogo_competencia_tipo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
    UPDATE `femedell`.`catalogo_combates` SET `nombre_catalogo_combates`='Por Puntos' WHERE `id_catalogo_combates`='1';
UPDATE `femedell`.`catalogo_combates` SET `nombre_catalogo_combates`='Continua (Light Contact)' WHERE `id_catalogo_combates`='2';
INSERT INTO `femedell`.`catalogo_combates` (`nombre_catalogo_combates`) VALUES ('Full Contact');

CREATE TABLE `femedell`.`persona_has_combate` (
  `id_persona_has_combate` INT NOT NULL AUTO_INCREMENT,
  `id_persona` INT NULL,
  `id_catalogo_combates` INT NULL,
  `fecha_alta` DATETIME NULL,
  PRIMARY KEY (`id_persona_has_combate`),
  INDEX `fk_pehaco_id_persona_idx` (`id_persona` ASC),
  INDEX `fk_pehaco_id_catalogo_combates_idx` (`id_catalogo_combates` ASC),
  CONSTRAINT `fk_pehaco_id_persona`
    FOREIGN KEY (`id_persona`)
    REFERENCES `femedell`.`persona` (`id_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pehaco_id_catalogo_combates`
    FOREIGN KEY (`id_catalogo_combates`)
    REFERENCES `femedell`.`catalogo_combates` (`id_catalogo_combates`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

UPDATE `femedell`.`catalogo_formas` SET `nombre_catalogo_modalidades`='Tradicional (Estricta)' WHERE `id_catalogo_modalidades`='1';
UPDATE `femedell`.`catalogo_formas` SET `nombre_catalogo_modalidades`='Creativa Musica' WHERE `id_catalogo_modalidades`='2';
UPDATE `femedell`.`catalogo_formas` SET `nombre_catalogo_modalidades`='Con Armas' WHERE `id_catalogo_modalidades`='3';
DELETE FROM `femedell`.`catalogo_formas` WHERE `id_catalogo_modalidades`='4';
INSERT INTO `femedell`.`catalogo_formas` (`id_catalogo_modalidades`) VALUES ('4');

DELETE FROM `femedell`.`catalogo_formas` WHERE `id_catalogo_modalidades`='4';

CREATE TABLE `femedell`.`persona_has_formas` (
  `id_persona_has_formas` INT NOT NULL AUTO_INCREMENT,
  `id_persona` INT NULL,
  `id_catalogo_formas` INT NULL,
  `fecha_alta` DATETIME NULL,
  PRIMARY KEY (`id_persona_has_formas`),
  INDEX `fk_pehafo_id_persona_idx` (`id_persona` ASC),
  INDEX `fk_pehafo_id_forma_idx` (`id_catalogo_formas` ASC),
  CONSTRAINT `fk_pehafo_id_persona`
    FOREIGN KEY (`id_persona`)
    REFERENCES `femedell`.`persona` (`id_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pehafo_id_forma`
    FOREIGN KEY (`id_catalogo_formas`)
    REFERENCES `femedell`.`catalogo_formas` (`id_catalogo_modalidades`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `femedell`.`persona_pre_registro` (
  `id_persona_pre_registro` INT NOT NULL AUTO_INCREMENT,
  `id_persona` INT NULL,
  `id_convocatoria` INT NULL,
  `fecha_registro` DATETIME NULL,
  PRIMARY KEY (`id_persona_pre_registro`),
  INDEX `fk_pere_id_persona_idx` (`id_persona` ASC),
  INDEX `fk_pere_id_convocatoria_idx` (`id_convocatoria` ASC),
  CONSTRAINT `fk_pere_id_persona`
    FOREIGN KEY (`id_persona`)
    REFERENCES `femedell`.`persona` (`id_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pere_id_convocatoria`
    FOREIGN KEY (`id_convocatoria`)
    REFERENCES `femedell`.`convocatorias` (`id_convocatoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
ALTER TABLE `femedell`.`persona` 
CHANGE COLUMN `procedencia_registro` `procedencia_registro` ENUM('1', '2') NULL DEFAULT '1' ;

/*Nuevo script 22 / febrero / 2019*/
CREATE TABLE `femedell`.`imagenes_graficos` (
  `id_imagenes_graficos` INT NOT NULL AUTO_INCREMENT,
  `id_grafico` INT NULL,
  `ruta_imagen` VARCHAR(250) NULL,
  PRIMARY KEY (`id_imagenes_graficos`));
  
  ALTER TABLE `femedell`.`imagenes_graficos` 
ADD COLUMN `nombre_archivo` VARCHAR(250) NULL AFTER `ruta_imagen`;

