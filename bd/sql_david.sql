DROP TABLE convocatorias;

CREATE TABLE `femedell`.`catalogo_nivel_convocatoria` (
  `id_nivel_convocatoria` INT NOT NULL AUTO_INCREMENT,
  `nombre_nivel_convocatoria` VARCHAR(50) NULL,
  PRIMARY KEY (`id_nivel_convocatoria`));
  
  INSERT INTO catalogo_nivel_convocatoria VALUES(NULL,'Nacional'),(NULL,'Estatal'); 

CREATE TABLE `femedell`.`convocatorias` (
  `id_convocatoria` INT NOT NULL AUTO_INCREMENT,
  `convocatoria_id_nivel` INT(11) NULL,
  `convocatoria_id_estado` INT(11) NULL,
  `convocatoria_sede` VARCHAR(50) NULL,
  `convocatoria_contacto` VARCHAR(50) NULL,
  `convocatoria_fecha_inicio` DATETIME NULL,
  `convocatoria_fecha_fin` DATETIME NULL,
  `convocatoria_costo` VARCHAR(50) NULL,
  PRIMARY KEY (`id_convocatoria`),
  INDEX `fk_catalogo_nivel_convocatoria_idx` (`convocatoria_id_nivel` ASC),
  CONSTRAINT `fk_nivel_convocatoria`
    FOREIGN KEY (`convocatoria_id_nivel`)
    REFERENCES `femedell`.`catalogo_nivel_convocatoria` (`id_nivel_convocatoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
  
  CREATE TABLE `femedell`.`convocatorias_has_catalogo_campeonato` (
  `id_convocatorias` INT(11) NULL,
  `id_catalogo_campeonato` INT(11) NULL,
  INDEX `fk_convocatorias_idx` (`id_convocatorias` ASC),
  INDEX `fk_catalogo_campeonato_idx` (`id_catalogo_campeonato` ASC),
  CONSTRAINT `fk_convocatorias`
    FOREIGN KEY (`id_convocatorias`)
    REFERENCES `femedell`.`convocatorias` (`id_convocatoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_catalogo_campeonato`
    FOREIGN KEY (`id_catalogo_campeonato`)
    REFERENCES `femedell`.`catalogo_campeonato` (`id_catalogo_campeonato`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

    
CREATE TABLE `femedell`.`convocatorias_has_catalogo_formas` (
  `id_convocatorias` INT(11) NULL,
  `id_catalogo_formas` INT(11) NULL,
  INDEX `fk_convocatorias_idx` (`id_convocatorias` ASC),
  INDEX `fk_catalogo_formas_idx` (`id_catalogo_formas` ASC),
  CONSTRAINT `fk_convocatorias`
    FOREIGN KEY (`id_convocatorias`)
    REFERENCES `femedell`.`convocatorias` (`id_convocatoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_catalogo_formas`
    FOREIGN KEY (`id_catalogo_formas`)
    REFERENCES `femedell`.`catalogo_formas` (`id_catalogo_modalidades`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
  
  CREATE TABLE `femedell`.`convocatorias_has_catalogo_categorias` (
  `id_convocatorias` INT(11) NULL,
  `id_catalogo_categorias` INT(11) NULL,
  INDEX `fk_chcc_convocatorias_idx` (`id_convocatorias` ASC),
  INDEX `fk_chcc_catalogo_categorias_idx` (`id_catalogo_categorias` ASC),
  CONSTRAINT `fk_chcc_convocatorias`
    FOREIGN KEY (`id_convocatorias`)
    REFERENCES `femedell`.`convocatorias` (`id_convocatoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_chcc_catalogo_categorias`
    FOREIGN KEY (`id_catalogo_categorias`)
    REFERENCES `femedell`.`catalogo_categorias` (`id_catalogo_categorias`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
    CREATE TABLE `femedell`.`convocatorias_has_catalogo_ramas` (
  `id_convocatorias` INT(11) NULL,
  `id_catalogo_ramas` INT(11) NULL,
  INDEX `fk_chcr_convocatorias_idx` (`id_convocatorias` ASC),
  INDEX `fk_chcr_catalogo_ramas_idx` (`id_catalogo_ramas` ASC),
  CONSTRAINT `fk_chcr_convocatorias`
    FOREIGN KEY (`id_convocatorias`)
    REFERENCES `femedell`.`convocatorias` (`id_convocatoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_chcr_catalogo_ramas`
    FOREIGN KEY (`id_catalogo_ramas`)
    REFERENCES `femedell`.`catalogo_ramas` (`id_catalogo_ramas`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
    CREATE TABLE `femedell`.`convocatorias_has_catalogo_tipos_competicion` (
  `id_convocatorias` INT(11) NULL,
  `id_catalogo_tipos_competicion` INT(11) NULL,
  INDEX `fk_chctc_convocatorias_idx` (`id_convocatorias` ASC),
  INDEX `fk_chctc_catalogo_tipos_competicion_idx` (`id_catalogo_tipos_competicion` ASC),
  CONSTRAINT `fk_chctc_convocatorias`
    FOREIGN KEY (`id_convocatorias`)
    REFERENCES `femedell`.`convocatorias` (`id_convocatoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_chctc_catalogo_tipos_competicion`
    FOREIGN KEY (`id_catalogo_tipos_competicion`)
    REFERENCES `femedell`.`catalogo_tipos_competicion` (`id_catalogo_tipos_competicion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
    CREATE TABLE `femedell`.`convocatorias_has_catalogo_combates` (
  `id_convocatorias` INT(11) NULL,
  `id_catalogo_combates` INT(11) NULL,
  INDEX `fk_chccombates_convocatorias_idx` (`id_convocatorias` ASC),
  INDEX `fk_chccombates_catalogo_combates_idx` (`id_catalogo_combates` ASC),
  CONSTRAINT `fk_chccombates_convocatorias`
    FOREIGN KEY (`id_convocatorias`)
    REFERENCES `femedell`.`convocatorias` (`id_convocatoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_chccombates_catalogo_combates`
    FOREIGN KEY (`id_catalogo_combates`)
    REFERENCES `femedell`.`catalogo_combates` (`id_catalogo_combates`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
    
    CREATE TABLE `femedell`.`convocatorias_has_catalogo_combate_continua` (
  `id_convocatorias` INT(11) NULL,
  `id_catalogo_combate_continua` INT(11) NULL,
  INDEX `fk_chccc_convocatorias_idx` (`id_convocatorias` ASC),
  INDEX `fk_catalogo_combate_continua_idx` (`id_catalogo_combate_continua` ASC),
  CONSTRAINT `fk_chccc_convocatorias`
    FOREIGN KEY (`id_convocatorias`)
    REFERENCES `femedell`.`convocatorias` (`id_convocatoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_catalogo_combate_continua`
    FOREIGN KEY (`id_catalogo_combate_continua`)
    REFERENCES `femedell`.`catalogo_combate_continua` (`id_catalogo_continua`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);