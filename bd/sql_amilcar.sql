ALTER TABLE `femedell`.`persona_has_competencia_tipo` 
ADD COLUMN `id_persona_pre_registro` INT NULL AFTER `id_persona_has_competencia_tipo`;

ALTER TABLE `femedell`.`persona_has_combate` 
ADD COLUMN `id_persona_pre_registro` INT NULL AFTER `id_persona_has_combate`;


ALTER TABLE `femedell`.`persona_has_formas` 
ADD COLUMN `id_persona_pre_registro` INT NULL AFTER `id_persona_has_formas`;


#-----------------------------------------------------------

ALTER TABLE `femedell`.`convocatorias_has_catalogo_formas` 
DROP FOREIGN KEY `fk_catalogo_formas`;
ALTER TABLE `femedell`.`convocatorias_has_catalogo_formas` 
CHANGE COLUMN `id_catalogo_formas` `id_catalogo_modalidades` INT(11) NULL DEFAULT NULL ;
ALTER TABLE `femedell`.`convocatorias_has_catalogo_formas` 
ADD CONSTRAINT `fk_catalogo_formas`
  FOREIGN KEY (`id_catalogo_modalidades`)
  REFERENCES `femedell`.`catalogo_formas` (`id_catalogo_modalidades`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


ALTER TABLE `femedell`.`convocatorias` 
CHANGE COLUMN `convocatoria_fecha_inicio` `convocatoria_fecha_inicio` DATE NULL DEFAULT NULL ,
CHANGE COLUMN `convocatoria_fecha_fin` `convocatoria_fecha_fin` DATE NULL DEFAULT NULL ,
CHANGE COLUMN `convocatoria_costo` `convocatoria_costo` DOUBLE NULL DEFAULT NULL ;
